<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_assistpurchase';

    function load()
    {
        $this->add_shortcode();
        $this->add_shortcode('direct');
        $this->add_shortcode('detail');
        $this->add_shortcode('confirmsucc');
        $this->add_shortcode('confirmfail');
        $this->add_shortcode('transucc');
        $this->add_shortcode('tranfail');
        $this->add_shortcode('uploadimage');
        $this->add_shortcode('trancancel');
        $this->add_shortcode('refund');
        $this->add_ajax('load_assistpurchase');
        $this->add_ajax('load_assistpurchase_direct');
        $this->add_ajax('confirm_success');
        $this->add_ajax('confirm_fail');
        $this->add_ajax('calculate_commission');
        $this->add_ajax('deduct_money');
        $this->add_ajax('cancel_transfer');
        $this->add_ajax('refund_purchase');
        $this->add_ajax('decrypt_pw');
        $this->add_ajax('transfer_success');
        $this->add_ajax('transfer_fail');
        $this->add_ajax('upload_images');
        $this->add_ajax('change_buystatus');
    }

    function shortcode()
    {
        $context = [
        ];
        $this->view('assistpurchase', $context);
    }

    function direct()
    {
        $this->the_script('FileSaver.min');
        $this->the_script('tableexport.min');
        $this->the_script('xlsx.full.min');
        $context = [
        ];
        $this->view('direct', $context);
    }

    function confirmsucc()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
            ];
            $this->view('confirmsucc', $context);
        }
    }

    function confirmfail()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
            ];
            $this->view('confirmfail', $context);
        }
    }

    function uploadimage()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
            ];
            $this->view('upload-images', $context);
        }
    }

    function transucc()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
            ];
            $this->view('transucc', $context);
        }
    }

    function tranfail()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
            ];
            $this->view('tranfail', $context);
        }
    }

    function trancancel()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
            ];
            $this->view('trancancel', $context);
        }
    }

    function confirm_success()
    {
        $data = Shoplic_Util::get_post_data('id', 'goodsmoney', 'expectcommission', 'discount', 'adjustment', 'realmoney');
        // print_r($data);
        $goodsmoney = intval($data['goodsmoney'] ?? 0);
        $expectcommission = intval($data['expectcommission'] ?? 0);
        $adjustment = intval($data['adjustment'] ?? 0);
        $realmoney = intval($data['realmoney'] ?? 0);

        if ($goodsmoney <= 0 || $expectcommission < 0 || $realmoney <= 0)
            wp_send_json_error('Invalid value');
        
        if ($goodsmoney + $expectcommission + $adjustment != $realmoney)
            wp_send_json_error('金额比对失败');

        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $data['id']]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');
        
        if ($purchase->status != Korgou_Purchase::STATUS_WAIT_CHECK)
            wp_send_json_error('Purchase status not WAIT_CHECK');

        $data['checktime'] = current_time('mysql');
        
        if ($balance != null && $balance->balance >= $realmoney) {
            $data['status'] = Korgou_Purchase::STATUS_PAY_SUCC;
            do_action('korgou_purchase_update_purchase', $data);

            do_action('korgou_user_balance_change_balance',
                $purchase->id, $purchase->userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $realmoney, 'KRW', '', '', '', 'sys', 'sys', '');
        } else {
            $data['status'] = Korgou_Purchase::STATUS_WAIT_PAY;
            do_action('korgou_purchase_update_purchase', $data);
        }

        kg_send_json_success();
    }

    function transfer_success()
    {
        $data = Shoplic_Util::get_post_data('id', 'remark');

        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $data['id']]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');
        
        if ($purchase->status != Korgou_Purchase::STATUS_PAY_SUCC)
            wp_send_json_error('Purchase status not Paid');

        $data['donetime'] = current_time('mysql');
        $data['status'] = $purchase->status = Korgou_Purchase::STATUS_SUCC;

        do_action('korgou_purchase_update_purchase', $data);

        $this->save_images($purchase->id);

        $email = KG::get_user_email($purchase->userid);
        if ($email != null) {
            if ($purchase->purchasetype == Korgou_Purchase::PURCHASETYPE_ZHUAN_ZHANG) {
                $body = $this->contents('email/transfer-order', [
                    'purchase' => $purchase,
                ]);

                SP_WC::send_email($email,
                    'Assisted transfer order completed 代刷代付订单完成',
                    'Assisted transfer order completed',
                    $body,
                    apply_filters('korgou_package_get_image_paths', [], ['packageid' => $purchase->id])
                );
            } else {
                $body = $this->contents('email/payment-order', [
                    'purchase' => $purchase,
                ]);

                SP_WC::send_email($email,
                    'Assisted purchase order completed 转账汇款订单完成',
                    'Assisted purchase order completed',
                    $body,
                    apply_filters('korgou_package_get_image_paths', [], ['packageid' => $purchase->id])
                );
            }
        }

        kg_send_json_success();
    }

    function save_images($packageid)
    {
        $arr_img_ext = [ 'image/png', 'image/jpeg', 'image/jpg', 'image/gif' ];

        $total = count($_FILES['file']['name']);
        for ($i = 0; $i < $total; $i++) {
            if (in_array($_FILES['file']['type'][$i], $arr_img_ext)) {
                $path = $_FILES['file']['name'][$i];
                $ext = strtolower(pathinfo($path,PATHINFO_EXTENSION));

                $dir = KORGOU_IMAGE_PATH . '/' . Shoplic_Util::now('ymd');
                mkdir($dir, 0755, true);

                $prefix = $dir . '/' . Shoplic_Util::uuid_v4() . '_';
                $big = $prefix . 'BIG.' . $ext;
                $small = $prefix . 'SMALL.' . $ext;

                if (move_uploaded_file($_FILES["file"]["tmp_name"][$i], $big)) {
                    $editor = wp_get_image_editor($big);
                    $editor->resize(300, 300);
                    $saved = $editor->save($small);
                    if ($saved) {
                        $imageid = apply_filters('korgou_package_insert_image', 0, [
                            'packageid' => $packageid,
                            'smallimageurl' => $small,
                            'bigimageurl' => $big,
                            'imagetype' => Korgou_Package_Image::IMAGETYPE_DAI_GOU_DAI_FU,
                        ]);
                    }
                }
            }
        }
    }

    function transfer_fail()
    {
        $data = Shoplic_Util::get_post_data('id', 'remark');

        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $data['id']]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');
        
        if ($purchase->status != Korgou_Purchase::STATUS_PAY_SUCC)
            wp_send_json_error('Purchase status not PAY_SUCC');

        $data['donetime'] = current_time('mysql');
        $data['status'] = Korgou_Purchase::STATUS_FAIL;

        do_action('korgou_purchase_update_purchase', $data);

        do_action('korgou_package_attach_images', $purchase->id, $_POST['imageid'], Korgou_Package_Image::IMAGETYPE_DAI_GOU_DAI_FU);

        kg_send_json_success();
    }

    function upload_images()
    {
        $data = Shoplic_Util::get_post_data('id', 'remark');

        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $data['id']]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');
        
        do_action('korgou_package_attach_images', $purchase->id, $_POST['imageid'], Korgou_Package_Image::IMAGETYPE_DAI_GOU_DAI_FU);

        kg_send_json_success();
    }

    function confirm_fail()
    {
        $data = Shoplic_Util::get_post_data('id', 'remark');

        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $data['id']]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');
        
        if ($purchase->status != Korgou_Purchase::STATUS_WAIT_CHECK)
            wp_send_json_error('Purchase status not WAIT_CHECK');

        $data['checktime'] = current_time('mysql');
        $data['donetime'] = current_time('mysql');
        $data['status'] = Korgou_Purchase::STATUS_REJECT;

        do_action('korgou_purchase_update_purchase', $data);

        kg_send_json_success();
    }

    function detail()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
                'goods' => apply_filters('korgou_purchase_get_goods_list', [], ['purchaseid' => $id]),
                'images' => apply_filters('korgou_package_get_image_list', [], ['packageid' => $id]),
            ];
            $this->view('detail', $context);
        }
    }

    function load_assistpurchase()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('id', 'userid', 'status', 'applytime_start', 'applytime_end');
        $conditions = [];
        if (isset($args['id']) && !empty($args['id']))
            $conditions[] = ['id', '=', $args['id']];
        if (isset($args['userid']) && !empty($args['userid']))
            $conditions[] = ['userid', '=', $args['userid']];
        if (isset($args['status']) && !empty($args['status']))
            $conditions[] = ['status', '=', $args['status']];
        if (isset($args['applytime_start']) && !empty($args['applytime_start']))
            $conditions[] = ['applytime', '>=', $args['applytime_start'] . ' 00:00:00'];
        if (isset($args['applytime_end']) && !empty($args['applytime_end']))
            $conditions[] = ['applytime', '<=', $args['applytime_end'] . ' 23:59:59'];

        $page = apply_filters('korgou_purchase_get_purchase_page', null, $args, $conditions, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {

            $data[] = [
                sprintf('<a class="link btn btn-link purchase-btn" href="/assistpurchase/detail/?id=%1$s" target="_blank">%1$s</a>', $item->id),
                korgou_user_role_id($item->userid),
                ($item->siteurl != null && Shoplic_Util::starts_with($item->siteurl, 'http')) ?
                    sprintf('<a href="%1$s" class="extern" target="_blank">%1$s</a>', $item->siteurl) : $item->siteurl,
                $item->loginusername,
                $item->purchasetype == '1' ?
                    sprintf('<button type="button" class="decryptpw-btn btn btn-sm btn-light" data-id="%s">View password</button>', $item->id) : $item->loginuserpsw,
                $item->goodsmoney,
                $item->expectcommission,
                $item->discount < 100 ? sprintf('<b class="text-info">%d</b>', $item->discount) : $item->discount . '%',
                $item->adjustment,
                $item->realmoney,
                $item->refundmoney,
                $item->goodsoutstock == '1' ? 'Skip and continue to checkout' :
                    ($item->goodsoutstock == '2' ? 'Pause the payment' : ''),
                $item->otherdemands,
                Korgou_Purchase::$PURCHASETYPES[$item->purchasetype] ?? '',
                Korgou_Purchase::$STATUSES[$item->status] ?? '',
                $item->applytime,
                $item->checktime,
                $item->donetime,
                $item->remark,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function calculate_commission()
    {
        $purchaseid = $_POST['id'];
        $goodsmoney = $_POST['goodsmoney'];

        $commission = apply_filters('korgou_purchase_calculate_commission', 0, $purchaseid, $goodsmoney);

        wp_send_json_success($commission);
    }

    function deduct_money()
    {
        $id = $_POST['id'];
        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');

        if ($purchase->status != Korgou_Purchase::STATUS_WAIT_PAY)
            wp_send_json_error('Purchase status is not WAIING PAYMENT.');

        if ($purchase->goodsmoney <= 0 || $purchase->expectcommission < 0 || $purchase->realmoney <= 0)
            wp_send_json_error('Invalid value');
        
        if ($purchase->goodsmoney + $purchase->expectcommission + $purchase->adjustment != $purchase->realmoney)
            wp_send_json_error('金额比对失败');

        $balance = apply_filters('korgou_user_balance_get_balance', null, ['userid' => $purchase->userid]);

        if ($balance != null && $balance->balance >= $purchase->realmoney) {
            $data = [
                'id' => $purchase->id,
                'status' => Korgou_Purchase::STATUS_PAY_SUCC,
            ];
            do_action('korgou_purchase_update_purchase', $data);

            do_action('korgou_user_balance_change_balance',
                $purchase->id, $purchase->userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $purchase->realmoney, 'KRW', 'Assisted purchase', '', '', 'sys', 'sys', '');

            kg_send_json_success();
        } else {
            wp_send_json_error('Insufficient balance');
        }
    }

    function cancel_transfer()
    {
        $id = $_POST['id'];
        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');

        if (!$purchase->is_status_processing())
            wp_send_json_error('Purchase is not PROCESSING.');

        do_action('korgou_purchase_update_purchase', [
            'id' => $id, 
            'remark' => $_POST['remark'],
            'status' => Korgou_Purchase::STATUS_CANCEL,
            'donetime' => current_time('mysql'),
        ]);

        kg_send_json_success();
    }

    function refund()
    {
        $id = $_GET['id'] ?? '';
        if (!empty($id)) {
            $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]);
            switch ($purchase->status) {
                case Korgou_Purchase::STATUS_PAY_SUCC:
                    $refundmoney = $purchase->realmoney;
                    break;
                case Korgou_Purchase::STATUS_PAY_SUCC:
                    $refundmoney = $purchase->realmoney - $purchase->expectcommission;
                    break;
                default:
                    $refundmoney = 0;
                    break;
            }
            $context = [
                'purchase' => apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]),
                'refundmoney' => $refundmoney,
            ];
            $this->view('refund', $context);
        }
    }

    function refund_purchase()
    {
        $id = $_POST['id'];
        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');

        if (!$purchase->is_status_before_refund())
            wp_send_json_error('Purchase is not ready for refund.');

        $refundmoney = intval($_POST['refundmoney']);
        if ($refundmoney <= 0)
            wp_send_json_error('Refund money should be greather than 0.');

        do_action('korgou_purchase_update_purchase', [
            'id' => $id, 
            'refundmoney' => $refundmoney,
            'status' => Korgou_Purchase::STATUS_REFUNDED,
        ]);

        // this.userBalanceService.changeBalance(record.getId(), record.getUserid(), BalanceType.BALANCE, refundmoney, Currency.KRW, "refund", "", "", "sys", "sys", "");
        do_action('korgou_user_balance_change_balance',
            $purchase->id, $purchase->userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, $refundmoney, 'KRW', 'Refunded', '', '', 'sys', 'sys', '');

        kg_send_json_success();
    }

    function decrypt_pw()
    {
        $id = $_POST['id'] ?? '';
        if (!empty($id)) {
            $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $id]);
            if ($purchase != null) {
                $decrypted = apply_filters('aes_decrypt', $purchase->loginuserpsw);
                wp_send_json_success($decrypted);
            }
        }

        wp_send_json_error('Purchase not available');
    }

    function change_buystatus()
    {
        $data = Shoplic_Util::get_post_data('id', 'buystatus');

        $purchase = apply_filters('korgou_purchase_get_purchase', null, ['id' => $_POST['purchaseid']]);
        if ($purchase == null)
            wp_send_json_error('Purchase not available');
        
        do_action('korgou_purchase_update_goods', $data);

        kg_send_json_success();
    }

    function load_assistpurchase_direct()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('id', 'userid', 'status', 'applytime_start', 'applytime_end');
        $conditions = [];
        $conditions[] = ['shopplace', '=', Korgou_Purchase::SHOPPLACE_KORGOU];

        if (isset($args['id']) && !empty($args['id']))
            $conditions[] = ['id', '=', $args['id']];
        if (isset($args['userid']) && !empty($args['userid']))
            $conditions[] = ['userid', '=', $args['userid']];
        if (isset($args['status']) && !empty($args['status']))
            $conditions[] = ['status', '=', $args['status']];
        if (isset($args['applytime_start']) && !empty($args['applytime_start']))
            $conditions[] = ['applytime', '>=', $args['applytime_start'] . ' 00:00:00'];
        if (isset($args['applytime_end']) && !empty($args['applytime_end']))
            $conditions[] = ['applytime', '<=', $args['applytime_end'] . ' 23:59:59'];

        $page = apply_filters('korgou_purchase_get_purchase_page', null, $args, $conditions, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {

            $goods_list = apply_filters('korgou_purchase_get_goods_list', [], ['purchaseid' => $item->id]);
            $i = 0;
            foreach ($goods_list as $goods) {
                $i++;
                if ($i == 1) {
                    $data[] = [
                        sprintf('<a class="link purchase-btn" href="/assistpurchase/detail/?id=%1$s" target="_blank">%1$s</a>', $item->id),
                        korgou_user_role_id($item->userid),
                        $item->applytime,
                        $goods->goods,
                        number_format($goods->price),
                        $goods->piece,
                        number_format($goods->price * $goods->piece),
                        number_format($item->goodsmoney),
                        Korgou_Purchase::$STATUSES[$item->status] ?? '',
                    ];
                } else {
                    $data[] = [
                        '',
                        '',
                        '',
                        $goods->goods,
                        number_format($goods->price),
                        $goods->piece,
                        number_format($goods->price * $goods->piece),
                        '',
                        '',
                    ];
                }
            }
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

};
