<?php
// print_r($purchase);
?>
<div class="row">
	<div class="col-12">
		<div class="card-box">
			<table class="table table-bordered">
				<thead class="thead-light">
					<tr class="even">
						<th>Number</th>
						<th>User ID</th>
						<th>Web site address/Name of bank</th>
						<th>Login/Bank Account Name</th>
						<th>Password/Bank account</th>
						<th>Amount</th>
						<th>Service charge</th>
						<th>Sale</th>
						<th>Price adjustment</th>
						<th>Total cost</th>
						<th>Refunds</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?php echo $purchase->id; ?></td>
						<td id="data-userid">
							<?php echo korgou_user_role_id($purchase->userid); ?>
						</td>
						<td>
							<?php if (Shoplic_Util::starts_with($purchase->siteurl, 'http')): ?>
								<a href="<?php echo $purchase->siteurl; ?>" class="extern" target="_blank"><?php echo $purchase->siteurl; ?></a>
							<?php else: ?>
								<?php echo $purchase->siteurl; ?>
							<?php endif; ?>
						</td>
						<td><?php echo $purchase->loginusername; ?></td>
						<td>
							<?php if ($purchase->purchasetype == '1'): ?>
								<button type="button" class="decryptpw-btn btn btn-sm btn-light" data-id="<?php echo $purchase->id; ?>">View password</button>
							<?php else: ?>
								<?php echo $purchase->loginuserpsw; ?>
							<?php endif; ?>
						</td>
						<td><?php echo $purchase->goodsmoney; ?></td>
						<td><?php echo $purchase->expectcommission; ?></td>
						<td>
							<?php if ($purchase->discount < 100): ?>
								<b class="text-info"><?php echo $purchase->discount; ?>%</b>
							<?php else: ?>
								<?php echo $purchase->discount; ?>%
							<?php endif; ?>
						</td>
						<td><?php echo $purchase->adjustment; ?></td>
						<td id="data-realmoney"><?php echo $purchase->realmoney; ?></td>
						<td><?php echo $purchase->refundmoney; ?></td>
					</tr>
				</tbody>
			</table>
			<table class="table table-bordered">
				<thead class="thead-light">
					<tr class="even">
						<th>Out of stock</th>
						<th>Other requirements</th>
						<th>Type of Service</th>
						<th>Status</th>
						<th>Domestic tracking number</th>
						<th>Application time</th>
						<th>Check time</th>
						<th>Completion time</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php if ($purchase->goodsoutstock == '1'): ?>
									Skip and continue to checkout
							<?php elseif ($purchase->goodsoutstock == '2'): ?>
									Pause the payment
							<?php endif; ?>
						</td>
						<td><?php echo $purchase->otherdemands; ?></td>
						<td>
							<?php echo $purchase->get_purchasetype_name(); ?>
						</td>
						<td><?php echo Korgou_Purchase::$STATUSES[$purchase->status]; ?></td>
						<td><?php echo $purchase->domestictrackno; ?></td>
						<td><?php echo $purchase->applytime; ?></td>
						<td><?php echo $purchase->checktime; ?></td>
						<td><?php echo $purchase->donetime; ?></td>
						<td><?php echo $purchase->remark; ?></td>
					</tr>
				</tbody>
			</table>
			<div class="text-center">
				<?php if ($purchase->status == Korgou_Purchase::STATUS_WAIT_CHECK): ?>
					<a class="link btn btn-primary mr-2" href="/assistpurchase/confirmsucc/?id=<?php echo $purchase->id; ?>">Confirm success and billing</a>
					<a class="link btn btn-warning mr-2" href="/assistpurchase/confirmfail/?id=<?php echo $purchase->id; ?>">Confirmation failed</a>
				<?php endif; ?>

				<?php if ($purchase->status == Korgou_Purchase::STATUS_WAIT_PAY): ?>
					<button class="btn btn-info mr-2 deduct-money-btn">Deduction</button>
				<?php endif; ?>

				<a class="link btn btn-success mr-2" href="/assistpurchase/transucc/?id=<?php echo $purchase->id; ?>">Transaction success</button>
				<a class="link btn btn-pink mr-2" href="/assistpurchase/tranfail/?id=<?php echo $purchase->id; ?>">Transaction failed</button>
				<a class="link btn btn-secondary mr-2" href="/assistpurchase/uploadimage/?id=<?php echo $purchase->id; ?>">Upload screenshot</button>
				<a class="link btn btn-dark mr-2" href="/assistpurchase/transcancel/?id=<?php echo $purchase->id; ?>">Cancel transaction</a>
				<a class="link btn btn-danger" href="/assistpurchase/refund/?id=<?php echo $purchase->id; ?>">Refunds</a>
			</div>
		</div>

		<?php if ($purchase->purchasetype != Korgou_Purchase::PURCHASETYPE_ZHUAN_ZHANG): ?>
			<div class="card-box">
				<h4 class="card-title">Items</h4>
				<table class="table table-bordered">
					<thead>
						<tr class="even">
							<th>#</th>
							<th>Item</th>
							<th>Quantity</th>
							<th>Unit price</th>
							<th>Color</th>
							<th>Size</th>
							<th>Other commodity attributes</th>
							<th>Purchase status</th>
							<!--
							<th>Change purchase status</th>
							-->
						</tr>
					</thead>
					<tbody>
						<?php $i = 0; $total = 0; foreach ($goods as $item): $i++; $total += $item->piece; ?>
							<tr>
								<td><?php echo $i; ?></td>
								<td>
									<?php if (Shoplic_Util::starts_with($item->goodsurl, 'http')): ?>
										<a href="<?php echo $item->goodsurl; ?>" class="extern" target="_blank"><?php echo $item->goods ?? $item->goodsurl; ?></a>
									<?php else: ?>
										<?php echo $item->goodsurl; ?>
									<?php endif; ?>
								<td><?php echo $item->piece; ?></td>
								<td><?php echo $item->price; ?></td>
								<td><?php echo $item->color; ?></td>
								<td><?php echo $item->goodssize; ?></td>
								<td><?php echo $item->goodsoptions; ?></td>
								<!--
								<td><?php echo Korgou_Purchase_Goods::$BUYSTATUSES[$item->buystatus]; ?>
								</td>
								-->
								<td data-id="<?php echo $item->id; ?>">
									<?php BS_FORM::select([
										'name' => 'buystatus',
										'options' => Korgou_Purchase_Goods::$BUYSTATUSES,
										'value' => $item->buystatus,
										'blank_option' => '---',
									]); ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				Total Items: <?php echo $total; ?>
			</div>
		<?php endif; ?>

		<div class="card-box">
			<?php do_action('korgou_we_package_image_list', $purchase->id); ?>
		</div>

	</div>
</div>

<div class="row text-center mb-3">
	<div class="col">
		<button type="button" class="btn btn-secondary cancel-btn">Back</button>
	</div>
</div>

<?php $this->ajax_form('deduct_money', ['id' => 'deduct-money-form']); ?>
    <input type="hidden" name="id" value="<?php echo $purchase->id; ?>">
</form>

<?php $this->ajax_form('change_buystatus', ['id' => 'change-buystatus-form']); ?>
    <input type="hidden" name="purchaseid" value="<?php echo $purchase->id; ?>">
    <input type="hidden" name="id" value="">
    <input type="hidden" name="buystatus" value="">
</form>

<script type="text/javascript">
jQuery(function($) {
	$('.deduct-money-btn').click(function() {
        // if (confirm('你确定从' + $('#data-userid').text() + '扣除' + $('#data-realmoney').text() + '吗？')) {
        if (confirm('Are you sure to deduct ' + $('#data-realmoney').text() + ' from ' + $.trim($('#data-userid').text()) + '?')) {
			$('#deduct-money-form').ajaxSubmit(function(response) {
                alert(response.data);
				if (response.success)
                	$('#section-2').trigger('reload');
			});
        }
        return false;
    });

	$('select[name="buystatus"]').change(function() {
		if (confirm('Are you sure to modify the purchase status?')) {
			$('#change-buystatus-form input[name="buystatus"]').val($(this).val());
			$('#change-buystatus-form input[name="id"]').val($(this).parent().data('id'));
			$('#change-buystatus-form').ajaxSubmit(function(response) {
                alert(response.data);
			});
        }
        return false;
	});
});
</script>
