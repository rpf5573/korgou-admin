<div class="card-box">
    <h5 class="card-title">Refunds</h5>

    <?php $this->ajax_form('refund_purchase'); ?>

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>Number</th>
                <td>
                    <input type="text" class="form-control-plaintext" name="id" value="<?php echo $purchase->id; ?>" readonly>
                </td>
            </tr>
            <tr>
                <th>Status</th>
                <td>
                    <input type="text" class="form-control-plaintext" value="<?php echo Korgou_Purchase::$STATUSES[$purchase->status]; ?>" readonly>
                </td>
            </tr>
            <tr>
				<td>Amount</td>
                <td>
                    <input type="text" class="form-control-plaintext" name="goodsmoney" value="<?php echo $purchase->goodsmoney; ?>" readonly>
                </td>
			</tr>
			<tr>
				<td>Service charge</td>
                <td>
                    <input type="text" class="form-control-plaintext" name="expectcommission" value="<?php echo $purchase->expectcommission; ?>" readonly>
                </td>
			</tr>
			<tr>
				<td>Price adjustment</td>
                <td>
                    <input type="text" class="form-control-plaintext" name="adjustment" value="<?php echo $purchase->adjustment; ?>" readonly>
                </td>
			</tr>
			<tr>
				<td>Total cost</td>
                <td>
                    <input type="text" class="form-control-plaintext" name="realmoney" value="<?php echo $purchase->realmoney; ?>" readonly>
				</td>
			</tr>
			<tr>
				<td>Refund Amount</td>
                <td>
                    <input type="text" class="form-control" name="refundmoney" value="<?php echo $refundmoney; ?>">
				</td>
			</tr>
        </tbody>
        </table>

        <p>
            <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
            <button type="button" class="btn btn-primary refund-btn">Save</button>
        </p>

    </form>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.refund-btn').click(function() {
		if ($('input[name="refundmoney"]') == '') {
			alert("请输入退款金额");
			return false;
		}
        $(this).closest('form').ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-3').html('');
                $('#section-2').trigger('reload');
            }
        });
        return false;
    });
});
</script>
