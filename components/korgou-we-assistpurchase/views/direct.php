<style>
a:visited:extern {
  color: grey !important;
}
a.extern:visited {
  color: grey !important;
}
</style>

<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                    'name' => 'id', 'type' => 'text', 'label' => 'ID'
                ], [
                    'name' => 'userid', 'type' => 'text', 'label' => 'User ID'
                ], [
                    'name' => 'status', 'type' => 'select', 'label' => 'Status', 'options' => Korgou_Purchase::$STATUSES, // 'value' => Korgou_Purchase::STATUS_WAIT_CHECK,
                ], [
                    'name' => ['applytime_start', 'applytime_end'], 'type' => 'period', 'label' => 'Application time'
                ]]);
                ?>

                <?php
                do_action('korgou_we_datatable', [
                    'ID', 'User ID', 'Application time', 'Product', 'Price', 'Quantity', 'Subtotal', 'Total', 'Status'
                ], [],
                    'assistpurchase_load_assistpurchase_direct'
                );
                ?>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<section id="section-3"></section>

<div class="modal" tabindex="-1" role="dialog" id="action-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    </div>
  </div>
</div>

<?php $this->ajax_form('decrypt_pw'); ?>
    <input type="hidden" name="id" id="decrypt-purchaseid">
</form>

<script type="text/javascript">
jQuery(function($) {

    $('section').on('click', 'a.link', function() {
        var href = $(this).attr('href');
        var $section = $(this).closest('section');
        $section.hide();
        $section.next().data('href', href).load(href);
        return false;
    });
    $('section').on('click', '.cancel-btn', function() {
        var $section = $(this).closest('section');
        $section.html('');
        $section.prev().show();
    });
    $('section').on('click', '.decryptpw-btn', function() {
        var self = this;
        $('#decrypt-purchaseid').val($(this).data('id')).closest('form').ajaxSubmit(function(response) {
            $(self).parent().text(response.data);
        });
    });
    $('#section-2').on('reload', function() {
        var href = $(this).data('href');
        $('#section-2').load(href).show();
    });

    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });
    $(document).on('click', '.modal-submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $form.find('.close').click();
            }
        });
        return false;
    });
    $('#section-2').on('click', '.modal-btn', function() {
        $('.modal-content').load($(this).data('href'));
    });
});
</script>
