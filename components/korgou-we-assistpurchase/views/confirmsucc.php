<div class="card-box">
    <h5 class="card-title">Confirm success and billing</h5>

    <?php $this->ajax_form('confirm_success'); ?>

        <input type="hidden" name="discount" value="<?php echo $purchase->discount; ?>">

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>Number</th>
                <td>
                    <input type="text" class="form-control-plaintext" name="id" value="<?php echo $purchase->id; ?>" readonly>
                </td>
            </tr>
            <tr>
                <th>Amount</th>
                <td>
                    <input type="text" class="form-control calculate-commission" name="goodsmoney" value="<?php echo $purchase->goodsmoney; ?>">
                </td>
            </tr>
            <tr>
                <th>Service charge</th>
                <td>
                    <input type="text" class="form-control calculate-realmoney" name="expectcommission" value="<?php echo $purchase->expectcommission; ?>" required>
                </td>
            </tr>
            <!--
            <tr>
                <th>Sale</th>
                <td>
                    <div class="input-group">
                        <input type="text" class="form-control calculate-realmoney" name="discount" value="<?php echo $purchase->discount; ?>">
                        <div class="input-group-append">
                            <span class="input-group-text">%</span>
                        </div>
                    </div>
                </td>
            </tr>
            -->
            <tr>
                <th>Price adjustment</th>
                <td>
                    <input type="text" class="form-control calculate-realmoney" name="adjustment" value="<?php echo $purchase->adjustment; ?>">
                </td>
            </tr>
            <tr>
                <th>Total cost</th>
                <td>
                    <input type="text" class="form-control" name="realmoney" value="<?php echo $purchase->realmoney; ?>" readonly>
                </td>
            </tr>
        </tbody>
        </table>

        <p>
            <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
            <button type="button" class="btn btn-primary confirmsucc-btn">Save</button>
        </p>

    </form>
</div>

<?php $this->ajax_form('calculate_commission'); ?>
    <input type="hidden" name="id" value="<?php echo $purchase->id; ?>">
    <input type="hidden" name="goodsmoney" id="calculate-goodsmoney">
</form>

<script type="text/javascript">
jQuery(function($) {
    // $('#section-3').on('click', '.confirmsucc-btn', function() {
    $('.confirmsucc-btn').click(function() {
        if ($('input[name="goodsmoney"]').val() == '') {
			alert("请输入代付金额");
			return false;
		}
        if ($('input[name="expectcommission"]').val() == '') {
			alert("请输入服务费");
			return false;
		}
        if ($('input[name="adjustment"]').val() == '') {
			alert("请输入价格调整");
			return false;
		}
        if ($('input[name="realmoney"]').val() == '') {
			alert("请输入总收费");
			return false;
		}
        $(this).closest('form').ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $('#section-3').html('');
                $('#section-2').trigger('reload');
            }
        });
        return false;
    });

    $('.calculate-commission').blur(function() {
        calculateCommission();
    });
    $('.calculate-commission').keyup(function() {
        calculateCommission();
    });

    $('.calculate-realmoney').blur(function() {
        calculateRealmoney();
    });
    $('.calculate-realmoney').keyup(function() {
        calculateRealmoney();
    });

    function calculateCommission() {
        var goodsmoney = $('input[name="goodsmoney"]').val();
        if (goodsmoney != '') {
            $('#calculate-goodsmoney').val(goodsmoney).closest('form').ajaxSubmit(function(response) {
                $('input[name="expectcommission').val(response.data);
                calculateRealmoney();
            });
        }

        return false;
    }

    function calculateRealmoney() {
        $('input[name="realmoney"]').val(
				Number($('input[name="goodsmoney"]').val())
						+ Number($('input[name="expectcommission"]').val())
						+ Number($('input[name="adjustment"]').val()));
    }
});
</script>