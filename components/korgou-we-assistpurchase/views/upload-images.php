<div class="card-box">
    <h5 class="card-title">Upload screenshot</h5>

    <?php $this->ajax_form('upload_images'); ?>

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>Number</th>
                <td>
                    <input type="text" class="form-control-plaintext" name="id" value="<?php echo $purchase->id; ?>" readonly>
                </td>
            </tr>
        </tbody>
        </table>

        <?php do_action('korgou_we_package_upload_image_form'); ?>

        <p>
            <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
            <button type="button" class="btn btn-primary upload-btn">Save</button>
        </p>

    </form>
</div>

<script type="text/javascript">
jQuery(function($) {
    // $('#section-3').on('click', '.confirmsucc-btn', function() {
    $('.upload-btn').click(function() {
        $(this).closest('form').ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-3').html('');
                $('#section-2').trigger('reload');
            }
        });
        return false;
    });
});
</script>
