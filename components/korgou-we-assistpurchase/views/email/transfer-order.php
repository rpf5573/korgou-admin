Your assisted purchase/payment order has been successfully processed. Please refer to the attached screenshot for more details. For assisted payment orders, you can directly log into the shopping site and check the order status.<br>
Generally it takes two or three business days for the purchased items to be delivered to the KorGou warehouse. You may submit your package forward application once all the items are delivered.<br>
<br>
您的代购/代付订单已经处理完毕，详情请参考截图，代付订单请直接登录购物网站确认订单状态。<br>
一般网站发货后两到三个工作日将会抵达KorGou仓库，商品入库后，请您根据系统发送的入库通知邮件进行后续转运申请操作。如有任何问题，请在KorGou客户支持中心提交工单或直接回复邮件。<br>
KorGou祝您生活愉快！<br>
<br>
* Regarding Assisted purchase and payment services, we are not responsible for any possible default from sellers. We only do those services on behalf of customers but customers should have all responsibility on the transaction.<br>
<br>
*关于辅助采购和付款服务,我们不负责有关商品的任何瑕疵等问题。我们只替客户采购客户所要求的商品以及支付其商品费用，其他有关采购商用的瑕疵 等 风险是需要由客户来承担以及与卖家协商处理。<br>
<br>
<br>
<table cellspacing="0" cellpadding="6" style="width: 100%;" border="1">
    <tbody>
    <tr>
        <th>Order ID</th>
        <td><?php echo $purchase->id; ?></td>
    </tr>
    <tr>
        <th>Name of Bank</th>
        <td><?php echo $purchase->siteurl; ?></td>
    </tr>
    <tr>
        <th>Account Holder</th>
        <td><?php echo $purchase->loginusername; ?></td>
    </tr>
    <tr>
        <th>Account Number</th>
        <td><?php echo $purchase->loginuserpsw; ?></td>
    </tr>
    <tr>
        <th>Amount</th>
        <td><?php echo $purchase->goodsmoney; ?></td>
    </tr>
    <tr>
        <th>Service charge</th>
        <td><?php echo $purchase->expectcommission; ?></td>
    </tr>
    <tr>
        <th>Total payment</th>
        <td><?php echo $purchase->realmoney; ?></td>
    </tr>
    <tr>
        <th>Status</th>
        <td><?php echo $purchase->get_status_name(); ?></td>
    </tr>
    <tr>
        <th>Application Time</th>
        <td><?php echo $purchase->applytime; ?></td>
    </tr>
    <tr>
        <th>Completion Time</th>
        <td><?php echo $purchase->donetime; ?></td>
    </tr>
    <tr>
        <th>Remark</th>
        <td><?php echo $purchase->remark; ?></td>
    </tr>
    </tbody>
</table>
