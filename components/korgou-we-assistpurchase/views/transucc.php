<div class="card-box">
    <h5 class="card-title">Transaction success</h5>

    <?php $this->ajax_form('transfer_success', ['enctype' => 'multipart/form-data']); ?>

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>Number</th>
                <td>
                    <input type="text" class="form-control-plaintext" name="id" value="<?php echo $purchase->id; ?>" readonly>
                </td>
            </tr>
            <tr>
                <th>Remarks</th>
                <td>
                    <textarea name="remark" class="form-control" rows="5"></textarea>
                </td>
            </tr>
            <tr>
                <th>Images</th>
                <td id="attachments">
                    <button type="button" class="btn btn-info mb-1" id="attach-btn">Add attachment</button>
                    <input type="file" class="d-block mb-1" name="file[]">
                    <input type="file" class="d-block mb-1" name="file[]">
                    <input type="file" class="d-block mb-1" name="file[]">
                    <input type="file" class="d-block mb-1" name="file[]">
                    <input type="file" class="d-block mb-1" name="file[]">
                </td>
            </tr>
        </tbody>
        </table>

        <p>
            <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
            <button type="button" class="btn btn-primary transucc-btn">Save</button>
        </p>

    </form>
</div>

<script type="text/javascript">
jQuery(function($) {
    // $('#section-3').on('click', '.confirmsucc-btn', function() {
    $('.transucc-btn').click(function() {
        $(this).closest('form').ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $('#section-3').html('');
                $('#section-2').trigger('reload');
            }
        });
        return false;
    });

    $('#attach-btn').click(function() {
        $('#attachments').append('<input type="file" class="d-block mb-1" name="file[]">');
        return false;
    });
});
</script>
