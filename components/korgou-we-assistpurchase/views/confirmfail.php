<div class="card-box">
    <h5 class="card-title">Confirmation failed</h5>

    <?php $this->ajax_form('confirm_fail'); ?>

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>Number</th>
                <td>
                    <input type="text" class="form-control-plaintext" name="id" value="<?php echo $purchase->id; ?>" readonly>
                </td>
            </tr>
            <tr>
                <th>Remarks</th>
                <td>
                    <textarea name="remark" class="form-control" rows="5"></textarea>
                </td>
            </tr>
        </tbody>
        </table>

        <p>
            <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
            <button type="button" class="btn btn-primary confirmfail-btn">Save</button>
        </p>

    </form>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.confirmfail-btn').click(function() {
        $(this).closest('form').ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $('#section-3').html('');
                $('#section-2').trigger('reload');
            }
        });
        return false;
    });
});
</script>
