<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_user';

    function load()
    {
        $this->add_shortcode('usermanage');
        $this->add_shortcode('edituserinfo');
        $this->add_shortcode('userbalancerecord');
        $this->add_shortcode('manualaddmoney');
        $this->add_shortcode('membership');
        $this->add_ajax('load_user');
        $this->add_ajax('load_balancerecord');
        $this->add_ajax('find_user');
        $this->add_ajax('update_user');
        $this->add_ajax('change_status');
        $this->add_ajax('change_balance_state');
        $this->add_ajax('reset_password');
        $this->add_ajax('add_money');
        $this->add_ajax('reset_membership');
    }

    function usermanage()
    {
        $this->view('usermanage', []);
    }

    function membership()
    {
        $this->view('membership', []);
    }

    function edituserinfo()
    {
        $context = [];
        $userid = $_GET['userid'] ?? '';
        if (!empty($userid)) {
            $user = apply_filters('korgou_user_get_user', null, ['userid' => $userid]);
            if ($user != null) {
                $context['user'] = $user;
                $this->view('edituserinfo', $context);
            }
        }
    }

    function userbalancerecord()
    {
        $context = [];
        $this->view('userbalancerecord', $context);
    }

    function load_user()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('userid', 'email', 'englishname', 'nativename', 'status');
        $conditions = [];
        if (!empty($args['userid']))
            $conditions[] = ['userid', '=', $args['userid']];
        if (!empty($args['status']))
            $conditions[] = ['status', '=', $args['status']];
        if (!empty($args['email']))
            $conditions[] = ['email', 'LIKE', '%' . $args['email'] . '%'];
        if (!empty($args['englishname']))
            $conditions[] = ['englishname', 'LIKE', '%' . $args['englishname'] . '%'];
        if (!empty($args['nativename']))
            $conditions[] = ['nativename', 'LIKE', '%' . $args['nativename'] . '%'];

        $page = apply_filters('korgou_user_get_user_and_balance_page', [], [], $conditions, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $change_status = '<div class="form-row"><div class="col">' . BS_FORM::select([
                'id' => 'user-status-' . $item->userid,
                'class' => 'form-control-sm',
                'style' => 'width: inherit;',
                'options' => Korgou_User::$STATUSES,
                'value' => $item->status,
                'blank_option' => '- Please choose -',
            ], false) . '</div><div class="col-auto"><button type="button" class="btn btn-sm btn-success change-status-btn" data-userid="' . $item->userid . '">Change</button></div></div>';
            
            $change_balancestate = '<div class="form-row"><div class="col-auto">' . BS_FORM::select([
                'id' => 'balance-state-' . $item->userid,
                'class' => 'form-control-sm',
                'style' => 'width: inherit;',
                'options' => Korgou_User_Balance::$STATES,
                'value' => $item->state,
                'blank_option' => '- Please choose -',
            ], false) . '</div><div class="col-auto"><button type="button" class="btn btn-sm btn-success change-balance-state-btn" data-userid="' . $item->userid . '">Change</button></div></div>';
            /*
            if ($item->photo == '1')
                $photo = 'Not applicable';
            elseif ($item->photo == '2')
                $photo = 'Already applied<button type="button" class="btn btn-sm btn-blue ml-1" data-packageid="' . $item->packageid . '">Upload photos</button>';
            elseif ($item->photo == '3') {
                $photo = 'Already applied<button type="button" class="btn btn-sm btn-blue" data-packageid="' . $item->packageid . '">Upload photos</button>' .
                    '<button type="button" class="btn btn-sm btn-pink ml-1" data-packageid="' . $item->packageid . '">View photos</button>';
            } else
                $photo = 'Unknown';
            
            if ($item->checkliststatus == '1')
                $checkliststatus = 'Not applicable';
            elseif ($item->checkliststatus == '2')
                $checkliststatus = 'Already applied<button type="button" class="btn btn-sm btn-warning ml-1" data-packageid="' . $item->packageid . '">Sorting</button>';
            elseif ($item->checkliststatus == '3')
                $checkliststatus = '<button type="button" class="btn btn-sm btn-warning view-package-check-btn" data-packageid="' . $item->packageid . '">Sorting completed</button>';
            elseif ($item->checkliststatus == '4')
                $checkliststatus = '<button type="button" class="btn btn-sm btn-dark view-package-check-btn" data-packageid="' . $item->packageid . '">Sorting failed</button>';
            else
                $checkliststatus = 'Unknown';
                */

            $action = '';
            if (current_user_can('kg_add_user_balance'))
                $action .= sprintf('<a class="btn btn-success btn-sm add-money-btn" href="/user/manualaddmoney/?userid=%s">Add balance</a>', $item->userid);
            if (current_user_can('kg_edit_user'))
                $action .= sprintf('<button class="btn btn-warning btn-sm reset-password-btn mr-2" data-userid="%s">Reset password</button>', $item->userid);

            $data[] = [
                '<span class="d-flex align-items-center">' . korgou_user_role_icon($item->userid) . sprintf('<a class="user-btn" href="/user/edituserinfo/?userid=%1$s">%1$s</a>', $item->userid) . '</span>',
                number_format($item->balance),
                $item->email,
                $item->englishname,
                $item->nativename,
                $item->gender,
                $item->country,
                $item->phonenum,
                $item->mobilenum,
                $item->facebookqq,
                // $item->skype,
                $item->registertime,
                Korgou_User::$STATUSES[$item->status],
                $change_status,
                Korgou_User_Balance::$STATES[$item->state],
                $change_balancestate,
                $action,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function load_balancerecord()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('userid', 'tradeno', 'outorderno', 'tradetime_start', 'tradetime_end');
        $conditions = [];
        if (isset($args['userid']) && !empty($args['userid']))
            $conditions[] = ['userid', '=', $args['userid']];
        if (isset($args['tradeno']) && !empty($args['tradeno']))
            $conditions[] = ['tradeno', '=', $args['tradeno']];
        if (isset($args['outorderno']) && !empty($args['outorderno']))
            $conditions[] = ['outorderno', '=', $args['outorderno']];
        if (isset($args['tradetime_start']) && !empty($args['tradetime_start']))
            $conditions[] = ['tradetime', '>=', $args['tradetime_start'] . ' 00:00:00'];
        if (isset($args['tradetime_end']) && !empty($args['tradetime_end']))
            $conditions[] = ['tradetime', '<=', $args['tradetime_end'] . ' 23:59:59'];

        $page = apply_filters('korgou_user_balance_get_balance_record_page', [], [], $conditions, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $data[] = [
                $item->tradeno,
                $item->outorderno,
                korgou_user_role_id($item->userid),
                $item->currency,
                $item->realbalance,
                $item->tradedesc,
                $item->tradefrom,
                $item->tradeto,
                $item->tradetime,
                $item->staffname,
                $item->remark,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function update_user()
    {
        $data = Shoplic_Util::get_post_data('userid', 'email', 'englishname', 'nativename', 'gender', 'country', 'phonenum', 'mobilenum', 'facebookqq', 'skype');
        do_action('korgou_user_update_user', $data);

        kg_send_json_success();
    }

    function change_status()
    {
        $data = Shoplic_Util::get_post_data('userid', 'status');
        do_action('korgou_user_update_user', $data);

        kg_send_json_success();
    }

    function change_balance_state()
    {
        $data = Shoplic_Util::get_post_data('userid', 'state');
        do_action('korgou_user_balance_update_balance', $data);

        do_action('boss_staff_insert_log', [
            'logtype' => '改变用户资金状态',
            'logcontent' => $data['userid'],
            'linkid' => $data['userid'],
        ]);

        kg_send_json_success();
    }

    function reset_password()
    {
        $userid = $_POST['userid'];
        $pwd = Shoplic_Util::random_lower_numeric(6);

        $user = get_user_by('login', $userid);
        wp_set_password($pwd, $user->ID);

        kg_send_json_success($pwd);
    }

    function manualaddmoney()
    {
        $user = apply_filters('korgou_user_get_user', null, ['userid' => $_GET['userid']]);
        $this->view('manualaddmoney', ['user' => $user]);
    }
    
    function add_money()
    {
        $userid = $_POST['userid'];
        $remark = $_POST['remark'];
        $inputmoney = floatval($_POST['inputmoney']);
        $currency = $_POST['currency'];
        $money = floatval($_POST['money']);

        if ($currency != 'KRW') {
            $currency_exchange = apply_filters('korgou_currency_get_exchange', null, ['currency' => $currency]);
            if ($currency_exchange == null || $currency_exchange->exchangekrw == null || $currency_exchange->exchangekrw <= 0)
                wp_send_json_error('Description cannot be empty');

            $calcmoney = $inputmoney * $currency_exchange->exchangekrw;
            $diff = $calcmoney - $money;
            $ff = round(bcdiv(abs($diff), abs($money), 5), 4, PHP_ROUND_HALF_DOWN);

            if ($ff > 0.05)
                wp_send_json_error("The deviation of the converted additional payment amount is too large");

        } else if ($currency == 'KRW') {
            if ($inputmoney != $money) {
                wp_send_json_error('Price and actual increase must be equal.');
            }
        } else {
            wp_send_json_error($currency . ' not available');
        }

        $staffid = KG::get_current_userid(); // KG::get_current_staff();

        do_action('korgou_user_balance_change_balance',
            '', $userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, $money, 'KRW', 'Manual Add Money', '', '', $staffid, '', $remark
        );

        // this.bossStaffLogService.save("手动加款", form.getUserid() + form.getMoney() + " " + form.getRemark(), form.getUserid(), request, getSession(session));
        do_action('boss_staff_insert_log', [
            'logtype' => '手动加款',
            'logcontent' => "$userid $money $remark",
            'linkid' => $data['userid'],
        ]);

        kg_send_json_success();

    }

    function reset_membership()
    {
        global $wpdb;

        $wpdb->query(
            "
                UPDATE wp_usermeta
                SET meta_value = 'a:1:{s:10:\"subscriber\";b:1;}'
                WHERE meta_key = 'wp_capabilities' AND meta_value IN ('a:1:{s:4:\"gold\";b:1;}', 'a:1:{s:6:\"silver\";b:1;}', 'a:1:{s:6:\"bronze\";b:1;}', 'a:1:{s:7:\"diamond\";b:1;}')
            "
        );

        $wpdb->query(
            "
                UPDATE korgou_user
                SET role = ''
            "
        );

        $results = $wpdb->get_results($wpdb->prepare(
            "
                SELECT userid, COUNT(*) as forward_count, SUM(totalfee) as forward_fee_sum
                FROM korgou_forward
                WHERE packagetime >= %s AND packagetime <= %s AND status IN ('5', '6')  -- forwarded, completed
                GROUP BY userid
                HAVING forward_count >= 5 OR forward_fee_sum >= 300000
            ", $_POST['start'], $_POST['end']
        ));

        $users = [];
        foreach ($results as $result) {
            $userid = $result->userid;
            $count = $result->forward_count;
            $sum = $result->forward_fee_sum;
            $role = ($count > 30 || $sum > 5000000) ? 'diamond' : (
                ($count > 20 || $sum > 3000000) ? 'gold' : (
                    ($count > 10 || $sum > 1000000) ? 'silver' : (
                        ($count >=5 || $sum >= 300000) ? 'bronze' : ''
                    )
                )
            );

            $wpdb->query(sprintf(
                "
                    UPDATE wp_usermeta UM, wp_users U
                    SET UM.meta_value = 'a:1:{s:%d:\"%s\";b:1;}'
                    WHERE UM.meta_key = 'wp_capabilities' AND UM.user_id = U.ID AND U.user_login = \"%s\"
                ", strlen($role), $role, $userid
            ));

            $wpdb->query(sprintf(
                "
                    UPDATE korgou_user
                    SET role = '%s'
                    WHERE userid = '%s'
                ", $role, $userid
            ));

            $users[] = [
                'userid' => $userid,
                'count' => $count,
                'sum' => $sum,
                'role' => $role,
            ];
        }

        wp_send_json_success($this->contents('reset-membership', ['users' => $users]));
    }
};
