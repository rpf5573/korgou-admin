<div class="row">
    <div class="col-12">
        <div class="card-box">
			<h4 class="card-title">Add balance</h4>

            <?php $this->ajax_form('add_money'); ?>
                <table class="table-bordered table table-hover w-50">
                <tbody>
                	<tr>
                        <th>User ID</th>
                        <td>
                            <input type="text" readonly name="userid" class="form-control" value="<?php echo $user->userid; ?>"/>
                        </td>
                    </tr>
                	<tr>
                        <th>Price（If the deduction is preceded by "-")</th>
                        <td>
                            <input type="text" id="inputmoney" name="inputmoney" class="form-control" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <th>Currency</th>
                        <td>
                            <?php
                            BS_Form::select([
                                'id' => 'currency',
                                'name' => 'currency',
                                'options' => Korgou::$CURRENCIES,
                                'blank_option' => '- Please choose -',
                            ]);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Actual increase</th>
                        <td>
                            <input type="text" id="money" name="money" value="" class="form-control" />
                            (KRW/South Korean won, allowing manual correction of the range +-5%)
                        </td>
                    </tr>
                    <tr>
                        <th>Description of income and expenditure</th>
                        <td><textarea id="remark" name="remark" rows="10" class="form-control"></textarea>
                        </td>
                    </tr>
                </tbody>
                </table>
                <p>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <button type="button" class="btn btn-primary submit-money-btn">Add</button>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>
