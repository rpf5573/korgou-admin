<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                $this->ajax_form('reset_membership', ['class' => 'form-inline']);
                do_action('korgou_we_form_controls', [[
                        'name' => ['start', 'end'], 'type' => 'period', 'label' => 'Period'
                    ], [
                        'type' => 'submit', 'button_label' => 'Reset membership',
                    ]
                ]);
                ?>
                </form>
                <div id="membership-result"></div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
jQuery(function($) {
    $('#submit-btn').click(function() {
        if ($('input[name="start"]').val().length != 10 || $('input[name="start"]').val().length != 10) {
            alert('Please write correct dates');
            return false;
        }
        if (confirm('All users\' membership will be initialized as Member and reset by their shipping count in the period. Continue?')) {
            $('#membership-result').html('');
            $(this).prop('disabled', 'disabled').
                html('Reset membership<div class="ml-1 spinner-border spinner-border-sm text-light" role="status"><span class="sr-only">Loading...</span></div>');
            $(this).closest('form').ajaxSubmit(function(response) {
                if (response.success) {
                    $('#membership-result').html(response.data);
                    $('#submit-btn').prop('disabled', '').html('Reset membership');
                }
            });
        }
        return false;
    });
});
</script>
