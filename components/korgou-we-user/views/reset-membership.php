<hr class="border-secondary1 mb-4" style="border-color: lightgray;">

<table class="table table-bordered table-sm">
    <tr>
        <th>User ID</th>
        <th>Forward count</th>
        <th>Forward fee sum</th>
        <th>Membership</th>
    </tr>
    <?php foreach ($users as $user): ?>
        <tr>
            <td><?php echo $user['userid']; ?></td>
            <td><?php echo $user['count']; ?></td>
            <td><?php echo number_format($user['sum']); ?></td>
            <td><?php echo $user['role']; ?></td>
        </tr>
    <?php endforeach; ?>
</table>
