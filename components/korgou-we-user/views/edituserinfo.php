<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_user'); ?>
                <table class="table-bordered table table-hover w-50">
                <tbody>
                    <tr>
                        <th>User ID</th>
                        <td class="user-role-wrap">
                            <?php korgou_user_role_icon($user->userid, true); ?>
                            <input type="text" readonly class="form-control-plaintext" name="userid" value="<?php echo $user->userid; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>
                            <input type="text" class="form-control" name="email" value="<?php echo $user->email; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>English Name</th>
                        <td><input type="text" name="englishname" class="form-control" value="<?php echo $user->englishname; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Native Name</th>
                        <td><input type="text" class="form-control" name="nativename"
                            value="<?php echo $user->nativename; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Gender</th>
                        <td>
                            <?php BS_Form::select(['name' => 'gender', 'options' => Korgou_User::$GENDERS, 'value' => $user->gender]); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td>
                            <?php do_action('korgou_select_country', $user->country); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Telephone</th>
                        <td><input type="text" name="phonenum" class="form-control" value="<?php echo $user->phonenum; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Cellphone</th>
                        <td><input type="text" name="mobilenum" class="form-control" value="<?php echo $user->mobilenum; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>QQ / Facebook</th>
                        <td><input type="text" name="facebookqq" class="form-control" value="<?php echo $user->facebookqq; ?>" />
                        </td>
                    </tr>
                </tbody>
                </table>
                <p>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <?php if (current_user_can('kg_edit_user')): ?>
                        <button type="button" class="btn btn-primary submit-btn">Save changes</button>
                    <?php endif; ?>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>
