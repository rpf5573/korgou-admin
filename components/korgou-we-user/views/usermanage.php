<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                        'name' => 'userid', 'type' => 'text', 'label' => 'User ID'
                    ], [
                        'name' => 'email', 'type' => 'text', 'label' => 'Email'
                    ], [
                        'name' => 'englishname', 'type' => 'text', 'label' => 'English Name'
                    ], [
                        'name' => 'nativename', 'type' => 'text', 'label' => 'Native Name'
                    ], [
                        'name' => 'status', 'type' => 'select', 'label' => 'Status', 'options' => Korgou_User::$STATUSES,
                    ], 
                ]);
                ?>

                <?php
                do_action('korgou_we_datatable', [
                    'User ID', 'Balance', 'EMAIL', 'English Name', 'Native Name',
				    'Gender', 'Country', 'Telephone', 'Cellphone', 'QQ',
                    // 'skype', 'Registration Time', User status', '改User status change', 'State funding',
                    'Registration Time', 'User status', 'User status change', 'State funding',
                    'Change status funds', 'Actions'],
                    [9, 10, 11, 12, 13, 14],
                    'user_load_user'
                );
                ?>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<div id="password-modal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Successful operation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>New password：<span id="new-password"></span></p>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    var rmbrate = 0;
	var usdrate = 0;

    $('.datatable').on('click', 'a', function() {
        $('#section-1').hide();
        $('#section-2').load($(this).attr('href'));
        return false;
    });
    $('.datatable').on('click', '.add-money-btn', function() {
        if (rmbrate == 0 && usdrate == 0) {
            $.post('/wp-admin/admin-ajax.php', {
                action: 'korgou_we_list_all_currencies',
                _wpnonce: '<?php echo wp_create_nonce('korgou_we_list_all_currencies'); ?>'
            }, function(response) {
                var rateArray = response.data;
                for (i = 0; i < rateArray.length; i++) {
                    if (rateArray[i].currency == 'RMB') {
                        rmbrate = rateArray[i].exchangekrw;
                    } else if (rateArray[i].currency == 'USD') {
                        usdrate = rateArray[i].exchangekrw;
                    }
                }
            });
        }
    });

    $('.datatable').on('click', '.change-status-btn', function() {
        if (confirm('Are you sure?')) {
            var userid = $(this).data('userid');
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('change_status'); ?>',
                _wpnonce: '<?php $this->the_nonce('change_status'); ?>',
                userid: userid,
                status: $('#user-status-' + userid).val()
            }, function(response) {
                alert(response.data);
            });
        }
        return false;
    });
    $('.datatable').on('click', '.change-balance-state-btn', function() {
        var userid = $(this).data('userid');
		var balancestate = $('#balance-state-' + userid).val();
		if (balancestate == '') {
			alert('请选择状态');
			return false;
		}

        if (confirm('Are you sure?')) {
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('change_balance_state'); ?>',
                _wpnonce: '<?php $this->the_nonce('change_balance_state'); ?>',
                userid: userid,
                state: balancestate
            }, function(response) {
                alert(response.data);
            });
        }
        return false;
    });
    $('.datatable').on('click', '.reset-password-btn', function() {
        var userid = $(this).data('userid');
        if (confirm('Are you sure?')) {
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('reset_password'); ?>',
                _wpnonce: '<?php $this->the_nonce('reset_password'); ?>',
                userid: userid
            }, function(response) {
                $('#new-password').text(response.data);
                $('#password-modal').modal();
            });
        }
        return false;
    });

    $('#section-2').on('click', '.cancel-btn', function() {
        $('#section-2').html('');
        $('#section-1').show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });
    $('#section-2').on('click', '.submit-money-btn', function() {
        var $self = $(this), $form = $(this).closest('form');
        $self.attr('disabled', "true");

        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
                $datatable.draw();
            } else {
                $self.attr('disabled', "true");
            }
        });
        return false;
    });
    $('#section-2').on('change', '#inputmoney', function() {
        calcExchange();
    });
    $('#section-2').on('change', '#currency', function() {
        calcExchange();
    });
    $('#section-2').on('keyup', '#inputmoney', function() {
        calcExchange();
    });
    $('#section-2').on('keyup', '#money', function() {
        $('#section-2 .submit-money-btn').prop("disabled", false);
    });

   	function calcExchange() {
		var inputmoney = $("#inputmoney").val();
		if (isNaN(inputmoney)) {
			return false;
		}
		var FROM = $("#currency").val();
		var moneyresult = 0;
		if (FROM == 'RMB') {
			moneyresult = inputmoney * rmbrate;
		} else if (FROM == 'USD') {
			moneyresult = inputmoney * usdrate;
		} else if (FROM == 'KRW') {
			moneyresult = inputmoney;
		} else {
			moneyresult = 0;
		}
		moneyresult = Math.round(moneyresult * 1) / 1;
		$("#money").val(moneyresult);

        $('#section-2 .submit-money-btn').prop("disabled", false);

		return false;
	}
});
</script>
