<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                        'name' => 'userid', 'type' => 'text', 'label' => 'User ID'
                    ], [
                        'name' => 'tradeno', 'type' => 'text', 'label' => 'Transaction No.'
                    ], [
                        'name' => 'outorderno', 'type' => 'text', 'label' => 'External order number'
                    ], [
                        'name' => ['tradetime_start', 'tradetime_end'], 'type' => 'period', 'label' => 'Time'
                    ], 
                ]);
                ?>
<!--
<q> <label for="tradeno">：</label> <input name="tradeno" id="tradeno" type="text" class="littlelong"> &nbsp;<label for="userid">：</label> <input name="userid" id="userid" type="text" class="short"> &nbsp; <label for="outorderno">：</label>
						<input name="outorderno" id="outorderno" type="text" class="littlelong"> &nbsp;<label for="">Time：</label> <input name="tradetimestart" id="tradetimestart" type="text" class="date">
						- <input name="tradetimeend" id="tradetimeend" type="text" class="date"> <a id="date_today" href="#">Today</a> <a id="date_yesterday" href="#">Yesterday</a> <a id="date_inWeek" href="#">Week</a>
						<a id="date_inMonth" href="#">Month</a> </q>
                        -->

                <?php
                do_action('korgou_we_datatable', [
                    'Transaction No.', 'External order number', 'User ID', 'Currency', 'Amount changed', 'Description', 'Payment method', 'Trading places', 'Time', 'Staff', 'Remarks'
                ],
                    [],
                    'user_load_balancerecord'
                );
                ?>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<script type="text/javascript">
jQuery(function($) {
    $('.datatable').on('click', 'a.package-btn', function() {
        console.log($(this).attr('href'));
        $('#section-1').hide();
        $('#section-2').load($(this).attr('href'));
        return false;
    });
    $('#section-2').on('click', '.cancel-btn', function() {
        $('#section-2').html('');
        $('#section-1').show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });
});
</script>
