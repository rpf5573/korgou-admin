<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_currency';

    function load()
    {
        $this->add_shortcode('exchange');
        $this->add_ajax('update_exchange');
        $this->add_ajax('load_currency');
        $this->add_ajax('load_history');
    }

    function load_currency()
    {
        wp_send_json_success(apply_filters('korgou_currency_load_currency', []));
    }

    function exchange()
    {
        $context = [
            'exchanges' => apply_filters('korgou_currency_get_exchange_list', [], []),
        ];
        $this->view('exchange', $context);
    }

    function update_exchange()
    {
        $exchanges = Shoplic_Util::get_post_data_array('currency', 'exchangekrw');

        foreach ($exchanges as $ex) {
            $args = [
                'updatetime' => current_time('mysql'),
                'exchangekrw' => str_replace(',', '', $ex['exchangekrw'])
            ];
            do_action('korgou_currency_update_exchange', $args, ['currency' => $ex['currency']]);

            $args['userid'] = KG::get_current_userid();
            $args['currency'] = $ex['currency'];
            do_action('korgou_currency_insert_history', $args);
        }

        kg_send_json_success();
    }

    function load_history()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        // $args = Shoplic_Util::get_post_data('country', 'forwardcourierid');
        $args = [];

        $page = apply_filters('korgou_currency_get_history_page', [], $args, null, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $data[] = [
                $item->id,
                $item->currency,
                sprintf("%.2f", $item->exchangekrw),
                $item->userid,
                $item->updatetime,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }
};
