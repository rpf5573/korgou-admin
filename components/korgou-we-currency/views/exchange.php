<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_exchange'); ?>
                <table class="table table-sm table-bordered">
                    <thead class="thead-light">
                        <th>Number</th>
                        <th>Currency</th>
                        <th>Exchange won</th>
                        <th>Update</th>
                    </tbody>
                    <tbody>
                        <?php foreach ($exchanges as $ex): ?>
                            <tr>
                                <td class="align-middle"><?php echo $ex->id; ?></td>
                                <td class="align-middle"><?php echo $ex->currency; ?></td>
                                <td>
                                    <input type="hidden" name="currency[]" class="form-control" value="<?php echo $ex->currency; ?>">
                                    <input type="text" id="currency-<?php echo $ex->currency; ?>" name="exchangekrw[]" class="form-control" value="<?php printf("%.2f", $ex->exchangekrw); ?>">
                                </td>
                                <td class="align-middle"><?php echo $ex->updatetime; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <p class="text-center">
                    <button type="button" id="update-btn" class="btn btn-primary">Update</button>
                    <button type="button" id="load-btn" class="btn btn-success">Load from Korea Eximbank</button>
                    <a href="https://www.koreaexim.go.kr/site/program/financial/exchange?menuid=001001004002002" target="_blank"
                        class="btn btn-link">Korea Eximbank</a>
                    <span>
                        (RMB - 6, USD * 0.901)
                    </span>
                </p>
            </form>
        </div>

        <div class="card-box">
            <?php
            do_action('korgou_we_datatable', [
                'Number', 'Currency', 'Exchange won', 'User', 'Update date',
            ], [],
                'currency_load_history'
            );
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#update-btn'), $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        $form.ajaxSubmit(function(response) {
            if (response.success) {
                alert(response.data);
                location.reload();
            } else {
                alert('Error occurred! Please try again later.');
            }
        });
    });
    $('#load-btn').click(function() {
        $.post('/wp-admin/admin-ajax.php', {
            action: '<?php $this->the_tag('load_currency'); ?>',
            _wpnonce: '<?php $this->the_nonce('load_currency'); ?>'
        }, function(response) {
            response.data.forEach(function(e) {
                $('#currency-' + e.currency).val(e.exchangekrw);
            });
        }, 'json');
        return false;
    });
});
</script>

