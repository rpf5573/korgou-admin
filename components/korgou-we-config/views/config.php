<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_config'); ?>
                <table class="table table-sm table-bordered">
                    <thead class="thead-light">
                        <th style="width: 300px;">Option</th>
                        <th>Value</th>
                    </tbody>
                    <tbody>
                        <?php foreach ($configs as $co): ?>
                            <tr>
                                <td class="align-middle">
                                    <?php echo $co->kname; ?>
                                    <input type="hidden" name="k[]" class="form-control" value="<?php echo $co->k; ?>">
                                </td>
                                <td>
                                    <input type="text" name="v[]" class="form-control" value="<?php echo $co->v; ?>">
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <p>
                    <button type="button" id="submit-btn" class="btn btn-primary">Update</button>
                </p>
            </form>
        </div>

    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        $form.ajaxSubmit((response) => {
            if (response.success) {
                alert(response.data);
                location.reload();
            } else {
                alert("抱歉，系统异常！");
            }
        });
    });
});
</script>

