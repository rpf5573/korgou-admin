<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_config';

    function load()
    {
        $this->add_shortcode();
        $this->add_ajax('update_config');
    }

    function shortcode()
    {
        $context = [
            'configs' => apply_filters('korgou_get_config_list', [], []),
        ];
        $this->view('config', $context);
    }

    function update_config()
    {
        $configs = Shoplic_Util::get_post_data_array('k', 'v');

        foreach ($configs as $co) {
            do_action('korgou_update_config', $co);
        }

        kg_send_json_success();
    }
};


