<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                /*
                do_action('korgou_we_search_form', [[
                    'name' => 'staffid', 'type' => 'text', 'label' => '工号',
                ], [
                    'name' => 'logname', 'type' => 'text', 'label' => '登陆名',
                ], [
                    'name' => 'staffname', 'type' => 'text', 'label' => 'Name',
                ]]);
                */
                ?>

                <?php
                do_action('korgou_we_datatable', [
                    'User ID', 'Name', 'Role', 'Registered', 'Last Login',
                    // '工号', 'Name', '登陆名', '上次登陆时间', '上次登陆IP',
                    // '登陆错误次数', '登陆错误时间', '登陆错误IP', '登陆时间', '登陆IP',
                    'Actions',
                ], [],
                    'bossstaff_load_bossstaff'
                );
                ?>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<script type="text/javascript">
jQuery(function($) {
    $('section').on('click', 'a.link', function() {
        var href = $(this).attr('href');
        var $section = $(this).closest('section');
        $section.hide();
        $section.next().data('href', href).load(href);
        return false;
    });
    $('section').on('click', '.cancel-btn', function() {
        var $section = $(this).closest('section');
        $section.html('');
        $section.prev().show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            if (response.success) {
                alert('Password updated');
                $('#section-2').html('');
                $('#section-1').show();
            } else {
                alert(response.data);
            }
        });
        return false;
    });
});
</script>
