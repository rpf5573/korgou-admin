<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('add_staff'); ?>
                <div class="form-group row">
                    <label for="input-staffid" class="col-sm-2 col-form-label">User ID</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="input-user-login" name="user_login">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-staffname" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="input-display-name" name="display_name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-logname" class="col-sm-2 col-form-label">Role</label>
                    <div class="col-sm-4">
                        <?php BS_Form::select([
                            'name' => 'role',
                            'options' => static::$STAFF_ROLES,
                        ]); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-logname" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="input-user-email" name="user_email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="input-user-pass" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="input-user-pass" name="user_pass">
                    </div>
                </div>
                <button type="button" id="submit-btn" class="btn btn-primary">Register</button>
            </form>
        </div>

    </div>
</div>

<?php $this->ajax_form('find_user', ['id' => 'find-user-form']); ?>
    <input type="hidden" name="userid" id="find-userid">
</form>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $('#input-user-login').blur(function() {
		$('#useridlabel').html('');
        var userid = $('#input-user-login').val();
		if (userid == '') {
			$('#useridlabel').html('<span class="text-danger">User ID required</span>');
			return false;
        }
        $('#find-userid').val(userid);
        $('#find-user-form').ajaxSubmit(function(response) {
            if (response.success) {
                $('#useridlabel').html(response.data.englishname + ' || ' + response.data.nativename);
            } else {
				$('#useridlabel').html('<span class="text-danger">' + response.data + '</span>');
            }
        });
    });
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        var userid = $('#input-user-login').val();
		if (userid == '') {
			alert('User ID required');
			return false;
		}
        $form.ajaxSubmit(function(response) {
            if (response.success) {
                alert('User registered');
                $form[0].reset();
            } else {
                alert(response.data);
            }
        });
    });
});
</script>
