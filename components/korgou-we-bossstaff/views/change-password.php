<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_password'); ?> 
                <h4 class="card-title">Change Password</h4>
                <div class="form-group row">
                    <label for="" class="col-sm-2 col-form-label"><?php _e('User ID', 'korgou'); ?></label>
                    <div class="col-sm-4">
                        <input type="text" readonly class="form-control-plaintext" id="input-user-id" name="user_login" value="<?php echo $user->user_login; ?>">
                    </div>
                </div>
                <div class="form-group required row">
                    <label for="" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="input-user-pass" name="user_pass" value="" required>
                    </div>
                </div>
                <div class="mt-4">
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <button type="button" class="btn btn-primary submit-btn">Update</button>
                </div>
            </form>
        </div> <!-- end card-box -->
    </div> <!-- end col -->
</div>

<script type="text/javascript">
jQuery(function($) {
});
</script>

