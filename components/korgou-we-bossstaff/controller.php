<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_bossstaff';

    static $STAFF_ROLES = [
        'senior_staff' => 'Senior staff',
        'junior_staff' => 'Junior staff',
    ];

    function load()
    {
        $this->add_shortcode('staffmanage');
        $this->add_shortcode('staffright');
        $this->add_shortcode('newstaff');
        $this->add_shortcode('edit');
        $this->add_shortcode('change_password');

        $this->add_ajax('add_staff');
        $this->add_ajax('update_staff');
        $this->add_ajax('update_password');
        $this->add_ajax('load_bossstaff');
        $this->add_ajax('load_staffright');
    }

    function change_password()
    {
        $context = [
            'user' => get_user_by('login', $_REQUEST['user']),
        ];
        $this->view('change-password', $context);
    }

    function edit()
    {
        $context = [
            'user' => get_user_by('login', $_REQUEST['user']),
        ];
        $this->view('edit-staff', $context);
    }

    function update_password()
    {
        $user = get_user_by('login', $_REQUEST['user_login']);
        if ($user) {
            $result = wp_update_user([
                'ID' => $user->ID,
                'user_pass' => $_REQUEST['user_pass'],
            ]);
            if (is_wp_error($result)) {
                wp_send_json_error($result->get_error_message());
            }

            wp_send_json_success();
        } else {
            wp_send_json_error('User not found');
        }
    }

    function add_staff()
    {
        $data = Shoplic_Util::get_post_data('user_login', 'display_name', 'user_email', 'user_pass', 'role');
        $result = wp_insert_user($data);
        if (is_wp_error($result)) {
            wp_send_json_error($result->get_error_message());
        }

        wp_send_json_success();
    }

    function update_staff()
    {
        $data = Shoplic_Util::get_post_data('ID', 'display_name', 'user_email', 'role');
        $result = wp_update_user($data);
        if (is_wp_error($result)) {
            wp_send_json_error($result->get_error_message());
        }

        wp_send_json_success();
    }

    function staffmanage()
    {
        $context = [];
        $this->view('staffmanage', $context);
    }

    function staffright()
    {
        $context = [];
        $this->view('staffright', $context);
    }

    function newstaff()
    {
        $context = [];
        $this->view('newstaff', $context);
    }

	public function get_role($user = null)
	{
        global $wp_roles;

        $roles = array();
        if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
            foreach ( $user->roles as $role )
                $roles[] .= translate_user_role( $wp_roles->roles[$role]['name'] );
        }
        return implode(', ',$roles);
	}

    function load_bossstaff()
    {
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;

        $query = new WP_User_Query([
            'role__in' => ['staff', 'senior_staff', 'junior_staff'],
            'orderby' => 'display_name',
            'order' => 'ASC',
            'paged' => $paged,
        ]);
        $users = $query->get_results();
        $data = [];
        foreach ($users as $user) {
            $actions = '';
            if (current_user_can('kg_edit_staff'))
                $actions .= sprintf('<a href="/bossstaff/change-password/?user=%s" class="link reset-pw-btn btn btn-sm btn-warning">Change password</a>', $user->user_login);

            $data[] = [
                current_user_can('kg_edit_staff') ?
                    sprintf('<a href="/bossstaff/edit-staff/?user=%1$s" class="link btn btn-link">%1$s</a>', $user->user_login) :
                    $user->user_login,
                $user->display_name,
                $this->get_role($user),
                $user->user_registered,
                get_usermeta($user->ID, 'sm_last_login'),
                $actions,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => $query->get_total(),
            'recordsFiltered' => $query->get_total(),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function load_staffright()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('staffid');

        $page = apply_filters('boss_staff_get_staff_page', [], $args, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $data[] = [
                $item->staffid,
                $item->staffname,
                sprintf('<button type="button" class="right-btn btn btn-sm btn-warning" data-id="%d">修改权陝</button>', $item->staffid),
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

};

