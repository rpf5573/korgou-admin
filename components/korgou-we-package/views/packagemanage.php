<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                    'name' => 'userid', 'type' => 'text', 'label' => 'User ID'
                ], [
                    'name' => 'domestictrackno', 'type' => 'text', 'label' => 'Tracking Number'
                ], [
                    'name' => 'packageid', 'type' => 'text', 'label' => 'Package ID'
                ], [
                    'name' => 'status', 'type' => 'select', 'label' => 'Status', 'options' => Korgou_PACKAGE::$STATUSES,
                ], [
                    'name' => 'checkliststatus', 'type' => 'select', 'label' => 'Photo', 'options' => Korgou_PACKAGE::$CHECKLISTSTATUSES,
                ], [
                    'name' => 'disposal', 'type' => 'select', 'label' => 'Disposal', 'options' => Korgou_PACKAGE::$DISPOSALS,
                ], [
                    'name' => ['arrivaltime_start', 'arrivaltime_end'], 'type' => 'period', 'label' => 'Arrival time'
                ]]);
                ?>
                <!--
                <form id="search-form" class="form-inline">
                    <label>用户编号： </label> <input type="text" class="form-control mr-2" name="userid">
                    <label>Tracking Number：</label> <input type="text" class="form-control mr-2" name="domestictrackno">
                    <label>Package ID： </label> <input type="text" class="form-control mr-2" name="packageid">
                    <label>Status：</label>
                    <select name="status" class="form-control mr-2">
                        <option value="">- Please choose -</option>
                        <?php BS_FORM::options(Korgou_Package::$STATUSES); ?>
                    </select>
                    <label>Photo：</label>
                    <select name="photo" class="form-control mr-2">
                        <option value="">- Please choose -</option>
                        <?php BS_FORM::options(Korgou_Package::$PHOTOS); ?>
                    </select>
                    <label>Sorting：</label>
                    <select name="checkliststatus" class="form-control mr-2">
                        <option value="">- Please choose -</option>
                        <?php BS_FORM::options(Korgou_Package::$CHECKLISTSTATUSES); ?>
                    </select>
                    <button type="button" class="btn btn-info" id="search-btn">查询</button>
                </form>
                <hr>
                -->

                <?php
                do_action('korgou_we_datatable',
                    [
                        '<input type="checkbox" class="check-all">', 'Package ID','User ID','Arrival time','Source package/Shopping website',
                        'Type', 'Courier company', 'Tracking Number', 'Package Contents', 'Weight(g)',
                        'Inbound staff', 'Status', 'Processing time', 'Comments', 'Photo',
                        'Disposal', 'Free<br>storage<br>period', 'Storage<br>charge<br>period', 'Delete', 'Remarks'
                    ],
                    [4, 6, 8, 10, 12, 13],
                    'package_load_package'
                );
                ?>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<script type="text/html" id="change-status-btn-group-tmpl">
    <div class="btn-group" id="change-status-btn-group">
        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Change status
        </button>
        <div class="dropdown-menu">
            <?php foreach (Korgou_Package::$STATUSES as $status => $label): ?>
                <a class="dropdown-item" href="#" data-status="<?php echo $status; ?>"><?php echo $label; ?></a>
            <?php endforeach; ?>
        </div>
    </div>
</script>

<script type="text/javascript">
jQuery(function($) {
    $('section').on('click', 'a.link', function() {
        var href = $(this).attr('href');
        var $section = $(this).closest('section');
        $section.hide();
        $section.next().data('href', href).load(href);
        return false;
    });
    $('<input type="hidden" name="order_by_user_id" value="false">').insertAfter('#search-btn');
    $('<button type="button" class="btn btn-light ml-3">Sort by User ID</button>').insertAfter('#search-btn').click(function() {
        if ($(this).hasClass('btn-light')) {
            $('input[name="order_by_user_id"]').val('true');
            $(this).removeClass('btn-light').addClass('btn-dark');
        } else {
            $('input[name="order_by_user_id"]').val('false');
            $(this).removeClass('btn-dark').addClass('btn-light');
        }
        $(this).blur();
        $('#search-btn').click();
    });

    $('.datatable').on('click', '.change-status-btn', function() {
        if (confirm('Are you sure?')) {
            var packageid = $(this).data('packageid');
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('update_package_status'); ?>',
                _wpnonce: '<?php $this->the_nonce('update_package_status'); ?>',
                packageid: packageid,
                status: $('#package-status-' + packageid).val()
            }, function(response) {
                alert(response.data);
            });
        }
        return false;
    });
    $('.datatable').on('click', '.delete-btn', function() {
        if (confirm("Deleting an order is an irreversible operation. If you delete it by mistake, it may cause serious consequences. Are you sure to delete it?")) {
            var packageid = $(this).data('packageid');
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('delete_package'); ?>',
                _wpnonce: '<?php $this->the_nonce('delete_package'); ?>',
                packageid: packageid
            }, function(response) {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });

    <?php if (KG_Admin::is_admin()): ?>
        $('<button id="delete-multi-btn" class="btn btn-danger" tabindex="0" aria-controls="DataTables_Table_0" type="button"><span>Delete</span></button>')
            .appendTo('.dt-buttons')
            .click(function() {
                if ($('.datatable .check:checked').length == 0) {
                    alert('Please select package to delete.');
                } else if (confirm("Deleting an order is an irreversible operation. If you delete it by mistake, it may cause serious consequences. Are you sure to delete it?")) {
                    var packageids = [];
                    $('.datatable .check:checked').each(function() {
                        packageids.push($(this).val());
                    });
                    $.post('/wp-admin/admin-ajax.php', {
                        action: '<?php $this->the_tag('delete_package'); ?>',
                        _wpnonce: '<?php $this->the_nonce('delete_package'); ?>',
                        packageid: packageids.join(',')
                    }, function(response) {
                        alert(response.data);
                        $datatable.draw();
                    });
                }
                return false;
            });
    <?php endif; ?>

    $($('#change-status-btn-group-tmpl').html()).appendTo('.dt-buttons');
    $('#change-status-btn-group a').click(function() {
        if ($('.datatable .check:checked').length == 0) {
            alert('Please select package to change status of.');
        } else if (confirm('Are you sure?')) {
            var packageids = [];
            $('.datatable .check:checked').each(function() {
                packageids.push($(this).val());
            });
            $.post('/wp-admin/admin-ajax.php', {
                action: '<?php $this->the_tag('update_package_status'); ?>',
                _wpnonce: '<?php $this->the_nonce('update_package_status'); ?>',
                packageid: packageids.join(','),
                status: $(this).data('status')
            }, function(response) {
                alert(response.data);
                $('#change-status-btn-group').dropdown('hide');
                $datatable.draw();
            });
        }
        return false;
    });
    /*
        .click(function() {
            if ($('.datatable .check:checked').length == 0) {
                alert('Please select package to delete.');
            } else if (confirm("Deleting an order is an irreversible operation. If you delete it by mistake, it may cause serious consequences. Are you sure to delete it?")) {
                var packageids = [];
                $('.datatable .check:checked').each(function() {
                    packageids.push($(this).val());
                });
                $.post('/wp-admin/admin-ajax.php', {
                    action: '<?php $this->the_tag('delete_package'); ?>',
                    _wpnonce: '<?php $this->the_nonce('delete_package'); ?>',
                    packageid: packageids.join(',')
                }, function(response) {
                    alert(response.data);
                    $datatable.draw();
                });
            }
            return false;
        });
        */

    $('.check-all').click(function() {
        $('.datatable .check').prop('checked', $(this).is(':checked'));
    });
    // 

    $('#section-2').on('click', '.cancel-btn', function() {
        $('#section-2').html('');
        $('#section-1').show();
        $datatable.draw();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
                $datatable.draw();
            }
        });
        return false;
    });
});
</script>
