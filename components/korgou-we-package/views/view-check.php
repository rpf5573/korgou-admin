<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h5 class="card-title">Parcel inventory</h5>

            <?php $this->ajax_form('update_check'); ?>
                <input type="hidden" id="input-checkliststatus" name="checkliststatus">

                <table class="table table-bordered w-50">
                    <tbody>
                        <tr>
                            <th>Package ID</th>
                            <td>
                                <input type="text" class="form-control-plaintext" readonly name="packageid" value="<?php echo $package->packageid; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <input type="text" class="form-control-plaintext" readonly value="<?php echo Korgou_Package::$CHECKLISTSTATUSES[$package->checkliststatus]; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th>Detailed description of the goods</th>
                            <td>
                                <textarea class="form-control" name="cargodetail" rows="5"><?php echo $check->cargodetail; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>Remarks</th>
                            <td>
                                <textarea class="form-control" id="input-checkremark" name="checkremark" rows="5"><?php echo $check->checkremark; ?></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <?php if (!empty($user_images)): ?>
                    <div class="mb-3">
                        <b>Attachments</b>
                        <br>
                        <?php do_action('korgou_package_show_image', $user_images); ?>
                    </div>
                <?php endif; ?>

                <?php if (!empty($images)): ?>
                    <div class="mb-3">
                        <b>Uploads</b>
                        <br>
                        <?php do_action('korgou_we_package_show_image', $images); ?>
                    </div>
                <?php endif; ?>

                <?php if (true || $package->checkliststatus == Korgou_Package::CHECKLISTSTATUS_APPLY): ?>
                    <?php do_action('korgou_we_package_upload_image_form'); ?>
                <?php endif; ?>

                <p>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <?php if (true || $package->checkliststatus == Korgou_Package::CHECKLISTSTATUS_APPLY): ?>
                        <button type="button" class="btn btn-primary check-success-btn">Complete</button>
                        <button type="button" class="btn btn-warning check-fail-btn">Fail</button>
                    <?php endif; ?>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>

<?php $this->ajax_form('delete_image'); ?>
    <input type="hidden" name="packageid" value="<?php echo $package->packageid; ?>">
    <input type="hidden" name="id" id="data-image-id" value="">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.check-success-btn').click(function() {
        if (confirm('Are you sure?')) {
            updateCheck('3', this);
        }
        return false;
    });
    $('.check-fail-btn').click(function() {
		if ($('#input-checkremark').val() == '') {
			alert('Please enter inventory description');
			return false;
		}
        if (confirm('Are you sure?')) {
            updateCheck('4', this);
        }
        return false;
    });

    function updateCheck(status, btn) {
        $('#input-checkliststatus').val(status);
        $(btn).closest('form').ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
                $datatable.draw();
            }
        });
    }
});
</script>
