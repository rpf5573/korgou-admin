<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_package'); ?>
                <table class="table-bordered table table-hover w-50">
                <tbody>
                    <tr>
                        <th>Package ID</th>
                        <td>
                            <input type="text" readonly class="form-control-plaintext" name="packageid" value="<?php echo $package->packageid; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>User ID</th>
                        <td class="user-role-wrap">
                            <?php korgou_user_role_icon($package->userid, true); ?>
                            <input type="text" readonly class="form-control-plaintext" value="<?php echo $package->userid; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Arrival time</th>
                        <td>
                            <input type="text" readonly class="form-control-plaintext" value="<?php echo $package->arrivaltime; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Source package/Shopping website</th>
                        <td><input type="text" id="packagesource" name="packagesource" class="form-control"
                            class="longer" value="<?php echo $package->packagesource; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Courier company</th>
                        <td><input type="text" id="domesticcourier" class="form-control" name="domesticcourier"
                            value="<?php echo $package->domesticcourier; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Tracking Number</th>
                        <td><input type="text" id="domestictrackno" class="form-control" name="domestictrackno"
                            value="<?php echo $package->domestictrackno; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Package Contents</th>
                        <td><input type="text" id="packagecontent" class="form-control" name="packagecontent"
                            value="<?php echo $package->packagecontent; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Weight(g)</th>
                        <td><input type="text" id="weight" name="weight" class="form-control"
                            value="<?php echo $package->weight; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Type</th>
                        <td><?php BS_FORM::select([
                            'name' => 'type',
                            'value' => $package->type,
                            'options' => Korgou_Package::$TYPES,
                        ]); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Storage Fee</th>
                        <td><input type="text" id="storagefee" name="storagefee" class="form-control"
                            value="<?php echo $package->storagefee; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Remarks</th>
                        <td><input type="text" id="remark" name="remark" class="form-control"
                            value="<?php echo $package->remark; ?>" />
                        </td>
                    </tr>
                </tbody>
                </table>
                <p>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <button type="button" class="btn btn-primary submit-btn">Save changes</button>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>
