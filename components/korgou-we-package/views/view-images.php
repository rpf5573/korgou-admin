<div class="row">
    <div class="col-12">
        <div class="card-box">
            <dl class="row">
                <dt class="col-md-2">Package ID</dt>
                <dd class="col-md-4">
                    <?php echo $packageid; ?>
                </dd>
            </dl>

            <table class="table1">
                <tbody>
                    <?php foreach ($images as $image): ?>
                        <tr>
                            <td>
                                <button class="btn btn-danger btn-sm delete-btn" data-image-id="<?php echo $image->id; ?>">Delete photo</button>
                            </td>
                            <td>
                                <img src="/image/?size=big&id=<?php echo $image->id; ?>">
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
                
        </div> <!-- end card-box -->
    <div>
</div>

<p>
    <button type="button" class="btn btn-secondary cancel-btn">Back</button>
</p>

<?php $this->ajax_form('delete_image'); ?>
    <input type="hidden" name="packageid" value="<?php echo $packageid; ?>">
    <input type="hidden" name="id" id="data-image-id" value="">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.delete-btn').click(function() {
        var $self = $(this);
        if (confirm("Are you sure to delete?")) {
            $('#data-image-id').val($(this).data('image-id')).closest('form').ajaxSubmit(function(response) {
                $self.closest('tr').remove();
            });
        }
    });
});
</script>
