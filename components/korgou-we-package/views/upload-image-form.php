<div class="dropzone mb-3">
    <div class="fallback">
        <input name="file" type="file" multiple />
    </div>

    <div class="dz-message needsclick">
        <i class="h1 text-muted dripicons-cloud-upload"></i>
        <h3>Drop files here or click to upload.</h3>
    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    /*
    $('.dropzone').dropzone({
        url: '/wp-admin/admin-ajax.php',
        params: {
            action: '<?php $this->the_tag('upload_image'); ?>',
            _wpnonce: '<?php $this->the_nonce('upload_image'); ?>'
        },
        addRemoveLinks: true,
        success: function(file, response) {
            console.log(response);
            console.log(file);
            file.previewElement.addEventListener("click", function() {
                $('.dropzone').removefile(file);
            });
        },
        removedfile: function(file) {
            console.log(file);
            return true;
        }
    });
    */
    var dropzone = new Dropzone('.dropzone', {
        url: '/wp-admin/admin-ajax.php',
        params: {
            action: '<?php $this->the_tag('upload_image'); ?>',
            _wpnonce: '<?php $this->the_nonce('upload_image'); ?>'
        },
        //addRemoveLinks: true,
        init: function() {
            this.on('success', function(file, response) {
                console.log(response);
                console.log(file);
                file.previewElement.addEventListener("click", function() {
                    this.removefile(file);
                });
                $('.dropzone').append('<input type="hidden" name="imageid[]" value="' + response.data + '">');
            });
        }
    });
});
</script>
