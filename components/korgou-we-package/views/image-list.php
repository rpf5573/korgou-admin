<style>
/* Container needed to position the button. Adjust the width as needed */
.package-figure {
  position: relative;
}

.img-container {
    width: 300px;
    height: 300px;
}

/* Make the image responsive */
.package-figure .img-container img {
    max-width: 300px;
    max-height: 300px;
}

/* Style the button and place it in the middle of the container/image */
.package-figure .btn {
  position: absolute;
  top: 4px;
  right: 4px;
}
ul {
  list-style-type: none;
  padding-left: 0;
}
li {
  float: left;
}

</style>

<ul>
    <?php foreach ($images as $image): ?>
        <li class="figure-item">
            <div class="package-figure mr-2 border ">
                <button type="button" class="delete-btn btn btn-danger btn-xs" data-image-id="<?php echo $image->id; ?>"><i class="fe-trash-2"></i></button>
                <a target="_blank" href="/image/?id=<?php echo $image->id;?>&size=big">
                <div class="img-container d-flex align-items-center justify-content-center">
                        <img src="/image/?id=<?php echo $image->id;?>">
                    </div>
                </a>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<div style="clear:both;"></div>

<?php $this->ajax_form('delete_image'); ?>
    <input type="hidden" name="packageid" value="<?php echo $packageid; ?>">
    <input type="hidden" name="id" id="data-image-id" value="">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.delete-btn').click(function() {
        var $self = $(this);
        if (confirm("Are you sure to delete?")) {
            $('#data-image-id').val($(this).data('image-id')).closest('form').ajaxSubmit(function(response) {
                $self.closest('.figure-item').remove();
            });
        }
        return false;
    });
});
</script>
