<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h5 class="card-title">Item disposal</h5>
            <?php $this->ajax_form('update_disposal'); ?>
                <table class="table table-bordered w-50">
                    <tbody>
                        <tr>
                            <th>Package ID</th>
                            <td>
                                <input type="text" class="form-control-plaintext" readonly name="packageid" value="<?php echo $package->packageid; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th>Disposal notes</th>
                            <td>
                                <textarea class="form-control" name="cargodetail" rows="5"><?php echo $disposal->cargodetail; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <select class="form-control" id="input-status" name="disposal">
                                    <?php foreach (Korgou_Package::$DISPOSALS as $value => $label): ?>
                                        <option value="<?php echo $value; ?>" <?php selected($package->disposal, $value); ?>><?php echo $label; ?></option>
                                    <?php endforeach; ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Remarks</th>
                            <td>
                                <textarea class="form-control" id="input-checkremark" name="checkremark" rows="5"><?php echo $disposal->checkremark; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>Disposal fee</th>
                            <td>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="input-disposalfee" name="disposalfee" value="<?php echo $disposal->disposalfee; ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text">KRW</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <?php if (!empty($user_images)): ?>
                    <div class="mb-3">
                        <b>Attachments</b>
                        <br><br>
                        <?php do_action('korgou_package_show_image', $user_images); ?>
                    </div>
                <?php endif; ?>

                <div class="mb-3">
                    <b>Uploads</b>
                    <?php if (!empty($images)): ?>
                        <br><br>
                        <?php do_action('korgou_we_package_show_image', $images); ?>
                    <?php endif; ?>
                </div>

                <?php if (true || $package->checkliststatus == Korgou_Package::CHECKLISTSTATUS_APPLY): ?>
                    <?php do_action('korgou_we_package_upload_image_form'); ?>
                <?php endif; ?>

                <p>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <button type="button" class="btn btn-primary save-disposal-btn">Save</button>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.save-disposal-btn').click(function() {
        $(this).closest('form').ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
                $datatable.draw();
            }
        });
    });
});
</script>
