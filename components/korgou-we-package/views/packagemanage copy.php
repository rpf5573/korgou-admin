<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <form class="search-form form-inline">
                    <label>用户编号： </label> <input type="text" class="form-control mr-2" name="userid">
                    <label>Tracking Number：</label> <input type="text" class="form-control mr-2" name="domestictrackno">
                    <label>Package ID： </label> <input type="text" class="form-control mr-2" name="packageid">
                    <label>Status：</label>
                    <select name="status" class="form-control mr-2">
                        <option value="">- Please choose -</option>
                        <?php BS_FORM::options(Korgou_Package::$STATUSES); ?>
                    </select>
                    <label>Photo：</label>
                    <select name="photo" class="form-control mr-2">
                        <option value="">- Please choose -</option>
                        <?php BS_FORM::options(Korgou_Package::$PHOTOS); ?>
                    </select>
                    <label>Sorting：</label>
                    <select name="checkliststatus" class="form-control mr-2">
                        <option value="">- Please choose -</option>
                        <?php BS_FORM::options(Korgou_Package::$CHECKLISTSTATUSES); ?>
                    </select>
                    <button type="button" class="btn btn-info">查询</button>
                </form>
                <hr>
                <div class="row mb-2">
                    <div class="col">
                    </div>
                    <div class="col text-right">
                        <div class="btn-group" style="width: 160px;">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Edit the display items
                                <i class="mdi mdi-chevron-down float-right"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style="position: absolute;">
                                <a class="dropdown-item" href="#"><input type="checkbox" class="align-middle" />&nbsp;Option 1</a>
                                <a class="dropdown-item" href="#"><input type="checkbox" class="align-middle" />&nbsp;Option 1</a>
                                <a class="dropdown-item" href="#"><input type="checkbox" class="align-middle" />&nbsp;Option 1</a>
                                <a class="dropdown-item" href="#"><input type="checkbox" class="align-middle" />&nbsp;Option 1</a>
                            </div>
                        </div>
                    </div>
                </div>
            
                <form class="table-form">
                    <table class="table table-sm table-bordered datatable" <?php $this->nonce_action_attr('load_package'); ?>>
                        <thead class="thead-light">
                            <th>Number</th>
                            <th>Chinese name</th>
                            <th>English name</th>
                            <th>Korean name</th>
                            <th>Status</th>
                            <th class="d-none">Delete</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </form>

                <div class="row">
                    <div class="col">
                    <div class="float-left pagination-detail">
      <span class="pagination-info">
      Showing 1 to 25 of 30 rows
      </span><span class="page-list"><span class="btn-group dropdown dropup">
        <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
        <span class="page-size">
        25
        </span>
        <span class="caret"></span>
        </button>
        <div class="dropdown-menu"><a class="dropdown-item " href="#">10</a><a class="dropdown-item active" href="#">25</a><a class="dropdown-item " href="#">50</a></div></span> rows per page</span></div>
                    </div>
                    <div class="dataTables_paginate paging_full_numbers col text-right" id="dataTable_paginate">
                        <a tabindex="0" class="first paginate_button paginate_button_disabled" id="dataTable_first">首页</a>
                        <a tabindex="0" class="previous paginate_button paginate_button_disabled" id="dataTable_previous">上一页</a>
                        <span><a tabindex="0" class="paginate_active">1</a>
                        <a tabindex="0" class="paginate_button">2</a>
                        <a tabindex="0" class="paginate_button">3</a>
                        <a tabindex="0" class="paginate_button">4</a>
                        <a tabindex="0" class="paginate_button">5</a>
                        </span>
                        <a tabindex="0" class="next paginate_button" id="dataTable_next">下一页</a>
                        <a tabindex="0" class="last paginate_button" id="dataTable_last">尾页</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.submit(() => {
        return false;
    });
    $btn.click(() => {
        $form.ajaxSubmit((response) => {
            if (response.success) {
                alert(response.data);
                location.reload();
            } else {
                alert("抱歉，系统异常！");
            }
        });
    });
});
</script>

