<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('add_package'); ?>
                <div class="form-group row">
                    <div class="col-md-6">
                        <label for="input-userid" class="col-form-label">User ID</label>
                        <input type="text" class="form-control d-inline-block ml-3" style="width: 200px;" id="input-userid" name="userid" value="<?php echo $userid; ?>">
                        <span id="useridlabel" class="col-form-label ml-2"></span>
                    </div>
                    <div class="col-md-6 d-flex align-items-center">
                        <label for="input-type" class="col-form-label">Type</label>
                        <div class="form-check form-check-inline ml-3">
                            <input class="form-check-input" type="radio" id="input-type-envelope" name="type" value="E" style="width: 24px; height: 24px;">
                            <label class="form-check-label" for="input-type-envelope">Envelope</label>
                        </div>
                        <div class="form-check form-check-inline ml-2">
                            <input class="form-check-input" type="radio" id="input-type-box" name="type" value="B" style="width: 24px; height: 24px;">
                            <label class="form-check-label" for="input-type-box">Box</label>
                        </div>
                        <div class="form-check form-check-inline ml-2">
                            <input class="form-check-input" type="radio" id="input-type-other" name="type" value="O" style="width: 24px; height: 24px;">
                            <label class="form-check-label" for="input-type-other">Other</label>
                        </div>
                        <!--
                        <select class="form-control" name="type">
                            <option value="E">Envelope</option>
                            <option value="B">Box</option>
                        </select>
                        -->
                    </div>
                </div>
                <table class="table table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th style="width: 40px;"></th>
                        <th>Tracking Number</th>
                        <th style="width: 160px;">Weight</th>
                        <th>Remark</th>
                    </tr>
                </thead>
                <tbody>
                    <?php for ($i = 1; $i <= 7; ++$i): ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td>
                                <input type="text" class="form-control" id="input-domestictrackno-<?php echo $i; ?>" name="domestictrackno[]"
                                    <?php if ($i == 1) echo 'value="' . $trackno . '"'; ?>
                                >
                            </td>
                            <td>
                                <input type="text" class="form-control" id="input-weight-<?php echo $i; ?>" name="weight[]">
                            </td>
                            <td>
                                <input type="text" class="form-control" id="input-remark-<?php echo $i; ?>" name="remark[]">
                            </td>
                        </tr>
                    <?php endfor; ?>
                </tbody>
                </table>

                <button type="submit" id="submit-btn" class="btn btn-primary">Save</button>
            </form>
        </div>

    </div>
</div>

<?php $this->ajax_form('find_user', ['id' => 'find-user-form']); ?>
    <input type="hidden" name="userid" id="find-userid">
</form>

<script type="text/javascript">
jQuery(function($) {
    var validUser = false;

    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $('#input-userid').focus().blur(function() {
		$('#useridlabel').html('');
        var userid = $('#input-userid').val();
		if (userid == '') {
			$('#useridlabel').html('<span class="text-danger">Please enter the user number</span>');
			return false;
        }
        $('#find-userid').val(userid);
        $('#find-user-form').ajaxSubmit(function(response) {
            if (response.success) {
                $('#useridlabel').html('<span class="text-success">' + response.data.englishname + ' | ' + response.data.nativename + '</span>');
                validUser = true;
            } else {
				$('#useridlabel').html('<span class="text-danger">' + response.data + '</span>');
                validUser = false;
            }
        });
    });
    $form.on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
            e.preventDefault();
            return false;
        }
    });
    $form.ajaxForm({
        beforeSubmit: function() {
            var userid = $('#input-userid').val();
            if (userid == '') {
                alert('Please enter the user number');
                return false;
            } else if (!validUser) {
                alert('Invalid User ID');
                return false;
            }
            return true;
        },
        success: function(response) {
            if (response.success) {
                alert('Success');
                <?php if (!empty($userid) && !empty($trackno)): ?>
                    opener.jQuery('#search-btn').click();
                    window.close();
                <?php endif; ?>
                $form[0].reset();
		        $('#useridlabel').html('');
            } else {
                alert(response.data);
            }
        }
    });
});
</script>
