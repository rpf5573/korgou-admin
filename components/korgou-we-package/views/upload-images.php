<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_images'); ?>
                <input type="hidden" name="packageid" value="<?php echo $package->packageid; ?>">
                <dl class="row">
                    <dt class="col-md-2">Package ID</dt>
                    <dd class="col-md-4">
                        <?php echo $package->packageid; ?>
                    </dd>
                </dl>

                <?php do_action('korgou_we_package_upload_image_form'); ?>

                <p class="mt-2">
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <button type="button" class="btn btn-primary submit-btn">Save changes</button>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>
