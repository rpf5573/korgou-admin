<style>
.b1pu9emn {
    display: inline-flex;
    flex-direction: column;
    align-items: center;
}
.hrklfnd2 {
    width: 250px;
    height: 250px;
    display: inline-block;
}

.cet1simm {
    width: 100%;
    height: 100%;
    object-fit: cover;
}
</style>

<?php foreach ($images as $image): ?>
    <div class="mr-2 b1pu9emn figure-item">
        <a href="/image/?size=big&id=<?php echo $image->id; ?>" target="_blank" class="hrklfnd2">
            <img src="/image/?id=<?php echo $image->id; ?>" class="img-thumbnail cet1simm">
        </a>
        <button class="mt-2 btn btn-danger delete-btn" data-image-id="<?php echo $image->id; ?>">Delete</button>
    </div>
<?php endforeach; ?>

<script type="text/javascript">
jQuery(function($) {
    $('.delete-btn').click(function() {
        var $self = $(this);
        if (confirm("Are you sure to delete?")) {
            $.post('<?php echo admin_url('admin-ajax.php'); ?>', {
                action: '<?php $this->the_tag('delete_image'); ?>',
                nonce: '<?php $this->the_nonce('delete_image'); ?>',
                id: $(this).data('image-id')
            }, function(response) {
                $self.closest('.figure-item').remove();
            });
        }
        return false;
    });
});
</script>

