<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_package';

    function load()
    {
        $this->add_shortcode('newpackage');
        $this->add_shortcode('packagemanage');
        $this->add_shortcode('detail');
        $this->add_shortcode('upload_images');
        $this->add_shortcode('view_images');
        $this->add_shortcode('view_check');
        $this->add_shortcode('view_disposal');
        $this->add_ajax('find_user');
        $this->add_ajax('add_package');
        $this->add_ajax('load_package');
        $this->add_ajax('update_package');
        $this->add_ajax('update_package_status');
        $this->add_ajax('delete_package');
        $this->add_ajax('upload_image');
        $this->add_ajax('update_images');
        $this->add_ajax('delete_image');
        $this->add_ajax('update_check');
        $this->add_ajax('update_disposal');
        $this->add_action('upload_image_form');
        $this->add_action('image_list');
        $this->add_action('show_image');
    }

    function show_image($images)
    {
        if (!is_array($images))
            $images = [$images];

        $this->view('show-image', [
            'images' => $images,
        ]);
    }

    function newpackage()
    {
        $this->view('newpackage', [
            'userid' => $_REQUEST['userid'] ?? '',
            'trackno' => $_REQUEST['trackno'] ?? '',
        ]);
    }

    function find_user()
    {
        $userid = $_POST['userid'];
        $user = apply_filters('korgou_user_get_user', null, ['userid' => $userid]);
        if ($user == null)
            wp_send_json_error('User not found');
        else
            wp_send_json_success($user);   
    }

    function add_package()
    {
        Shoplic_Util::display_errors();
        $userid = $_POST['userid'];
        $userid = strtoupper(substr($userid, 0, 1)) . substr($userid, 1);

        $domesticcourier = $_POST['domesticcourier'] ?? '';
        $type = $_POST['type'] ?? '';

        $staff = wp_get_current_user();

        $tracknos = [];
        $packages = Shoplic_Util::get_post_data_array('domestictrackno', 'weight', 'remark');

        foreach ($packages as $package) {
            if (empty($package['domestictrackno']))
                continue;

            $package = array_merge($package, [
                'userid' => $userid,
                'type' => $type,
                'packagecontent' => $_POST['packagecontent'] ?? '',
                'packageresource' => $_POST['packageresource'] ?? '',
                'staffid' => $staff->user_login,
                'staffname' => $staff->display_name,
                'photo' => Korgou_Package::PHOTO_NONE,
                'checkliststatus' => Korgou_Package::CHECKLISTSTATUS_NONE,
            ]);

            if (!empty($domesticcourier))
                $package['domesticcourier'] = Korgou_Package::$KOREA_COURIERS[$domesticcourier];

            do_action('korgou_package_insert_package', $package);

            $tracknos[] = $package['domestictrackno'];
        }

        $email = KG::get_user_email($userid);

        if ($email != null) {
            $body = str_replace("\n", '<br>', $this->contents('email/package-arrival', [
                'trackno' => implode(', ' , $tracknos),
                'url' => get_site_url(1, '/my/dashboard/'),
            ]));

            SP_WC::send_email($email, 'Package arrival notification 包裹入库通知', 'Package Arrival', $body);
        }

        wp_send_json_success();
    }

    function packagemanage()
    {
        $context = [
        ];
        $this->view('packagemanage', $context);
    }

    function load_package()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('userid', 'domestictrackno', 'packageid', 'status', 'photo', 'checkliststatus', 'disposal', 'arrivaltime_start', 'arrivaltime_end');

        $conditions = [];
        if (!empty($args['userid']))
            $conditions[] = ['userid', '=', $args['userid']];
        if (!empty($args['domestictrackno']))
            $conditions[] = ['domestictrackno', 'LIKE', '%' . $args['domestictrackno'] . '%'];
        if (!empty($args['packageid']))
            $conditions[] = ['packageid', '=', $args['packageid']];
        if (!empty($args['status']))
            $conditions[] = ['status', '=', $args['status']];
        if (!empty($args['photo']))
            $conditions[] = ['photo', '=', $args['photo']];
        if (!empty($args['checkliststatus']))
            $conditions[] = ['checkliststatus', '=', $args['checkliststatus']];
        if (!empty($args['disposal']))
            $conditions[] = ['disposal', '=', $args['disposal']];
        if (isset($args['arrivaltime_start']) && !empty($args['arrivaltime_start']))
            $conditions[] = ['arrivaltime', '>=', $args['arrivaltime_start'] . ' 00:00:00'];
        if (isset($args['arrivaltime_end']) && !empty($args['arrivaltime_end']))
            $conditions[] = ['arrivaltime', '<=', $args['arrivaltime_end'] . ' 23:59:59'];

        $orderby = (isset($_POST['order_by_user_id']) && $_POST['order_by_user_id'] == 'true') ? 'userid ASC' : null;
        
        $page = apply_filters('korgou_package_get_package_page', [], $args, $orderby, $conditions, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            /*
            $change_status = '<div class="form-row"><div class="col-auto">' . BS_FORM::select([
                'id' => 'package-status-' . $item->packageid,
                'name' => 'status',
                'class' => 'form-control-sm d1-inline',
                'style' => 'width: inherit;',
                'options' => Korgou_Package::$STATUSES,
                'value' => $item->status,
                'blank_option' => '- Please choose -',
            ], false) . '</div><div class="col-auto"><button type="button" class="btn btn-sm btn-success change-status-btn" data-packageid="' . $item->packageid . '">Change</button></div></div>';
            
            */
            $change_status = '<div class="input-group">' . BS_FORM::select([
                'id' => 'package-status-' . $item->packageid,
                'name' => 'status',
                'class' => 'custom-select',
                // 'style' => 'width: inherit;',
                'options' => Korgou_Package::$STATUSES,
                'value' => $item->status,
                'blank_option' => '- Please choose -',
            ], false) . '<div class="input-group-append"><button type="button" class="btn btn-sm btn-success change-status-btn" data-packageid="' . $item->packageid . '">Change</button></div>';
            
            if ($item->checkliststatus == '3')
                $checkliststatus = '<a class="link btn btn-link btn-block" href="/package/view-check/?packageid=' . $item->packageid . '">Completed</button>';
            elseif ($item->checkliststatus == '4')
                $checkliststatus = '<a class="link btn btn-link btn-block" href="/package/view-check/?packageid=' . $item->packageid . '">Failed</button>';
            elseif ($item->checkliststatus == '2') {
                $check = apply_filters('korgou_package_get_check', null, [
                    'packageid' => $item->packageid,
                ]);
                if ($check != null && strlen($check->cargodetail) > 0) {
                    $tooltip = sprintf('data-toggle="tooltip" data-placement="right" title="%s"', esc_attr($check->cargodetail));
                    $icon = ' <i class="mdi mdi-file-document-box-outline"></i>';
                } else {
                    $tooltip = '';
                    $icon = '';
                }
                $checkliststatus = sprintf(
                    '<a class="link btn btn-sm btn-info btn-block" href="/package/view-check/?packageid=%s" %s>Applied%s</a>',
                    $item->packageid,
                    $tooltip,
                    $icon
                );
            } elseif ($item->checkliststatus == '1')
                $checkliststatus = 'Not applied';
            else
                $checkliststatus = 'Unknown';

            if ($item->disposal == '2')
                $disposal = sprintf('<a class="link btn btn-sm btn-blue btn-block" href="/package/view-disposal/?packageid=%s">%s</a>', $item->packageid, Korgou_Package::$DISPOSALS[$item->disposal]);
            elseif ($item->disposal == '1')
                $disposal = 'Not applied';
            else
                $disposal = sprintf('<a class="link" href="/package/view-disposal/?packageid=%s">%s</a>', $item->packageid, Korgou_Package::$DISPOSALS[$item->disposal]);

            $data[] = [
                sprintf('<input type="checkbox" class="check" value="%1$s">', $item->packageid),
                sprintf('<a class="link package-btn" href="/package/detail/?packageid=%1$s">%1$s</a>', $item->packageid),
                korgou_user_role_id($item->userid),
                $item->arrivaltime,
                $item->packagesource,
                $item->get_type_name(),
                $item->domesticcourier,
                $item->domestictrackno,
                $item->packagecontent,
                $item->weight,
                $item->staffname,
                // Korgou_Package::$STATUSES[$item->status] ?? 'Unknown',
                $change_status,
                $item->userprocesstime,
                $item->usercomment,
                $checkliststatus,
                $disposal,
                $item->the_free_period(false),
                $item->the_charge_period(false) . '<br>' . $item->get_storagefee() . ' KRW',
                current_user_can('kg_delete_package') ? '<button type="button" class="btn btn-sm btn-danger delete-btn" data-packageid="' . $item->packageid . '">Delete</button>' : '',
                $item->remark,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function detail()
    {
        $args = [
            'packageid' => $_GET['packageid']
        ];
        $package = apply_filters('korgou_package_get_package', null, $args);
        $this->view('detail', [
            'package' => $package
        ]);
    }

    function update_package()
    {
        $data = Shoplic_Util::get_post_data( 'packageid', 'status', 'packagesource', 'domesticcourier', 'domestictrackno', 'packagecontent', 'weight',
            'type', 'storagefee', 'remark');
        do_action('korgou_package_update_package', $data);

        kg_send_json_success();
    }

    function update_package_status()
    {
        $status = $_POST['status'];
        foreach (explode(',', $_POST['packageid']) as $packageid) {
            $data = [
                'packageid' => $packageid,
                'status' => $status,
            ];

            $package = apply_filters('korgou_package_get_package', null, ['packageid' => $packageid]);
            if ($package->status == Korgou_Package::STATUS_EXPIRED && $data['status'] == Korgou_Package::STATUS_IN_REPO)
                $data['adminactive'] = '1';

            do_action('korgou_package_update_package', $data);

        }
        
        kg_send_json_success();
    }

    function delete_package()
    {
        $data = [];
        foreach (explode(',', $_POST['packageid']) as $packageid) {
            $data['packageid'] = $packageid;
            do_action('korgou_package_delete_package', $data);

            do_action('korgou_package_delete_cart', $data);
        }

        kg_send_json_success();
    }

    function upload_images()
    {
        $args = [
            'packageid' => $_GET['packageid']
        ];
        $package = apply_filters('korgou_package_get_package', null, $args);
        if ($package != null)
            $this->view('upload-images', [
                'package' => $package
            ]);
    }

    function upload_image_form()
    {
        $this->view('upload-image-form');
    }

    function upload_image()
    {
        $arr_img_ext = [ 'image/png', 'image/jpeg', 'image/jpg', 'image/gif' ];
        if (in_array($_FILES['file']['type'], $arr_img_ext)) {
            $path = $_FILES['file']['name'];
            $ext = strtolower(pathinfo($path,PATHINFO_EXTENSION));

            $dir = KORGOU_IMAGE_PATH . '/' . Shoplic_Util::now('ymd');
            mkdir($dir, 0755, true);

            $prefix = $dir . '/' . Shoplic_Util::uuid_v4() . '_';
            $big = $prefix . 'BIG.' . $ext;
            $small = $prefix . 'SMALL.' . $ext;

            if (move_uploaded_file($_FILES["file"]["tmp_name"], $big)) {
                $editor = wp_get_image_editor($big);
                $editor->resize(300, 300);
                $saved = $editor->save($small);
                if ($saved) {
                    $imageid = apply_filters('korgou_package_insert_image', 0, [
                        'smallimageurl' => $small,
                        'bigimageurl' => $big,
                        'imagetype' => Korgou_Package_Image::IMAGETYPE_NONE,
                    ]);
                    wp_send_json_success($imageid);
                }
            }
        }

        wp_send_json_error();
    }

    function update_images()
    {
        $packageid = $_POST['packageid'];
        if (isset($_POST['imageid']))
            do_action('korgou_package_attach_images', $packageid, $_POST['imageid'], Korgou_Package_Image::IMAGETYPE_IN_REPO_PACKAGE);

        $data = [
            'packageid' => $packageid,
            'photo' => Korgou_Package::PHOTO_HAVE,
        ];
        do_action('korgou_package_update_package', $data);

        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
        ]);
        $email = KG::get_user_email($package->userid);

        $body = 'Your applied package photos';
        SP_WC::send_email($email,
            'Your applied package photos',
            'Your applied package photos',
            $body,
            apply_filters('korgou_package_get_image_paths', [], ['packageid' => $packageid, 'imagetype' => Korgou_Package_Image::IMAGETYPE_IN_REPO_PACKAGE])
        );

        kg_send_json_success();
    }

    function view_images()
    {
        $packageid = $_GET['packageid'] ?? '';
        if (!empty($packageid)) {
            $images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $packageid,
                // 'imagetype' => [Korgou_Package_Image::IMAGETYPE_IN_REPO_PACKAGE, Korgou_Package_Image::IMAGETYPE_CHECKLIST],
            ]);
            $this->view('view-images', [
                'images' => $images,
                'packageid' => $packageid
            ]);
        }
    }

    function image_list($packageid)
    {
        if (!empty($packageid)) {
            $images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $packageid,
                // 'imagetype' => [Korgou_Package_Image::IMAGETYPE_IN_REPO_PACKAGE, Korgou_Package_Image::IMAGETYPE_CHECKLIST],
            ]);
            $this->view('image-list', [
                'images' => $images,
                'packageid' => $packageid
            ]);
        }
    }

    function delete_image()
    {
        $data = Shoplic_Util::get_post_data('packageid', 'id');
        do_action('korgou_package_delete_image', $data);

        kg_send_json_success();
    }

    function view_check()
    {
        $packageid = $_GET['packageid'];
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
        ]);
        if ($package != null) {
            $check = apply_filters('korgou_package_get_check', null, [
                'packageid' => $packageid,
            ]);
            $user_images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $packageid,
                'imagetype' => Korgou_Package_Image::IMAGETYPE_USER_PHOTO,
            ]);
            $images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $packageid,
                'imagetype' => [Korgou_Package_Image::IMAGETYPE_CHECKLIST, Korgou_Package_Image::IMAGETYPE_IN_REPO_PACKAGE],
            ]);
            $this->view('view-check', [
                'check' => $check,
                'user_images' => $user_images,
                'images' => $images,
                'package' => $package
            ]);
        }
    }

    function view_disposal()
    {
        $packageid = $_GET['packageid'];
        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
        ]);
        if ($package != null) {
            $disposal = apply_filters('korgou_package_get_disposal', null, [
                'packageid' => $packageid,
            ]);
            $user_images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $packageid,
                'imagetype' => Korgou_Package_Image::IMAGETYPE_USER_DISPOSAL,
            ]);
            $images = apply_filters('korgou_package_get_image_list', [], [
                'packageid' => $packageid,
                'imagetype' => Korgou_Package_Image::IMAGETYPE_DISPOSAL,
            ]);
            $this->view('view-disposal', [
                'disposal' => $disposal,
                'user_images' => $user_images,
                'images' => $images,
                'package' => $package
            ]);
        }
    }

    function update_disposal()
    {
        $packageid = $_POST['packageid'];
        $disposal = Shoplic_Util::get_post_data('cargodetail', 'checkremark', 'disposalfee');
        $conditions = Shoplic_Util::get_post_data('packageid');
        do_action('korgou_package_update_disposal', $disposal, $conditions);

        $package = Shoplic_Util::get_post_data('packageid', 'disposal');
        do_action('korgou_package_update_package', $package);

        if (isset($_POST['imageid']) && !empty($_POST['imageid']))
            do_action('korgou_package_attach_images', $_POST['packageid'], $_POST['imageid'], Korgou_Package_Image::IMAGETYPE_DISPOSAL);

        if ($package['disposal'] == Korgou_PACKAGE::DISPOSAL_CONFIRMED) {
            $package = apply_filters('korgou_package_get_package', null, [
                'packageid' => $packageid,
            ]);
            $email = KG::get_user_email($package->userid);

            $body = sprintf('<p>We have confirmed the application for the disposal service of the requested item (%s).</p><p>You can check the Service Fee of the requested service on the Arrived Packages section.</p><p>If the Deposit is Confirmed, the Disposal Service will be Provided.</p><p>Thank you.</p>', $packageid);
            SP_WC::send_email($email,
                'Your applied package disposal',
                'Your applied package disposal',
                $body
            );
        }

        kg_send_json_success();
    }

    function update_check()
    {
        $packageid = $_POST['packageid'];
        $check = Shoplic_Util::get_post_data('cargodetail', 'checkremark');
        $conditions = Shoplic_Util::get_post_data('packageid');
        do_action('korgou_package_update_check', $check, $conditions);

        $package = Shoplic_Util::get_post_data('packageid', 'checkliststatus');
        // print_r($package); exit;
        do_action('korgou_package_update_package', $package);

        if (isset($_POST['imageid']) && !empty($_POST['imageid']))
            do_action('korgou_package_attach_images', $_POST['packageid'], $_POST['imageid'], Korgou_Package_Image::IMAGETYPE_CHECKLIST);

        $package = apply_filters('korgou_package_get_package', null, [
            'packageid' => $packageid,
        ]);
        $email = KG::get_user_email($package->userid);

        $body = 'Your applied photo service';
        SP_WC::send_email($email,
            'Your applied photo service',
            'Your applied photo service',
            $body,
            apply_filters('korgou_package_get_image_paths', [], ['packageid' => $packageid])
        );

        kg_send_json_success();
    }
};
