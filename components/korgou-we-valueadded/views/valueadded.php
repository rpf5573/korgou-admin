<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('insert'); ?>
                <table class="table table-sm table-bordered">
                    <thead class="thead-light">
                        <th>类型编码</th>
                        <th>Chinese name</th>
                        <th>English name</th>
                        <th>Korean name</th>
                        <th style="width: 80px;">Currency</th>
                        <th>价格</th>
                        <th>Status</th>
                        <th></th>
                    </tbody>
                    <tbody>
                            <tr>
                                <td class="align-middle">
                                    <input type="text" name="type" class="form-control form-control-sm" value="">
                                </td>
                                <td>
                                    <input type="text" name="zhname" class="form-control form-control-sm" value="">
                                </td>
                                <td>
                                    <input type="text" name="enname" class="form-control form-control-sm" value="">
                                </td>
                                <td>
                                    <input type="text" name="koname" class="form-control form-control-sm" value="">
                                </td>
                                <td class="align-middle">
                                    <input type="text" name="currency" class="form-control form-control-sm" value="KRW">
                                </td>
                                <td>
                                    <input type="text" name="price" class="form-control form-control-sm" value="">
                                </td>
                                <td>
                                    <?php BS_Form::select([
                                        'name' => 'status',
                                        'options' => Korgou_Value_Added::$STATUSES
                                    ]); ?>
                                </td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-info btn-xs" id="add-btn">Add</button>
                                </td>
                            </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="card-box">
            <?php $this->ajax_form('update'); ?>
                <table class="table table-sm table-bordered">
                    <thead class="thead-light">
                        <th>类型编码</th>
                        <th>Chinese name</th>
                        <th>English name</th>
                        <th>Korean name</th>
                        <th>Currency</th>
                        <th>价格</th>
                        <th>Status</th>
                        <th>Delete</th>
                    </tbody>
                    <tbody>
                        <?php foreach ($rows as $row): ?>
                            <tr>
                                <td class="align-middle">
                                    <?php echo $row->type; ?>
                                    <input type="hidden" name="type[]" class="form-control form-control-sm" value="<?php echo $row->type; ?>">
                                </td>
                                <td>
                                    <input type="text" name="zhname[]" class="form-control form-control-sm" value="<?php echo $row->zhname; ?>">
                                </td>
                                <td>
                                    <input type="text" name="enname[]" class="form-control form-control-sm" value="<?php echo $row->enname; ?>">
                                </td>
                                <td>
                                    <input type="text" name="koname[]" class="form-control form-control-sm" value="<?php echo $row->koname; ?>">
                                </td>
                                <td class="align-middle">
                                    <?php echo $row->currency; ?>
                                </td>
                                <td>
                                    <input type="text" name="price[]" class="form-control form-control-sm" value="<?php echo $row->price; ?>">
                                </td>
                                <td>
                                    <?php BS_Form::select([
                                        'name' => 'status[]',
                                        'value' => $row->status,
                                        'options' => Korgou_Value_Added::$STATUSES
                                    ]); ?>
                                </td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-danger btn-xs delete-btn" data-type="<?php echo $row->type; ?>">Delete</button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <p class="text-center">
                    <button type="button" id="submit-btn" class="btn btn-primary">Update</button>
                </p>
            </form>
        </div>

    </div>
</div>

<?php $this->ajax_form('delete'); ?>
    <input type="hidden" name="type" value="" id="delete-type">
</form>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        $form.ajaxSubmit((response) => {
            if (response.success) {
                alert(response.data);
                location.reload();
            } else {
                alert("抱歉，系统异常！");
            }
        });
    });
    $('#add-btn').click(function() {
        if (confirm('你确定添加吗？')) {
            $(this).closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                if (response.success) {
                    location.reload();
                } else {
                    alert("抱歉，系统异常！");
                }
            });
        }
    });
    $('.delete-btn').click(function() {
        if (confirm('你确定添加吗？')) {
            $('#delete-type').val($(this).data('type')).closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                if (response.success) {
                    location.reload();
                } else {
                    alert("抱歉，系统异常！");
                }
            });
        }
    });
});
</script>

