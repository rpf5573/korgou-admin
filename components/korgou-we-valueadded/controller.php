<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_valueadded';

    function load()
    {
        $this->add_shortcode();
        $this->add_ajax('update');
        $this->add_ajax('insert');
        $this->add_ajax('delete');
    }

    function shortcode()
    {
        $context = [
            'rows' => apply_filters('korgou_get_value_added_list', [], []),
        ];
        $this->view('valueadded', $context);
    }

    function update()
    {
        $rows = Shoplic_Util::get_post_data_array('type', 'zhname', 'enname', 'koname', 'price', 'status');

        foreach ($rows as $row) {
            do_action('korgou_update_value_added', $row);
        }

        kg_send_json_success();
    }

    function insert()
    {
        $data = Shoplic_Util::get_post_data('type', 'zhname', 'enname', 'koname', 'price', 'status');
        do_action('korgou_insert_value_added', $data);
        kg_send_json_success();
    }

    function delete()
    {
        $data = Shoplic_Util::get_post_data('type');
        if (apply_filters('korgou_get_value_added', null, $data) != null) {
            do_action('korgou_delete_value_added', $data);
            kg_send_json_success();
        }

        wp_send_json_error('Value added not available');
    }
};


