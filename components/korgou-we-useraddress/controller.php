<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_useraddress';

    function load()
    {
        $this->add_shortcode();
        $this->add_shortcode('edit');

        $this->add_ajax('load_address');
        $this->add_ajax('update_address');
    }

    function shortcode()
    {
        $context = [];
        $this->view('useraddress', $context);
    }

    function edit()
    {
        $id = $_GET['id'];
        $context = [
            'address' => apply_filters('korgou_user_get_address', null, ['id' => $id]),
        ];
        $this->view('edit', $context);
    }

    function update_address()
    {
        $data = Shoplic_Util::get_post_data('id', 'userid', 'country', 'englishname', 'nativename', 'zipcode',
            'province', 'city', 'addressdetail', 'company', 'phonenum', 'mobilenum');
        do_action('korgou_user_update_address', $data);
        kg_send_json_success();
    }

    function load_address()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('userid', 'country');

        $page = apply_filters('korgou_user_get_address_page', [], $args, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $data[] = [
                sprintf('<a class="user-btn" href="/useraddress/edit/?id=%d">%s</a>', $item->id, korgou_user_role_id($item->userid)),
                $item->country,
                $item->englishname,
                $item->nativename,
                $item->zipcode,
                $item->province,
                $item->city,
                $item->addressdetail,
                $item->company,
                $item->phonenum,
                $item->mobilenum,
                Korgou_User_Address::$ASDEFAULTS[$item->asdefault],
                $item->createtime,
                $item->updatetime,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }
};
