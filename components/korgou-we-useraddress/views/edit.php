<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_address'); ?>
                <table class="table-bordered table table-hover w-50">
                <tbody>
                    <tr>
                        <th>User ID</th>
                        <td class="user-role-wrap">
                            <input type="hidden" name="id" value="<?php echo $address->id; ?>">
                            <?php korgou_user_role_icon($address->userid, true); ?>
                            <input type="text" readonly class="form-control-plaintext" name="userid" value="<?php echo $address->userid; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td>
                            <?php do_action('korgou_select_country', $address->country); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>English name</th>
                        <td><input type="text" name="englishname" class="form-control" value="<?php echo $address->englishname; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Local name</th>
                        <td><input type="text" class="form-control" name="nativename" value="<?php echo $address->nativename; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Zip code</th>
                        <td><input type="text" class="form-control" name="zipcode" value="<?php echo $address->zipcode; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Province</th>
                        <td><input type="text" class="form-control" name="province" value="<?php echo $address->province; ?>" /></td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td><input type="text" class="form-control" name="city" value="<?php echo $address->city; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Detailed Address</th>
                        <td><input type="text" class="form-control" name="addressdetail" value="<?php echo $address->addressdetail; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td><input type="text" class="form-control" name="company" value="<?php echo $address->company; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Telephone</th>
                        <td><input type="text" class="form-control" name="phonenum" value="<?php echo $address->phonenum; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Cellphone</th>
                        <td><input type="text" class="form-control" name="mobilenum" value="<?php echo $address->mobilenum; ?>" /></td>
                    </tr>
                </tbody>
                </table>
                <p>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <?php if (current_user_can('kg_edit_user')): ?>
                        <button type="button" class="btn btn-primary submit-btn">Save changes</button>
                    <?php endif; ?>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>
