<?php
defined( 'ABSPATH' ) or exit;

use PhpOffice\PhpSpreadsheet\IOFactory;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_forwardfare';

    function load()
    {
        $this->add_shortcode();
        $this->add_shortcode('upload');
        $this->add_shortcode('mapping');
        $this->add_ajax('upload_fare');
        $this->add_ajax('save_fare');
        $this->add_ajax('load_fare');
        $this->add_ajax('load_country');
        $this->add_ajax('add_fare');
        $this->add_ajax('delete_fare');
        $this->add_ajax('update_fare');
        $this->add_ajax('map_country');
    }

    function load_country()
    {
        $courierid = $_POST['courierid'];

        $countries = apply_filters('korgou_forward_get_fare_countries', [], $courierid);

        if ($countries) {
            echo '<option value=""> - Please choose -';
            foreach ($countries as $country) {
                echo '<option>' . $country;
            }
        }
        
        die;
    }

    function shortcode()
    {
        $couriers = [];
        foreach (apply_filters('korgou_forward_get_courier_list', []) as $courier) {
            $couriers[$courier->id] = $courier->enname;
        }
        $context = [
            'couriers' => $couriers,
        ];
        $this->view('forwardfare', $context);
    }

    function upload()
    {
        $context = [
            'courier' => apply_filters('korgou_forward_get_courier', null, ['id' => $_GET['id']]),
        ];
        $this->view('upload', $context);
    }

    function mapping()
    {
        $id = $_GET['id'];
        $courier_countries = [];
        foreach (apply_filters('korgou_forward_get_fare_countries', [], $id) as $country) {
            $courier_countries[$country] = [];
        }

        foreach (apply_filters('korgou_forward_get_courier_country_list', [], ['forwardcourierid' => $id]) as $row) {
            $courier_countries[$row->country][] = $row->user_country;
        }

        $context = [
            'courier' => apply_filters('korgou_forward_get_courier', null, ['id' => $id]),
            'courier_countries' => $courier_countries,
        ];
        $this->view('mapping', $context);
    }

    function upload_fare()
    {
        $this->lib('vendor/autoload');
        $inputFileName = $_FILES["file"]["tmp_name"];  // ABSPATH . 'wp-content/uploads/dhl.xlsx';
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $couriers = [];
        foreach (apply_filters('korgou_forward_get_courier_list', []) as $courier) {
            $couriers[$courier->id] = $courier->enname;
        }
        $context = [
            'couriers' => $couriers,
            'sheet' => $sheetData,
            'courierid' => $_REQUEST['courierid'],
        ];
        $this->view('sheet', $context);
        die;
    }

    function save_fare()
    {
        $courierid = $_POST['courierid'];
        $updatetime = current_time('mysql');
        $weight_zone = $_POST['format'] == 'weight-zone';
        $weight_kg = $_POST['weight_unit'] == 'kg';
        $rows = json_decode(stripslashes($_POST['data']), true);
        $fare = [];
        foreach ($rows as $row) {
            if ($weight_zone) {
                $weight = str_replace(',', '', $row['y']);
                $fare['country'] = $row['x'];
            } else {
                $weight = str_replace(',', '', $row['x']);
                $fare['country'] = $row['y'];
            }
            $fare['price'] = str_replace(',', '', $row['price']);
            $fare['weight'] = $weight_kg ? $weight * 1000 : $weight;
            $fare['forwardcourierid'] = $courierid;
            $fare['updatetime'] = $updatetime;

            do_action('korgou_forward_replace_fare', $fare);
        }
        
        kg_send_json_success();
    }

    function load_fare()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('country', 'forwardcourierid');

        $page = apply_filters('korgou_forward_get_fare_page', [], $args, $rowcount, $paged);

        $data = [];
        foreach (apply_filters('korgou_forward_get_courier_list', []) as $courier) {
            $couriers[$courier->id] = $courier->enname;
        }
        foreach ($page->items as $item) {
            $packagelist = '';
            // print_r(explode(',', $item->packagelist));
            foreach (explode(',', $item->packagelist) as $i => $pid) {
                // print_r($i . ':' . $pid . '<br>');
                if ($i > 0) {
                    $packagelist .= ',';
                    if ($i % 3 == 0)
                        $packagelist .= '<br>';
                }
                $packagelist .= $pid;
            }

            $data[] = [
                sprintf('<input type="checkbox" class="check" value="%1$s">', $item->id),
                sprintf('<input type="hidden" name="id[]" value="%1$d">%1$s', $item->id),
                $couriers[$item->forwardcourierid],
                $item->country,
                sprintf('<input type="text" class="form-control form-control-sm" name="weight[]" value="%d">', $item->weight),
                sprintf('<input type="text" class="form-control form-control-sm" name="price[]" value="%d">', $item->price),
                sprintf('<input type="text" class="form-control form-control-sm" name="servicefee[]" value="%d">', $item->servicefee),
                $item->updatetime,
                sprintf('<button type="button" class="delete-btn btn btn-sm btn-danger" data-id="%d">Delete</button>', $item->id),
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function add_fare()
    {
        $data = Shoplic_Util::get_post_data('country', 'forwardcourierid', 'weight');

        do_action('korgou_forward_delete_fare', $data);

        $data['price'] = $_POST['price'];
        $data['servicefee'] = $_POST['servicefee'];
        $data['updatetime'] = current_time('mysql');

        do_action('korgou_forward_insert_fare', $data);

        kg_send_json_success();
    }

    function delete_fare()
    {
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $data = [];
            foreach (explode(',', $_POST['id']) as $id) {
                $data['id'] = $id;
                do_action('korgou_forward_delete_fare', $data);
            }
        }

        kg_send_json_success();
    }

    function update_fare()
    {
        $rows = Shoplic_Util::get_post_data_array('id', 'weight', 'price', 'servicefee');

        foreach ($rows as $data) {
            $data['updatetime'] = current_time('mysql');
            do_action('korgou_forward_update_fare', $data);
        }

        kg_send_json_success();
    }

    function map_country()
    {
        $courierid = $_REQUEST['courierid'];

        do_action('korgou_forward_delete_courier_country', [
            'forwardcourierid' => $courierid,
        ]);
        
        foreach ($_REQUEST['user_country'] as $country => $user_countries) {
            foreach ($user_countries as $user_country) {
                do_action('korgou_forward_insert_courier_country', [
                    'forwardcourierid' => $courierid,
                    'country' => $country,
                    'user_country' => $user_country,
                ]);
            }
        }

        kg_send_json_success();
    }
};

