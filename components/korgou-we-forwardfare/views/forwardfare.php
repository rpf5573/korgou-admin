<section id="section-1">

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                    'name' => 'forwardcourierid', 'type' => 'select', 'label' => 'Courier', 'options' => $couriers,
                ], [
                    'name' => 'country', 'type' => 'select', 'label' => 'Country',
                ]], '<a class="btn btn-info mb-2 ml-2 upload-btn" href="upload/?id=">Upload</a><a class="btn btn-success mb-2 ml-3 mapping-btn" href="mapping/?id=">Country mapping</a>');
                ?>

                <?php
                $this->ajax_form('update_fare');

                do_action('korgou_we_datatable', [
                    '<input type="checkbox" class="check-all">', 'Number', 'Courier', 'Country', 'Weight(g)', 'Shipping fee',
                    'Service charge', 'Update', 'Delete'
                ], [],
                    'forwardfare_load_fare'
                );
                ?>

                <div class="mt-2 text-center">
                    <button type="button" class="btn btn-info update-btn">Update</button>
                    <button type="button" class="btn btn-danger delete-checked-btn">Delete</button>
                </div>

                </form>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<?php $this->ajax_form('delete_fare'); ?>
    <input type="hidden" name="id" id="delete-id">
</form>

<?php $this->ajax_form('load_country'); ?>
    <input type="hidden" name="courierid" id="country-courier-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.datatable').on('click', 'a.package-btn', function() {
        console.log($(this).attr('href'));
        $('#section-1').hide();
        $('#section-2').load($(this).attr('href'));
        return false;
    });
    $('#section-1').on('click', '.delete-btn', function() {
		if(confirm("Are you sure to delete?")) {
            $('#delete-id').val($(this).data('id')).closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });
    $('#section-1').on('click', '.delete-checked-btn', function() {
        if ($('.datatable .check:checked').length == 0) {
            alert('Please select fares to delete.');
            return false;
        }
		if(confirm("Are you sure to delete?")) {
            var ids = [];
            $('.datatable .check:checked').each(function() {
                ids.push($(this).val());
            });
            $('#delete-id').val(ids.join(',')).closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });
    $('#section-1').on('click', '.update-btn', function() {
		if(confirm("Are you sure to update?")) {
            $(this).closest('form').ajaxSubmit((response) => {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });
    $('#section-2').on('click', '.cancel-btn', function() {
        $('#section-2').html('');
        $('#section-1').show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });
    $('select[name="forwardcourierid"]').change(function() {
        $('#country-courier-id').val($(this).val()).closest('form').ajaxSubmit(function(response) {
            $('select[name="country"]').html(response);
        });
        return false;
    });
    $('.check-all').click(function() {
        $('.datatable .check').prop('checked', $(this).is(':checked'));
    });
    $('.upload-btn').click(function() {
        if ($('select[name="forwardcourierid"]').val() == '') {
            alert('Please select a courier.');
            return false;
        }
        $(this).attr('href', $(this).attr('href')+$('select[name="forwardcourierid"]').val());
    });
    $('.mapping-btn').click(function() {
        if ($('select[name="forwardcourierid"]').val() == '') {
            alert('Please select a courier.');
            return false;
        }
        $(this).attr('href', $(this).attr('href')+$('select[name="forwardcourierid"]').val());
    });
});
</script>
