<section id="section-1">

    <?php
    do_action('korgou_we_add_form', 'forwardfare_add_forwardfare', [[
        'name' => 'country', 'type' => 'country', 'label' => 'Country',
    ], [
        'name' => 'forwardcourierid', 'type' => 'select', 'label' => 'Couriers', 'options' => $couriers,
    ], [
       'type' => 'spacer',
    ], [
        'name' => 'weight', 'type' => 'text', 'label' => 'Weight(g)',
    ], [
        'name' => 'price', 'type' => 'text', 'label' => 'Shipping fee(KRW)',
    ], [
        'name' => 'servicefee', 'type' => 'text', 'label' => 'Service charge(KRW)',
    ]]);
    ?>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                    'name' => 'forwardcourierid', 'type' => 'select', 'label' => 'Couriers', 'options' => $couriers,
                ], [
                    'name' => 'country', 'type' => 'country', 'label' => 'Country',
                ]]);
                ?>

                <?php
                $this->ajax_form('update_forwardfare');

                do_action('korgou_we_datatable', [
                    'Number', 'Couriers', 'Country', 'Weight', 'Shipping fee',
                    'Service charge', 'Update', 'Delete'
                ], [],
                    'forwardfare_load_forwardfare'
                );
                ?>

                <div class="mt-2 text-center">
                    <button type="button" class="btn btn-info update-btn">Update</button>
                </div>

                </form>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<?php $this->ajax_form('delete_forwardfare'); ?>
    <input type="hidden" name="id" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.datatable').on('click', 'a.package-btn', function() {
        console.log($(this).attr('href'));
        $('#section-1').hide();
        $('#section-2').load($(this).attr('href'));
        return false;
    });
    $('#section-1').on('click', '.delete-btn', function() {
		if(confirm("Are you sure to delete?")) {
            $('#delete-id').val($(this).data('id')).closest('form').ajaxSubmit((response) => {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });
    $('#section-1').on('click', '.update-btn', function() {
		if(confirm("Are you sure to delete?")) {
            $(this).closest('form').ajaxSubmit((response) => {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });
    $('#section-2').on('click', '.cancel-btn', function() {
        $('#section-2').html('');
        $('#section-1').show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });
});
</script>
