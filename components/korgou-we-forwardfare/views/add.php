<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('upload'); ?>
                <div class="form-group row">
                    <label class="col-sm-2">Courier</label>
                    <div class="col-sm-10">
                        <select class="corm-control"></select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Format</label>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
                            <label class="form-check-label mr-2" for="inlineRadio1">Weight ＼ Zone</label>

                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="2">
                            <label class="form-check-label" for="inlineRadio1">Zone ＼ Weight</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">File</label>
                    <div class="col-sm-10">
                        <input type="file">
                    </div>
                </div>
                <div class="form-group row">
                    <button type="button" class="btn btn-info update-btn">Upload</button>
                </div>
            </form>
            
        </div>

    </div>
</div>
