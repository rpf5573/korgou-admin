<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('upload_fare'); ?>
                <input type="hidden" name="courierid" value="<?php echo $courier->id; ?>">
                <div class="form-group row">
                    <label class="col-sm-2">Courier</label>
                    <div class="col-sm-4">
                        <?php echo $courier->enname; ?>
                    </div>
                    <label class="col-sm-2">File</label>
                    <div class="col-sm-4">
                        <input type="file" class="form-control-file" id="fare-file" name="file">
                    </div>
                </div>
            </form>
            
            <article id="sheet-area">
                <a href="#" onclick="history.back();" class="btn btn-secondary">Cancel</a>
            </article>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('#fare-file').change(function() {
        $(this).closest('form').ajaxSubmit(function(response) {
            $('#sheet-area').html(response);
        });
    });
});
</script>
