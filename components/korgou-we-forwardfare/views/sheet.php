<?php $this->ajax_form('save_fare'); ?>
    <input type="hidden" name="courierid" value="<?php echo $courierid; ?>">

    <!--
    <div class="form-group row">
        <label class="col-sm-2">Courier</label>
        <div class="col-sm-10">
            <?php
            BS_Form::select([
                'name' => 'courierid',
                'options' => $couriers,
                'style' => 'width: initial;',
                'blank_option' => '',
            ]);
            ?>
        </div>
    </div>
    -->
    <div class="form-group row">
        <label class="col-sm-2">Format</label>
        <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="format" id="inlineRadio1" value="weight-zone" checked>
                <label class="form-check-label mr-4" for="inlineRadio1">Weight ＼ Country</label>

                <input class="form-check-input" type="radio" name="format" id="inlineRadio2" value="zone-weight">
                <label class="form-check-label" for="inlineRadio2">Country ＼ Weight</label>
            </div>
        </div>
        <label class="col-sm-2">Weight unit</label>
        <div class="col-sm-4">
            <div class="form-check">
                <input class="form-check-input" type="radio" name="weight_unit" id="inlineRadio3" value="g" checked>
                <label class="form-check-label mr-4" for="inlineRadio3">g</label>

                <input class="form-check-input" type="radio" name="weight_unit" id="inlineRadio4" value="kg">
                <label class="form-check-label" for="inlineRadio4">kg</label>
            </div>
        </div>
    </div>

    <div id="sheet-data" class="mb-3" style="overflow: auto;">
        <table class="table table-bordered table-sm">
            <thead>
            <tr>
                <?php $columns = array_shift($sheet); ?>
                <?php foreach ($columns as $column => $cell): if (empty($cell)) continue; ?>
                    <th>
                        <?php echo $cell; ?>
                    </th>
                <?php endforeach; ?>
            </tr>
            <tbody>
            <?php foreach ($sheet as $row): $y = array_shift($row); if (empty($y)) continue; ?>
                <tr>
                    <th>
                        <?php echo $y; ?>
                    </th>
                    <?php foreach ($row as $column => $cell): if (empty($cell)) continue; ?>
                        <td class="cell-data" data-x="<?php echo trim($columns[$column]); ?>" data-y="<?php echo trim($y); ?>" data-price="<?php echo trim($cell); ?>">
                            <?php echo $cell; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <p class="text-center">
        <a href="#" onclick="history.back();" class="btn btn-secondary">Cancel</a>
        <input type="submit" class="btn btn-primary" id="save-btn" value="Save">
        <input type="hidden" name="data">
    </p>
</form>

<script type="text/javascript">
jQuery(function($) {
    $('#save-btn').click(function() {
        if ($('select[name="courierid"]').val() == '') {
            alert('Please select a courier.');
            $('select[name="courierid"]').focus();
            return false;
        }
        var data = [];
        $('.cell-data').each(function() {
            data.push({
                x: $(this).data('x'),
                y: $(this).data('y'),
                price: $(this).data('price')
            });
        });
        $(this).next().val(JSON.stringify(data));
        $(this).closest('form').ajaxSubmit(function(response) {
            if (response.success) {
                alert('Successful operation');
                location.href = '/forwardfare/';
            } else {
                alert(response.data);
            }
        });
        return false;
    });
    // $('#sheet-data').width($('#sheet-data').prev().width());
    // $('#sheet-data').height($(window).height() - 368);
});
</script>
