<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('map_country'); ?>
                <input type="hidden" name="courierid" value="<?php echo $courier->id; ?>">
                <div class="form-group row">
                    <label class="col-sm-2">Courier</label>
                    <div class="col-sm-10">
                        <?php echo $courier->enname; ?>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 16.66667%;">Courier country</th>
                            <th>User country</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($courier_countries as $country => $user_countries): ?>
                            <tr>
                                <td>
                                    <?php echo $country; ?>
                                </td>
                                <td sttyle="padding: .65rem .85rem .85rem .85rem;">
                                    <?php do_action('korgou_select2m_country', $user_countries, ['name' => "user_country[$country][]"]); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <p class="text-center">
                    <a href="#" onclick="history.back();" class="btn btn-secondary">Cancel</a>
                    <input type="submit" class="btn btn-primary" id="save-btn" value="Save">
                </p>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('[data-toggle="select2"]').select2();
    $('#save-btn').click(function() {
        $(this).closest('form').ajaxSubmit(function(response) {
            if (response.success) {
                alert('Successful operation');
                location.href = '/forwardfare/';
            } else {
                alert(response.data);
            }
        });
        return false;
    });
});
</script>
