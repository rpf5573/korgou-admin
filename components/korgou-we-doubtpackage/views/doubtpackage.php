<section id="section-1">

<!--
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php $this->ajax_form('add_doubtpackage', ['class' => 'form-inline']); ?>
                    <label>Domestic tracking number : </label>
                    <input type="text" class="form-control mx-2" name="trackno">
                    <label>识别困难缘由 : </label>
                    <?php BS_Form::select([
                        'name' => 'reason',
                        'class' => 'mx-2',
                        'blank_option' => '- Please choose -',
                        'options' => Korgou_Doubt_Package::$REASONS,
                    ]); ?>
                    <button type="button" class="btn btn-info" id="add-doubt-package-btn">Add</button>
                </form>
            </div>
        </div>
    </div>
-->

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [
                    [
                        'name' => 'trackno', 'type' => 'text', 'label' => 'Tracking number',
                    ], 
                    [
                        'name' => 'status', 'type' => 'select', 'label' => 'Status', 'options' => Korgou_Doubt_Package::$STATUSES,
                    ], 
                ]);
                ?>

                <?php
                do_action('korgou_we_datatable', [
                    'Number', 'Date', 'End Time', 'Tracking number', 'Courier', 'Reason',
                    'Status', 'User ID', 'Package Contents', 'User comments', 'Operation'
                ], [],
                    'doubtpackage_load_doubtpackage'
                );
                ?>
                
                <div class="text-center mt-4">
                    <a href="/doubtpackage/add/" class="btn btn-info">Add</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="section-2"></section>

<?php $this->ajax_form('claim_succ'); ?>
    <input type="hidden" name="id" id="claim-succ-id">
</form>

<?php $this->ajax_form('claim_fail'); ?>
    <input type="hidden" name="id" id="claim-fail-id">
</form>

<?php $this->ajax_form('delete'); ?>
    <input type="hidden" name="id" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('section').on('click', 'a.link', function() {
        var href = $(this).attr('href');
        var $section = $(this).closest('section');
        $section.hide();
        $section.next().data('href', href).load(href);
        return false;
    });
    $('section').on('click', '.delete-btn', function() {
        if (confirm('Are you sure?')) {
            $('#delete-id').val($(this).data('id'));
            $('#delete-id').closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                if (response.success)
                    $datatable.draw();
            });
        }
        return false;
    });
    $('section').on('click', '.cancel-btn', function() {
        var $section = $(this).closest('section');
        $section.html('');
        $section.prev().show();
    });
    var $btn = $('#add-doubt-package-btn'), $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        if (confirm('Are you sure?')) {
            $form.ajaxSubmit((response) => {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });

    $(document).on('click', '.claimsucc-btn', function() {
        if (confirm('Are you sure?')) {
            var href = $(this).data('href');
            $('#claim-succ-id').val($(this).data('id'));
            $('#claim-succ-id').closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                if (response.success)
                    window.open(href);
                else
                    $datatable.draw();
            });
        }
        return false;
    });

    $(document).on('click', '.claimfail-btn', function() {
        if (confirm('Are you sure?')) {
            $('#claim-fail-id').val($(this).data('id'));
            $('#claim-fail-id').closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                $datatable.draw();
            });
        }
        return false;
    });
    
    $('#section-2').on('click', '.cancel-btn', function() {
        $('#section-2').html('');
        $('#section-1').show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });
});
</script>
