<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php $this->ajax_form('add_doubtpackage'); ?>
                    <table class="table table-bordered w-50">
                    <thead class="thead-light">
                        <tr>
                            <th style="width: 40px;"></th>
                            <th>Tracking No.</th>
                            <th>Courier</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i = 1; $i <= 10; $i++): ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td>
                                    <input type="text" class="form-control" name="trackno[]">
                                </td>
                                <td>
                                    <select class="form-control" name="courier[]">
                                        <option value="">Select courier</option>
                                        <?php foreach(array_values(Korgou_Package::$KOREA_COURIERS) as $courier): ?>
                                            <option><?php echo $courier; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </tr>
                        <?php endfor; ?>
                    </tbody>
                    </table>

                    <div>
                        <a href="#" onclick="history.back();" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary" id="add-doubt-package-btn">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#add-doubt-package-btn');
    $btn.closest('form').ajaxForm({
        beforeSubmit: function() {
            return confirm('Are you sure?');
        },
        success: function(response) {
            if (response.success) {
                alert('Success');
                location.href = '/doubtpackage/';
            } else {
                alert(response.data);
            }
        }
    });
});
</script>
