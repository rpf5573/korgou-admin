<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_doubtpackage';

    function load()
    {
        $this->add_shortcode();
        $this->add_shortcode('add');
        $this->add_ajax('load_doubtpackage');
        $this->add_ajax('add_doubtpackage');
        $this->add_ajax('delete');
        $this->add_ajax('claim_succ');
        $this->add_ajax('claim_fail');
    }

    function shortcode()
    {
        $context = [
        ];
        $this->view('doubtpackage', $context);
    }

    function add()
    {
        $context = [
        ];
        $this->view('add', $context);
    }

    function load_doubtpackage()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('trackno', 'status');

        $conditions = [];
        if (!empty($args['trackno']))
            $conditions[] = ['trackno', 'LIKE', '%' . $args['trackno'] . '%'];
        if (!empty($args['status']))
            $conditions[] = ['status', '=', $args['status']];

        $page = apply_filters('korgou_get_doubt_package_page', [], $args, $conditions, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $packagelist = '';
            // print_r(explode(',', $item->packagelist));
            foreach (explode(',', $item->packagelist) as $i => $pid) {
                // print_r($i . ':' . $pid . '<br>');
                if ($i > 0) {
                    $packagelist .= ',';
                    if ($i % 3 == 0)
                        $packagelist .= '<br>';
                }
                $packagelist .= $pid;
            }

            $operations = sprintf('<a class="link btn btn-xs btn-primary mr-1" href="/package/view-images/?packageid=%1$s">View</a><button type="button" class="delete-btn btn btn-xs btn-danger mr-1" data-id="%1$s">Delete</button>', $item->id);
            if ( $item->status == Korgou_Doubt_Package::STATUS_CLAIMING )
                $operations .= 
                    sprintf('<button type="button" class="claimsucc-btn btn btn-xs btn-success mr-1" data-id="%1$s" data-href="/package/newpackage/?userid=%2$s&trackno=%3$s">Claim success</button><button type="button" class="claimfail-btn btn btn-xs btn-warning" data-id="%1$s">Claim failure</button>', $item->id, $item->claimuserid, $item->trackno);

            $data[] = [
                sprintf('<a class="link" href="/package/view-images/?packageid=%1$s">%1$s</a>', $item->id),
                $item->receivetime,
                $item->stoptime,
                $item->trackno,
                $item->courier,
                $item->reason == '1' ? '运单收件信息打印不全或被遮挡' : '--',
                Korgou_Doubt_Package::$STATUSES[$item->status] ?? '--',
                $item->claimuserid,
                $item->claimpackagecontent,
                $item->claimnote,
                $operations,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function add_doubtpackage()
    {
        $rows = Shoplic_Util::get_post_data_array('trackno', 'courier');
        foreach ($rows as $data) {
            if (empty($data['trackno']))
                continue;

            $data['status'] = Korgou_Doubt_Package::STATUS_WAIT;
            $data['receivetime'] = current_time('mysql');

            do_action('korgou_insert_doubt_package', $data);
        }

        kg_send_json_success();
    }

    function delete()
    {
        do_action('korgou_delete_doubt_package', $_POST['id']);

        kg_send_json_success();
    }

    function claim_succ()
    {
        $data = [
            'stoptime' => current_time('mysql'),
            'status' => Korgou_Doubt_Package::STATUS_CLAIM_SUCC,
        ];
        $condition = [
            'id' => $_POST['id'],
            'status' => Korgou_Doubt_Package::STATUS_CLAIMING,
        ];
        
        do_action('korgou_update_doubt_package', $data, $condition);

        kg_send_json_success();
    }

    function claim_fail()
    {
        $data = [
            'stoptime' => current_time('mysql'),
            'status' => Korgou_Doubt_Package::STATUS_CLAIM_FAIL,
        ];
        $condition = [
            'id' => $_POST['id'],
            'status' => Korgou_Doubt_Package::STATUS_CLAIMING,
        ];
        
        do_action('korgou_update_doubt_package', $data, $condition);

        $doubt = apply_filters('korgou_get_doubt_package', null, ['id' => $_POST['id']]);

        $data = [
            'trackno' => $doubt->trackno,
            'courier' => $doubt->courier,
            'reason' => $doubt->reason,
            'receivetime' => $doubt->receivetime,
            'status' => Korgou_Doubt_Package::STATUS_WAIT,
        ];

        do_action('korgou_insert_doubt_package', $data);

        kg_send_json_success();
    }
};

