<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                    'name' => 'userid', 'type' => 'text', 'label' => 'User ID'
                ]]);
                ?>

                <?php
                do_action('korgou_we_datatable', [
                    'User ID', 'Transport discount', 'Substitute discount', 'Delete'
                ], [],
                    'userdiscount_load_userdiscount'
                );
                ?>
                
            </div>

        </div>
    </div>
</section>

<section id="section-2"></section>

<script type="text/javascript">
jQuery(function($) {
    $('.datatable').on('click', 'a.package-btn', function() {
        console.log($(this).attr('href'));
        $('#section-1').hide();
        $('#section-2').load($(this).attr('href'));
        return false;
    });
    $('#section-2').on('click', '.cancel-btn', function() {
        $('#section-2').html('');
        $('#section-1').show();
    });
    $('#section-2').on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit((response) => {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });
});
</script>
