<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_userdiscount';

    function load()
    {
        $this->add_shortcode();
        $this->add_ajax('load_userdiscount');
    }

    function shortcode()
    {
        $context = [
        ];
        $this->view('userdiscount', $context);
    }

    function load_userdiscount()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('userid');

        $page = apply_filters('korgou_user_get_discount_page', null, $args, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $data[] = [
                sprintf('<input type="hidden" name="userid" value="%1$d">%1$s', $item->userid),
                sprintf('<input type="text" class="form-control form-control-sm" name="forwarddiscount" value="%d">', $item->forwarddiscount),
                sprintf('<input type="text" class="form-control form-control-sm" name="assistpaydiscount" value="%d">', $item->assistpaydiscount),
                sprintf('<button type="button" class="delete-btn btn btn-sm btn-danger" data-userid="%d">Delete</button>', $item->userid),
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

};

