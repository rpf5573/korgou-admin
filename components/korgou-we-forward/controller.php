<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we_forward';

    function load()
    {
        $this->add_shortcode('print');
        $this->add_shortcode('courier');
        $this->add_shortcode('forwardmanage');
        $this->add_shortcode('detail');
        $this->add_shortcode('history');
        $this->add_shortcode('weight_fee');
        $this->add_shortcode('start');
        $this->add_shortcode('status');
        $this->add_shortcode('address');
        $this->add_ajax('cancel');
        $this->add_ajax('refuse_cancel');
        $this->add_ajax('add_courier');
        $this->add_ajax('update_address');
        $this->add_ajax('update_courier');
        $this->add_ajax('update_remark');
        $this->add_ajax('delete_courier');
        $this->add_ajax('load_forward');
        $this->add_ajax('update_weight_fee');
        $this->add_ajax('calculate_forward_fee');
        $this->add_ajax('deduct_money');
        $this->add_ajax('start_forward');
        $this->add_ajax('change_status');
    }

    function print()
    {
        $forwards = [];
        $forwardids = $_REQUEST['forwardid'];
        foreach ($forwardids as $forwardid) {
            $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
            if ($forward != null) {
                $forwards[] = $forward;
                $forward->packages = apply_filters('korgou_package_get_package_list', [], [
                    'packageid' => explode(',', $forward->packagelist),
                ]);
                $forward->vas = apply_filters('korgou_forward_get_value_added_forward_list', [], ['forwardid' => $forwardid]);

                do_action('korgou_forward_print', $forwardid);
            }
        }

        usort($forwards, function($f1, $f2) {
            $u1 = substr($f1->userid, 0, 1) . substr($f1->userid, -4);
            $u2 = substr($f2->userid, 0, 1) . substr($f2->userid, -4);
            return strcmp($u1, $u2);
        });

        $this->view('print', ['forwards' => $forwards]);
    }

    function cancel()
    {
        $data = Shoplic_Util::get_post_data('forwardid');
        if (isset($data['forwardid']) && !empty($data['forwardid'])) {
            $forward = apply_filters('korgou_forward_get_forward', null, $data);

            $data['status'] = Korgou_Forward::STATUS_CANCEL;
            do_action('korgou_forward_update_forward', $data);

            foreach (explode(',', $forward->packagelist) as $packageid) {
                $package = [
                    'packageid' => $packageid,
                    'status' => Korgou_Package::STATUS_IN_REPO,
                ];
                do_action('korgou_package_update_package', $package);
            }
            kg_send_json_success();
        }

        wp_send_json_error('forwardid N/A');
    }

    function refuse_cancel()
    {
        $data = Shoplic_Util::get_post_data('forwardid');
        if (isset($data['forwardid']) && !empty($data['forwardid'])) {
            $data['status'] = Korgou_Forward::STATUS_CANNOT_BE_CANCELED;
            do_action('korgou_forward_update_forward', $data);
            kg_send_json_success();
        }

        wp_send_json_error('forwardid N/A');
    }

    function courier()
    {
        $context = [
            'couriers' => apply_filters('korgou_forward_get_courier_list', [], []),
        ];
        $this->view('courier', $context);
    }

    function forwardmanage()
    {
        $couriers = [];
        foreach (apply_filters('korgou_forward_get_courier_list', [], []) as $courier) {
            $couriers[$courier->id] = $courier->enname;
        }
        $context = [
            'couriers' => $couriers,
        ];
        $this->view('forwardmanage', $context);
    }

    function address()
    {
        $forwardid = $_GET['forwardid'];
        if (!isset($forwardid) || empty($forwardid))
            return;

        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null)
            return;
        
        $this->view('address', [
            'forward' => $forward,
            'couriers' => apply_filters('korgou_forward_get_courier_list', [], ['status' => Korgou_Forward_Courier::STATUS_OPEN]),
        ]);
    }

    function detail()
    {
        $forwardid = $_GET['forwardid'];
        if (!isset($forwardid) || empty($forwardid))
            return;

        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null)
            return;

        $packages = apply_filters('korgou_package_get_package_list', [], [
            'packageid' => explode(',', $forward->packagelist),
        ]);
        $customs = apply_filters('korgou_forward_get_customs_declaration_list', [], ['forwardid' => $forwardid]);
        $value_addeds = apply_filters('korgou_forward_get_value_added_forward_list', [], ['forwardid' => $forwardid]);

        $context = [
            'forward' => $forward,
            'packages' => $packages,
            'customs' => $customs,
            'vas' => $value_addeds,
            'history' => apply_filters('korgou_forward_get_history_list', [], ['forwardid' => $forwardid]),
        ];
        $this->view('detail', $context);
    }

    function history()
    {
        $id = $_GET['id'];
        if (!isset($id) || empty($id))
            return;

        $forward = apply_filters('korgou_forward_get_history_forward', null, ['id' => $id]);
        if ($forward == null)
            return;

        $forwardid = $forward->forwardid;

        $context = [
            'history' => $history,
            'forward' => $forward,
        ];
        $this->view('history/detail', $context);
    }

    function update_courier()
    {
        $couriers = Shoplic_Util::get_post_data_array('id', 'zhname', 'enname', 'koname', 'status');

        foreach ($couriers as $co) {
            do_action('korgou_forward_update_courier', $co);
        }

        kg_send_json_success();
    }

    function update_address()
    {
        $data = Shoplic_Util::get_post_data('forwardid', 'country', 'englishname', 'nativename', 'zipcode',
            'province', 'city', 'addressdetail', 'company', 'phonenum', 'mobilenum', 'forwardcourierid', 'forwardcouriertrackno');
        if (!empty($data['forwardcourierid'])) {
            $courier = apply_filters('korgou_forward_get_courier', null, ['id' => $data['forwardcourierid']]);
            if ($courier != null)
                $data['forwardcouriername'] = $courier->zhname . '-' . $courier->enname;
        }

        $prev_forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $data['forwardid']]);

        do_action('korgou_forward_update_forward', $data);

        if ($prev_forward != null && !empty($prev_forward->country) && !empty($data['country']) && $prev_forward->country != $data['country']) {
            do_action('korgou_forward_recalculate_forward_fee', $data['forwardid']);
        }

        kg_send_json_success();
    }

    function update_remark()
    {
        $data = Shoplic_Util::get_post_data('forwardid', 'remark');
        do_action('korgou_forward_update_forward', $data);
        kg_send_json_success();
    }

    function load_forward()
    {
        Shoplic_Util::display_errors();
        $rowcount = intval($_POST['length']);
        $start = intval($_POST['start']);
        $paged = ($start / $rowcount) + 1;
        $args = Shoplic_Util::get_post_data('processing', 'userid_prefix', 'userid', 'forwardid', 'status', 'forwardcourierid', 'forwardcouriertrackno', 'packagetime_start', 'packagetime_end', 'paytime_start', 'paytime_end', 'updatetime_start', 'updatetime_end', 'forwardtime_start', 'forwardtime_end');
        $orderby = null;
        $conditions = [];
        if (!empty($args['processing']))
            $conditions[] = ['processing', '=', $args['processing']];
        if (!empty($args['userid_prefix'])) {
            $conditions[] = ['userid', 'LIKE', $args['userid_prefix'] . '%'];
            $orderby = 'userid ASC';
        }
        if (!empty($args['userid']))
            $conditions[] = ['userid', '=', $args['userid']];
        if (!empty($args['forwardid']))
            $conditions[] = ['forwardid', 'LIKE', '%' . $args['forwardid']];
        if (!empty($args['status']))
            $conditions[] = ['status', '=', $args['status']];
        if (!empty($args['forwardcourierid']))
            $conditions[] = ['forwardcourierid', '=', $args['forwardcourierid']];
        if (!empty($args['forwardcouriertrackno']))
            $conditions[] = ['forwardcouriertrackno', 'LIKE', '%' . $args['forwardcouriertrackno'] . '%'];
        if (isset($args['packagetime_start']) && !empty($args['packagetime_start']))
            $conditions[] = ['packagetime', '>=', $args['packagetime_start'] . ' 00:00:00'];
        if (isset($args['packagetime_end']) && !empty($args['packagetime_end']))
            $conditions[] = ['packagetime', '<=', $args['packagetime_end'] . ' 23:59:59'];
        if (isset($args['updatetime_start']) && !empty($args['updatetime_start']))
            $conditions[] = ['updatetime', '>=', $args['updatetime_start'] . ' 00:00:00'];
        if (isset($args['updatetime_end']) && !empty($args['updatetime_end']))
            $conditions[] = ['updatetime', '<=', $args['updatetime_end'] . ' 23:59:59'];
        if (isset($args['paytime_start']) && !empty($args['paytime_start']))
            $conditions[] = ['paytime', '>=', $args['paytime_start'] . ' 00:00:00'];
        if (isset($args['paytime_end']) && !empty($args['paytime_end']))
            $conditions[] = ['paytime', '<=', $args['paytime_end'] . ' 23:59:59'];
        if (isset($args['forwardtime_start']) && !empty($args['forwardtime_start']))
            $conditions[] = ['forwardtime', '>=', $args['forwardtime_start'] . ' 00:00:00'];
        if (isset($args['forwardtime_end']) && !empty($args['forwardtime_end']))
            $conditions[] = ['forwardtime', '<=', $args['forwardtime_end'] . ' 23:59:59'];

            // print_r($conditions);
        $page = apply_filters('korgou_forward_get_forward_page', [], $args, $orderby, $conditions, $rowcount, $paged);

        $data = [];
        foreach ($page->items as $item) {
            $packagelist = '';
            // print_r(explode(',', $item->packagelist));
            foreach (explode(',', $item->packagelist) as $i => $pid) {
                // print_r($i . ':' . $pid . '<br>');
                if ($i > 0) {
                    $packagelist .= ',';
                    if ($i % 3 == 0)
                        $packagelist .= '<br>';
                }
                $packagelist .= $pid;
            }

            $data[] = [
                sprintf('<input type="checkbox" class="check" value="%1$s">', $item->forwardid),
                sprintf('<a class="forward-link %s" target="_blank" href="/forward/detail/?forwardid=%2$s">%2$s</a>', $item->print == 'Y' ? 'text-muted' : '', $item->forwardid),
                    korgou_user_role_id($item->userid),
                $item->get_processing_name(),
                $packagelist,
                $item->get_status_name(), //Korgou_Forward::$STATUSES_ZH[$item->status] ?? 'Unknown',
                $item->forwardcomment,
                $item->packageweight,
                $item->forwardfee,
                $item->valueaddedfee,
                $item->servicefee,
                ($item->servicefeediscount < 100) ? '<b class="text-info">' . $item->servicefeediscount . '%</b>' : $item->servicefeediscount . '%',
                // $item->adjustment,
                '<b class="text-danger">' . $item->totalfee . '</b>',
                $item->packagetime,
                $item->paytime,
                $item->updatetime,
                $item->forwardtime,
                $item->receipttime,
                $item->country,
                $item->englishname . '&nbsp;/&nbsp;' . $item->nativename,
                $item->province,
                $item->city,
                $item->addressdetail,
                $item->company,
                $item->zipcode,
                $item->phonenum . '&nbsp;/&nbsp;' . $item->mobilenum,
                str_replace('-', '<br>', $item->forwardcouriername),
                $item->forwardcouriertrackno,
                $item->repacking,
                $item->remark,
                $item->get_storagefee_total(),
                $item->insurancefee,
                $item->splittingfee,
                $item->repackingfee,
            ];
        }

        $result = [
            'draw' => isset ( $_POST['draw'] ) ? intval( $_POST['draw'] ) : 0,
            'recordsTotal' => intval($page->count),
            'recordsFiltered' => intval($page->count),
            'data' => $data,
        ];

        $this->send_json($result);
    }

    function weight_fee()
    {
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $_GET['forwardid']]);
        if ($forward != null) {
            if ($forward->totalfee == 0)
                $forward->totalfee = $forward->forwardfee + $forward->valueaddedfee + $forward->servicefee + $fowward->adjustment;

            if ($forward->processing == 'P') {
                $processing_unit_fee = 4000;
            } else {
                switch ($forward->forwardcourierid) {
                    case '1':
                    case '2':
                    case '3':
                    case '8':
                    case '9':
                        // $processing_unit_fee = 2000;
                    // break;
                    case '6':
                    case '7':
                    case '10':
                    default:
                        $processing_unit_fee = 3000;
                    break;
                }
            }

            if ($forward->servicefeediscount == 0) {
                $user_discount = apply_filters('korgou_user_get_discount', null, $forward->userid);
                $forward->servicefeediscount = ($user_discount != null) ? $user_discount->forwarddiscount : KG::get_user_discount_by_role($forward->userid);
            }

            if (empty($forward->shipping_country)) {
                $courier_country = apply_filters('korgou_forward_get_courier_country', null, ['forwardcourierid' => $forward->forwardcourierid, 'user_country' => $forward->country]);
                if ($courier_country != null) {
                    $forward->shipping_country = $courier_country->country;
                }
            }

            $this->view('weight-fee', [
                'forward' => $forward,
                'processing_unit_fee' => $processing_unit_fee,
                'items' => apply_filters('korgou_forward_get_item_list', [], ['forwardid' => $forward->forwardid]),
                'countries' => apply_filters('korgou_forward_get_fare_countries', [], $forward->forwardcourierid),
                'fares' => apply_filters('korgou_forward_get_fare_list_group_country', [], $forward->forwardcourierid),
                'courier_courtry' => $courier_country,
            ]);
        }
    }

    function update_weight_fee()
    {
        $forwardid = $_POST['forwardid'];
        $adjustment = intval(trim($_POST['adjustment']));
        $packageweight = intval(trim($_POST['packageweight']));
        if ($packageweight <= 0) {
            wp_send_json_error('Weight errors');
        }
        $forwardfee = intval(trim($_POST['forwardfee']));
        if ($forwardfee <= 0) {
            wp_send_json_error('Product delivery is greater than 0.');
        }
        $valueaddedfee = intval(trim($_POST['valueaddedfee']));
        if ($valueaddedfee < 0) {
            wp_send_json_error('增值费需大于等于0');
        }
        $servicefee = intval(trim($_POST['servicefee']));
        if ($servicefee <= 0) {
            wp_send_json_error('服务费需大于0');
        }
        $totalfee = intval(trim($_POST['totalfee']));
        /*
        if ($totalfee != $priorityfee + $forwardfee + $valueaddedfee + $adjustment + $servicefee) {
            wp_send_json_error('The total cost of the error rate');
        }
        */
        if ($totalfee <= 0) {
            wp_send_json_error('Total fee shoud be greater than 0.');
        }

        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null) {
            wp_send_json_error('Forward is not available.');
        }

        $data = [
            'forwardid' => $forwardid,
        ];
        $item_forwardfees = $_POST['item_forwardfee'];
        foreach ($item_forwardfees as $i => $fee) {
            $data['id'] = $_POST['item_id'][$i];
            $data['boxsize'] = $_POST['item_boxsize'][$i];
            $data['length'] = $_POST['item_length'][$i];
            $data['width'] = $_POST['item_width'][$i];
            $data['height'] = $_POST['item_height'][$i];
            $data['weight'] = $_POST['item_weight'][$i];
            $data['forwardfee'] = $_POST['item_forwardfee'][$i];

            if (empty($data['id'])) {
                if (!empty($data['forwardfee']) && $data['forwardfee'] != '0')
                    do_action('korgou_forward_insert_item', $data);
            } else {
                if (empty($data['forwardfee']) || $data['forwardfee'] == '0')
                    do_action('korgou_forward_delete_item', ['id' => $data['id'], 'forwardid' => $data['forwardid']]);
                else
                    do_action('korgou_forward_update_item', $data);
            }
            
        }

        $data = [
            'packageweight' => $packageweight,
            'forwardfee' => $forwardfee,
            'servicefeediscount' => $_POST['servicefeediscount'],
            'servicefee' => $servicefee,
            'insurancefee' => $_POST['insurancefee'],
            'splittingfee' => $_POST['splittingfee'],
            'repackingfee' => $_POST['repackingfee'],
            'storagefee' => $_POST['storagefee'],
            'storagefee2' => $_POST['storagefee2'],
            'valueaddedfee' => $valueaddedfee,
            'totalfee' => $totalfee,
            'shipping_country' => $_POST['shipping_country'],
            'billingtime' => current_time('mysql'),
        ];
        $condition = [
            'forwardid' => $forwardid,
            // 'status' => Korgou_Forward::STATUS_ACCEPTED,
        ];

        $balance = apply_filters('korgou_user_balance_get_balance', null, ['userid' => $forward->userid]);

        if ($balance != null && $balance->balance >= $totalfee) {
            $data['status'] = Korgou_Forward::STATUS_PAID;
            $data['paytime'] = current_time('mysql');
            do_action('korgou_forward_update_forward', $data, $condition, $forwardid);

            do_action('korgou_user_balance_change_balance',
                $forwardid, $forward->userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $totalfee, 'KRW', 'Paid', '', '', 'sys', 'sys', '');
        } else {
            $data['status'] = Korgou_Forward::STATUS_WAIT_PAY;
            do_action('korgou_forward_update_forward', $data, $condition, $forwardid);
        }

        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        $email = KG::get_user_email($forward->userid);
        if ($email != null) {
            $body = $this->contents('email/forward-order', [
                'forward' => $forward,
            ]);
            SP_WC::send_email($email, 'Forward order notification', 'Forward order', $body);
        }

        kg_send_json_success();
    }

    function calculate_forward_fee()
    {
        $forwardid = $_POST['forwardid'];
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null) {
            wp_send_json_error('Forward is not available.');
        }

        $packageweight = intval($_POST['packageweight']);

        $forward_fare = apply_filters('korgou_forward_get_minimum_fare', null, $forward->country, $forward->forwardcourierid, $packageweight);
        if ($forward_fare == null) {
            $forward_fare = apply_filters('korgou_forward_get_minimum_fare', null, 'other', $forward->forwardcourierid, $packageweight);
        }

        wp_send_json_success($forward_fare != null ? $forward_fare : ['price' => 0, 'servicefee' => 0]);
    }

    function deduct_money()
    {
        $forwardid = $_POST['forwardid'];
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null)
            wp_send_json_error('Forward is not available.');

        if ($forward->status != Korgou_Forward::STATUS_WAIT_PAY)
            wp_send_json_error('Forward status is not WAIING PAYMENT.');

        if ($forward->totalfee == null || $forward->totalfee <= 0)
            wp_send_json_error('扣款金额有误');

        $balance = apply_filters('korgou_user_balance_get_balance', null, ['userid' => $forward->userid]);

        if ($balance != null && $balance->balance >= $forward->totalfee) {
            $data = [
                'status' => Korgou_Forward::STATUS_PAID,
                'paytime' => current_time('mysql'),
            ];
            $condition = [
                'forwardid' => $forwardid,
                // 'status' => Korgou_Forward::STATUS_ACCEPTED,
                'status' => Korgou_Forward::STATUS_WAIT_PAY,
            ];
            do_action('korgou_forward_update_forward', $data, $condition, $forwardid);

            do_action('korgou_user_balance_change_balance',
                $forwardid, $forward->userid, Korgou_User_Balance_Record::BALANCETYPE_BALANCE, - $forward->totalfee, 'KRW', 'Paid', '', '', 'sys', 'sys', '');

            kg_send_json_success();
        } else {
            wp_send_json_error('Insufficient balance');
        }
    }

    function start()
    {
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $_GET['forwardid']]);
        if ($forward != null) {
            $this->view('start', ['forward' => $forward]);
        }
    }

    function start_forward()
    {
        $forwardid = $_POST['forwardid'];
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $forwardid]);
        if ($forward == null)
            wp_send_json_error('Forward is not available.');

        if ($forward->status == Korgou_Forward::STATUS_ACCEPTED || $forward->status == Korgou_Forward::STATUS_WAIT_PAY)
            wp_send_json_error("You haven't given the money yet, don't transfer it.");

        if ($forward->status != Korgou_Forward::STATUS_PAID)
            wp_send_json_error('Forward not paid.');

        $data = [
            'forwardid' => $forward->forwardid,
            'forwardcouriertrackno' => $_POST['forwardcouriertrackno'],
            'status' => Korgou_Forward::STATUS_FORWARDED,
            'forwardtime' => current_time('mysql'),
        ];

        do_action('korgou_forward_update_forward', $data);

        foreach (explode(',', $forward->packagelist) as $packageid) {
            $data = [
                'status' => Korgou_Package::STATUS_FORWARDED,
                'forwardtime' => current_time('mysql'),
            ];
            $condition = [
                'packageid' => $packageid,
                'status' => Korgou_Package::STATUS_APPLY_FORWARD,
            ];
            do_action('korgou_package_update_package', $data, $condition);
        }
        
        kg_send_json_success();
    }

    function status()
    {
        $forward = apply_filters('korgou_forward_get_forward', null, ['forwardid' => $_GET['forwardid']]);
        if ($forward != null) {
            $this->view('status', ['forward' => $forward]);
        }
    }

    function change_status()
    {
        $forwardid = $_POST['forwardid'];
        $status = $_POST['status'];

        do_action('korgou_forward_update_forward_status', $forwardid, $status);
        
        kg_send_json_success();
    }

    function delete_courier()
    {
        $data = Shoplic_Util::get_post_data('id');
        if (!empty($data)) {
            do_action('korgou_forward_delete_courier', $data);
        }

        kg_send_json_success();
    }

    function add_courier()
    {
        $data = Shoplic_Util::get_post_data('id', 'zhname', 'enname', 'koname', 'status');
        if (!empty($data)) {
            do_action('korgou_forward_insert_courier', $data);
        }

        kg_send_json_success();
    }
};

