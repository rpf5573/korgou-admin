        <div class="card-box">
            <h5 class="card-title">海关申报</h5>

            <table class="table-bordered table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>物品内容</th>
                    <th>数量</th>
                    <th>价值</th>
                    <th>净重(g)</th>
                    <th>HS Tariff Number</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($customs as $custom): ?>
                <tr>
                    <td><?php echo $custom->contents; ?></td>
                    <td><?php echo $custom->quantity; ?></td>
                    <td><?php echo $custom->value; ?></td>
                    <td><?php echo $custom->netweight; ?></td>
                    <td><?php echo $custom->hstariffnumber; ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div> <!-- end card-box -->

