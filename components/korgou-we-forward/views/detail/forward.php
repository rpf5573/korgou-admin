<div class="card-box">
    <h5 class="card-title">Transfer order information</h5>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Forward No.</th>
            <th>User ID</th>
            <th>Status</th>
            <th>Priority processing</th>
            <th>Premium re-packing</th>
            <th>Insurance</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td id="data-forwardid"><?php echo $forward->forwardid; ?></td>
            <td id="data-userid"><?php echo korgou_user_role_id($forward->userid); ?></td>
            <td><?php echo $forward->get_status_name(); ?></td>
            <td><?php echo $forward->processing == 'P' ? 'Priority' : 'Normal'; ?></td>
            <td><?php echo $forward->repacking; ?></td>
            <td><?php echo $forward->insurance; ?></td>
        </tr>
    </tbody>
    </table>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Application time</th>
            <th>Billing time</th>
            <th>Payment time</th>
            <th>Forwarded time</th>
            <th>Update time</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $forward->packagetime; ?></td>
            <td><?php echo $forward->billingtime; ?></td>
            <td><?php echo $forward->paytime; ?></td>
            <td><?php echo $forward->forwardtime; ?></td>
            <td><?php echo $forward->updatetime; ?></td>
        </tr>
    </tbody>
    </table>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Other special requirements</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $forward->forwardcomment; ?></td>
        </tr>
    </tbody>
    </table>

    <h5 class="card-title">Fee <small>(KRW)</small></h5>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Weight(g)</th>
            <th>Storage fee</th>
            <th>Shipping fee</th>
            <th>Processing discounts</th>
            <th>Processing fee</th>
            <th>Insurance</th>
            <th>Splitting fee</th>
            <th>Premium re-packing fee</th>
            <th>Value-added services fee</th>
            <th>Total fee</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo number_format($forward->packageweight); ?></td>
            <td><?php echo number_format($forward->get_storagefee_total()); ?></td>
            <td><?php echo number_format($forward->forwardfee); ?></td>
            <td><?php if ($forward->servicefeediscount < 100): ?>
                    <b class="text-danger"><?php echo $forward->servicefeediscount; ?>%</b>
                <?php else: ?>
                    <?php echo $forward->servicefeediscount; ?>%
                <?php endif; ?>
            </td>
            <td><?php echo number_format($forward->servicefee); ?></td>
            <td><?php echo number_format($forward->insurancefee); ?></td>
            <td><?php echo number_format($forward->splittingfee); ?></td>
            <td><?php echo number_format($forward->repackingfee); ?></td>
            <td><?php echo number_format($forward->valueaddedfee); ?></td>
            <td id="data-totalfee"><?php echo number_format($forward->totalfee); ?></td>
        </tr>
    </tbody>
    </table>

    <?php if ($forward->status == Korgou_Forward::STATUS_CANCEL_REQUESTED ||
            $forward->status == Korgou_Forward::STATUS_CANNOT_BE_CANCELED ||
            $forward->status == Korgou_Forward::STATUS_CANCEL): ?>
        <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>Cancel reason</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $forward->cancel_reason; ?></td>
        </tbody>
        </table>
    <?php endif; ?>

    <div class="text-center">
        <?php if ($forward->status == Korgou_Forward::STATUS_CANCEL_REQUESTED): ?>
            <button class="btn btn-danger mr-2 ajax-btn" data-forwardid="<?php echo $forward->forwardid; ?>"
                <?php $this->nonce_action_attr('cancel'); ?>>Cancel</button>
            <button class="btn btn-warning mr-2 ajax-btn" data-forwardid="<?php echo $forward->forwardid; ?>"
                <?php $this->nonce_action_attr('refuse_cancel'); ?>>Cannot be canceled</button>
        <?php else: ?>
            <input type="hidden" id="forwardid" name="forwardid" value="<?php echo $forward->forwardid; ?>">
            <a href="/forward/weight/?forwardid=<?php echo $forward->forwardid; ?>" class="link btn btn-primary mr-2">Metering and billing</a>
            <button class="btn btn-pink mr-2 deduct-money-btn" <?php $this->nonce_action_attr('deduct_money'); ?>>Deduction</button>
            <a href="/forward/start/?forwardid=<?php echo $forward->forwardid; ?>" class="link btn btn-success mr-2">Transhipment started</a>
            <a href="/forward/status/?forwardid=<?php echo $forward->forwardid; ?>" class="link btn btn-blue <?php if ($forward->status == Korgou_Forward::STATUS_FORWARDED) echo ' disabled'; ?>">Change status</a>
        <?php endif; ?>
    </div>
</div>

<?php $this->ajax_form('deduct_money'); ?>
    <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">
</form>

<script type="text/javascript">
jQuery(function($) {
    $('.deduct-money-btn').click(function() {
        var $section = $(this).closest('section');
        if (confirm('Are you sure to deduct ' + $('#data-totalfee').text() + ' from ' + $('#data-userid').text() + '?')) {
            $.post('/wp-admin/admin-ajax.php', {
                action: $(this).data('action'),
                _wpnonce: $(this).data('nonce'),
                forwardid: $('#data-forwardid').text()
            }, function(response) {
                alert(response.data);
                if (response.success) {
                    location.reload();
                }
            });
        }
        return false;
    });

    $('.ajax-btn').click(function() {
        $.post('/wp-admin/admin-ajax.php', $(this).data(), function(response) {
            alert(response.data);
            if (response.success) {
                location.reload();
            }
        });
        return false;
    });

});
</script>
