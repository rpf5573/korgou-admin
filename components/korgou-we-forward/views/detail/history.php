<div class="card-box">
    <h5 class="card-title">History</h5>

    <table class="table-bordered table table-hover">
        <thead class="thead-light">
        <tr>
            <th>No.</th>
            <th>Date</th>
            <th>User</th>
            <th>Status</th>
            <th>Weight(g)</th>
            <th>Total fee(KRW)</th>
            <th>Application time</th>
            <th>Payment time</th>
            <th>Update time</th>
            <th>Forwarded time</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($history as $item): $forward = new Korgou_Forward(); $forward->from_json($item->data); ?>
            <tr>
                <td><a class="link history-link" href="/forward/forwardmanage/forward-history/?id=<?php echo $item->id; ?>"><?php echo $item->id; ?></a></td>
                <td><?php echo $item->create_date; ?></td>
                <td><?php echo $item->userid; ?></td>
                <td><?php echo $forward->get_status_name(); ?></td>
                <td><?php echo $forward->packageweight; ?></td>
                <td><?php echo $forward->totalfee; ?></td>
                <td><?php echo $forward->packagetime; ?></td>
                <td><?php echo $forward->paytime; ?></td>
                <td><?php echo $forward->updatetime; ?></td>
                <td><?php echo $forward->forwardtime; ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div> <!-- end card-box -->

