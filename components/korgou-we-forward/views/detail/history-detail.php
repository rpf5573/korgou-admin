<p>
    Forward history ID: <?php echo $history->id; ?>
</p>

<div class="row mb-4">
    <div class="col-12">
        <?php include 'packages.php'; ?>
        <?php include 'forward.php'; ?>
        <?php include 'address.php'; ?>
        <?php include 'customs.php'; ?>
        <?php include 'valueadded.php'; ?>
        <?php include 'history-remark.php'; ?>

        <p class="text-center">
            <button type="button" class="btn btn-secondary cancel-btn">Back</button>
        </p>

    </div>
</div>
