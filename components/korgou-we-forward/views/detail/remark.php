<div class="card-box">
    <h5 class="card-title">Remark</h5>

    <?php $this->ajax_form('update_remark'); ?>
        <p>
            <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">
            <textarea class="form-control" name="remark" rows="5"><?php echo $forward->remark; ?></textarea>
        </p>
        <p class="text-center">
            <button type="button" class="btn btn-dark update-remark-btn">Update</button>
        </p>
    </form>

</div>

<script type="text/javascript">
jQuery(function($) {
    $('.update-remark-btn').click(function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            alert(response.data);
        });
        return false;
    });
});
</script>