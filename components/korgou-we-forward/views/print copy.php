<?php
// print_r($forwards);
?>
<style>
body {
    background-color: white;
}
</style>

<div class="container-fluid">
    <?php foreach ($forwards as $forward): ?>
        <dl class="row">
            <dt class="col-sm-3">
                Application Time
            </dt>
            <dd class="col-sm-9">
                <?php echo $forward->packagetime; ?>
            </dd>
            <dt class="col-sm-3">
                User ID
            </dt>
            <dd class="col-sm-9">
                <?php echo $forward->userid; ?>
            </dd>
            <dt class="col-sm-3">
                Forward Order No.
            </dt>
            <dd class="col-sm-9">
                <?php echo $forward->forwardid; ?>
            </dd>
            <dt class="col-sm-3">
                Logistics
            </dt>
            <dd class="col-sm-9">
                <?php echo $forward->forwardcouriername; ?> / 
                <?php echo $forward->forwardcouriertrackno; ?>
            </dd>
            <dt class="col-sm-3">
                Value Added Service
            </dt>
            <dd class="col-sm-9">
                <?php foreach ($forward->vas as $va): ?>
                    <?php echo "{$va->zhname} {$va->enname} {$va->koname}"; ?><br>
                <?php endforeach; ?>
            </dd>
            <dt class="col-sm-3">
                Package Forward Instruction
            </dt>
            <dd class="col-sm-9">
                <?php echo $forward->forwardcomment; ?>
            </dd>
        </dl>

        <table class="table table-sm table-bordered mb-4">
        <thead>
            <tr>
                <th>No.</th>
                <th>Tracking No.</th>
                <th>E/B</th>
                <th>Weight</th>
                <th>Remark</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = 0; foreach ($forward->packages as $package): $i++ ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $package->domestictrackno; ?></td>
                    <td><?php echo Korgou_Package::$TYPES[$package->type] ?? ''; ?></td>
                    <td><?php echo $package->weight; ?></td>
                    <td><?php echo $package->remark; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
        </table>

    <?php endforeach; ?>
</div>

<script type="text/javascript">
jQuery(function($) {
    // window.print();
    // setTimeout(window.close, 0);
});
</script>
