<div class="card-box">
    <h5 class="card-title">选择的包裹如下</h5>

    <table class="table-bordered table table-hover">
        <thead class="thead-light">
        <tr>
            <th>包裹编号</th>
            <th>到达时间</th>
            <th>快递公司</th>
            <th>运单号</th>
            <th>包裹内容</th>
            <th>Weight</th>
            <th>Type</th>
            <th>Freight Collect Fee</th>
            <th>备注</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($packages as $package): ?>
        <tr>
            <td><?php echo $package->packageid; ?></td>
            <td><?php echo $package->arrivaltime; ?></td>
            <td><?php echo $package->domesticcourier; ?></td>
            <td><?php echo $package->domestictrackno; ?></td>
            <td><?php echo $package->packagecontent; ?></td>
            <td><?php echo $package->weight; ?></td>
            <td><?php echo Korgou_Package::$TYPES[$package->type] ?? ''; ?></td>
            <td><?php echo $package->freightfee; ?></td>
            <td><?php echo $package->remark; ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div> <!-- end card-box -->

