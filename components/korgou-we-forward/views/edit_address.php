<div class="card-box">
    <h5 class="card-title">称重计费</h5>

    <?php $this->ajax_form('update_weight_fee'); ?>

        <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">

        <table class="table table-bordered w-50">
        <thead class="thead-light">
            <th></th>
            <th>Weight</th>
            <th>Forward Fee</th>
        </thead>
        <tbody>
            <?php for($i = 0; $i < sizeof($items); $i++): ?>
                <tr>
                    <td><?php echo $i + 1; ?>
                        <input type="hidden" class="form-control" name="item_id[]" value="<?php echo $items[$i]->weight; ?>">
                    </td>
                    <td>
                        <input type="text" class="form-control input-weight" name="item_weight[]" value="<?php echo $items[$i]->weight; ?>">
                    </td>
                    <td>
                        <input type="text" class="form-control input-forwardfee" name="item_forwardfee[]" value="<?php echo $items[$i]->forwardfee; ?>">
                    </td>
                </tr>
            <?php endfor; ?>
            <?php for(; $i < 10; $i++): ?>
                <tr>
                    <td><?php echo $i + 1; ?>
                        <input type="hidden" class="form-control" name="item_id[]" value="">
                    </td>
                    <td>
                        <input type="text" class="form-control input-weight" name="item_weight[]" value="">
                    </td>
                    <td>
                        <input type="text" class="form-control input-forwardfee" name="item_forwardfee[]" value="">
                    </td>
                </tr>
            <?php endfor; ?>
            <tr>
                <td>Total</td>
                <td>
                    <input type="text" readonly class="form-control-plaintext" id="total-weight" value="">
                </td>
                <td>
                    <input type="text" readonly class="form-control-plaintext" id="total-forwardfee" value="">
                </td>
            </tr>
        </tbody>
        </table>

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>转运渠道</th>
                <td>
                    <input type="text" readonly class="form-control-plaintext" value="<?php echo $forward->forwardcouriername; ?>">
                </td>
            </tr>
            <tr>
                <th>重量(G/克)</th>
                <td>
                    <input type="text" class="form-control calculate-forward-fee" id="packageweight" name="packageweight" value="<?php echo $forward->packageweight; ?>" required>
                </td>
            </tr>
            <tr>
                <th>Priority Order Fee(KRW) (= Weight X 4000)</th>
                <td>
                    <?php if ($forward->processing == 'P'): ?>
                        <input type="text" readonly class="form-control-plaintext calculate-total-fee" id="priorityfee" name="priorityfee" value="<?php echo $forward->priorityfee; ?>">
                    <?php else: ?>
                        <input type="text" readonly class="form-control-plaintext" value="N/A">
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <th>转运费(KRW)<br>
                Freight forwarding
                </th>
                <td>
                    <input type="text" class="form-control calculate-total-fee" id="forwardfee" name="forwardfee" value="<?php echo $forward->forwardfee; ?>">
                </td>
            </tr>
            <tr>
                <th>增值服务费(KRW)<br>
                Value-added service charges
                </th>
                <td>
                    <input type="text" class="form-control calculate-total-fee" id="valueaddedfee" name="valueaddedfee" value="<?php echo $forward->valueaddedfee; ?>">
                </td>
            </tr>
            <tr>
                <th>转运服务费(KRW)
                Service Fee
                </th>
                <td>
                    <input type="text" class="form-control calculate-total-fee" id="servicefee" name="servicefee" value="<?php echo $forward->servicefee; ?>">
                </td>
            </tr>
            <tr>
                <th>折扣<br>
                Discount
                </th>
                <td>
                    <div class="input-group">
                        <input type="text" class="form-control" id="servicefeediscount" name="servicefeediscount" value="<?php echo $forward->servicefeediscount; ?>">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">%</span>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th>人工调价(KRW)</th>
                <td>
                    <input type="text" class="form-control calculate-total-fee" id="adjustment" name="adjustment" value="<?php echo $forward->adjustment; ?>">
                </td>
            </tr>
            <tr>
                <th>总收费(KRW)<br>
                Total charges
                </th>
                <td>
                    <input type="text" class="form-control-plaintext calculate-total-fee" id="totalfee" name="totalfee" value="<?php echo $forward->totalfee; ?>" readonly>
                </td>
            </tr>
        </tbody>
        </table>

        <p>
            <button type="button" class="btn btn-secondary cancel-btn">关闭</button>
            <button type="button" class="btn btn-primary weight-fee-btn">确定</button>
        </p>

    </form>
</div>

<?php $this->ajax_form('calculate_forward_fee'); ?>
    <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">
    <input type="hidden" name="packageweight" id="input-packageweight">
</form>

<script type="text/javascript">
jQuery(function($) {
    function calculateTotal() {
        var totalWeight = 0, totalFee = 0;
        $('.input-weight').each(function() {
            const val = parseInt($(this).val());
            if (!isNaN(val))
                totalWeight += val;
        });
        $('#total-weight').val(totalWeight);
        $('#packageweight').val(totalWeight);

        $('.input-forwardfee').each(function() {
            const val = parseInt($(this).val());
            if (!isNaN(val))
                totalFee += val;
        });
        $('#total-forwardfee').val(totalFee);
        $('#priorityfee').each(function() {
            $(this).val(totalWeight/1000*4000);
        });
    }

    $('.input-weight').change(calculateTotal);
    $('.input-forwardfee').change(calculateTotal);

    calculateTotal();
});
</script>