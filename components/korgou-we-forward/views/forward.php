<div class="card-box">
    <h5 class="card-title">转运订单信息</h5>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>转运单号</th>
            <th>用户编号</th>
            <th>Processing</th>
            <th>状态</th>
            <th>转运说明</th>
            <th>申请时间</th>
            <th>付款时间</th>
            <th>收货时间</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td id="data-forwardid"><?php echo $forward->forwardid; ?></td>
            <td id="data-userid"><?php echo $forward->userid; ?></td>
            <td><?php echo $forward->get_processing_name(); ?></td>
            <td><?php echo $forward->get_status_name(); ?></td>
            <td><?php echo $forward->forwardcomment; ?></td>
            <td><?php echo $forward->packagetime; ?></td>
            <td><?php echo $forward->paytime; ?></td>
            <td><?php echo $forward->receipttime; ?></td>
        </tr>
    </tbody>
    </table>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>重量(G/克)</th>
            <th>Priority Order Fee(KRW)</th>
            <th>转运费(KRW)</th>
            <th>增值服务费(KRW)</th>
            <th>服务费(KRW)</th>
            <th>服务费折扣</th>
            <th>人工调价(KRW)</th>
            <th>总收费(KRW)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $forward->packageweight; ?></td>
            <td><?php echo $forward->priorityfee; ?></td>
            <td><?php echo $forward->forwardfee; ?></td>
            <td><?php echo $forward->valueaddedfee; ?></td>
            <td><?php echo $forward->servicefee; ?></td>
            <td><?php if ($forward->servicefeediscount < 100): ?>
                    <b class="text-danger"><?php echo $forward->servicefeediscount; ?>%</b>
                <?php else: ?>
                    <?php echo $forward->servicefeediscount; ?>%
                <?php endif; ?>
            </td>
            <td><?php echo $forward->adjustment; ?></td>
            <td id="data-totalfee"><?php echo $forward->totalfee; ?></td>
        </tr>
    </tbody>
    </table>

    <?php if ($forward->status == Korgou_Forward::STATUS_CANCEL_REQUESTED ||
            $forward->status == Korgou_Forward::STATUS_CANNOT_BE_CANCELED ||
            $forward->status == Korgou_Forward::STATUS_CANCEL): ?>
        <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>Cancel reason</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $forward->cancel_reason; ?></td>
        </tbody>
        </table>
    <?php endif; ?>

    <div class="text-center">
        <?php if ($forward->status == Korgou_Forward::STATUS_CANCEL_REQUESTED): ?>
            <button class="btn btn-danger mr-2 ajax-btn" data-forwardid="<?php echo $forward->forwardid; ?>"
                <?php $this->nonce_action_attr('cancel'); ?>>Cancel</button>
            <button class="btn btn-warning mr-2 ajax-btn" data-forwardid="<?php echo $forward->forwardid; ?>"
                <?php $this->nonce_action_attr('refuse_cancel'); ?>>Cannot be canceled</button>
        <?php else: ?>
            <input type="hidden" id="forwardid" name="forwardid" value="<?php echo $forward->forwardid; ?>">
            <a href="/forward/weight/?forwardid=<?php echo $forward->forwardid; ?>" class="link btn btn-primary mr-2">称重计费</a>
            <button class="btn btn-info mr-2 deduct-money-btn" <?php $this->nonce_action_attr('deduct_money'); ?>>扣款</button>
            <a href="/forward/start/?forwardid=<?php echo $forward->forwardid; ?>" class="link btn btn-success mr-2">开始转运</a>
            <a href="/forward/status/?forwardid=<?php echo $forward->forwardid; ?>" class="link btn btn-blue">改状态</a>
        <?php endif; ?>
    </div>
</div>

<?php $this->ajax_form('deduct_money'); ?>
    <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">
</form>