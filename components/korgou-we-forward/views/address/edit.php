<div class="row">
    <div class="col-12">
        <div class="card-box">
            <h5 class="card-title">Forward Address</h5>
            <?php $this->ajax_form('update_address'); ?>
                <table class="table-bordered table table-hover w-50">
                <tbody>
                    <tr>
                        <th>用户编号</th>
                        <td>
                            <input type="hidden" name="id" value="<?php echo $forward->forwardid; ?>">
                            <input type="text" readonly class="form-control-plaintext" name="userid" value="<?php echo $forward->userid; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>国家</th>
                        <td>
                            <?php do_action('korgou_select_country', $forward->country); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>英文名</th>
                        <td><input type="text" name="englishname" class="form-control" value="<?php echo $forward->englishname; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>本地名</th>
                        <td><input type="text" class="form-control" name="nativename" value="<?php echo $forward->nativename; ?>" /></td>
                    </tr>
                    <tr>
                        <th>邮编</th>
                        <td><input type="text" class="form-control" name="zipcode" value="<?php echo $forward->zipcode; ?>" /></td>
                    </tr>
                    <tr>
                        <th>省</th>
                        <td><input type="text" class="form-control" name="province" value="<?php echo $forward->province; ?>" /></td>
                    </tr>
                    <tr>
                        <th>市</th>
                        <td><input type="text" class="form-control" name="city" value="<?php echo $forward->city; ?>" /></td>
                    </tr>
                    <tr>
                        <th>详细地址</th>
                        <td><input type="text" class="form-control" name="addressdetail" value="<?php echo $forward->addressdetail; ?>" /></td>
                    </tr>
                    <tr>
                        <th>公司</th>
                        <td><input type="text" class="form-control" name="company" value="<?php echo $forward->company; ?>" /></td>
                    </tr>
                    <tr>
                        <th>电话</th>
                        <td><input type="text" class="form-control" name="phonenum" value="<?php echo $forward->phonenum; ?>" /></td>
                    </tr>
                    <tr>
                        <th>手机</th>
                        <td><input type="text" class="form-control" name="mobilenum" value="<?php echo $forward->mobilenum; ?>" /></td>
                    </tr>
                </tbody>
                </table>
                <p>
                    <button type="button" class="btn btn-secondary cancel-btn">Cancel</button>
                    <button type="button" class="btn btn-primary submit-btn">保存修改</button>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>
