<div class="row mb-4">
    <div class="col-12">
        <?php include 'detail/packages.php'; ?>
        <?php include 'detail/forward.php'; ?>
        <?php include 'detail/address.php'; ?>
        <?php include 'detail/customs.php'; ?>
        <?php include 'detail/valueadded.php'; ?>
        <?php include 'detail/remark.php'; ?>
        <?php include 'detail/history.php'; ?>

        <p class="text-center">
            <button type="button" class="btn btn-secondary" onclick="self.close();">Close</button>
        </p>

    </div>
</div>
