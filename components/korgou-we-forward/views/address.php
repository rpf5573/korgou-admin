<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_address'); ?>
                <table class="table-bordered table table-hover w-50">
                <tbody>
                    <tr>
                        <th>Forward No.</th>
                        <td>
                            <input type="text" readonly class="form-control-plaintext" name="forwardid" value="<?php echo $forward->forwardid; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Country</th>
                        <td>
                            <?php do_action('korgou_select_country', $forward->country); ?>
                        </td>
                    </tr>
                    <tr>
                        <th>English name</th>
                        <td><input type="text" name="englishname" class="form-control" value="<?php echo $forward->englishname; ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th>Local name</th>
                        <td><input type="text" class="form-control" name="nativename" value="<?php echo $forward->nativename; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Zip code</th>
                        <td><input type="text" class="form-control" name="zipcode" value="<?php echo $forward->zipcode; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Province</th>
                        <td><input type="text" class="form-control" name="province" value="<?php echo $forward->province; ?>" /></td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td><input type="text" class="form-control" name="city" value="<?php echo $forward->city; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Detailed Address</th>
                        <td><input type="text" class="form-control" name="addressdetail" value="<?php echo $forward->addressdetail; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Company</th>
                        <td><input type="text" class="form-control" name="company" value="<?php echo $forward->company; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Telephone</th>
                        <td><input type="text" class="form-control" name="phonenum" value="<?php echo $forward->phonenum; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Cellphone</th>
                        <td><input type="text" class="form-control" name="mobilenum" value="<?php echo $forward->mobilenum; ?>" /></td>
                    </tr>
                    <tr>
                        <th>Courier</th>
                        <td>
                            <select name="forwardcourierid" class="form-control">
                                <option value="">--- <?php _e('Choose', 'korgou'); ?> ---</option>
                                <?php foreach ($couriers as $courier): $c_name = $courier->zhname . '-' . $courier->enname; ?>
                                    <option value="<?php echo $courier->id; ?>" <?php if ($forward->forwardcourierid == $courier->id) echo 'selected'; ?>
                                    ><?php echo $c_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Track No.</th>
                        <td><input type="text" class="form-control" name="forwardcouriertrackno" value="<?php echo $forward->forwardcouriertrackno; ?>" /></td>
                    </tr>
                </tbody>
                </table>
                <p>
                    <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                    <button type="button" class="btn btn-primary update-address-btn">Update</button>
                </p>
            </form>
        </div> <!-- end card-box -->
    <div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.update-address-btn').click(function() {
        var $section = $(this).closest('section');
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                location.href = '/forward/detail/?forwardid=<?php echo $forward->forwardid; ?>';
            }
        });
        return false;
    });
});
</script>