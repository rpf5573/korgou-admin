<div class="card-box">

    <?php $this->ajax_form('start_forward'); ?>

        <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>Tracking Number</th>
                <td>
                    <input type="text" class="form-control" id="forwardcouriertrackno" name="forwardcouriertrackno" value="<?php echo $forward->forwardcouriertrackno; ?>" required>
                </td>
            </tr>
        </tbody>
        </table>

        <p>
            <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
            <button type="button" class="btn btn-primary start-btn">Save</button>
        </p>

    </form>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('#forwardcouriertrackno').focus();

    $('.start-btn').click(function() {
		if ($("#forwardcouriertrackno").val() == '') {
			alert("Please enter the forwarding tracking number.");
			return false;
		}
        var $section = $(this).closest('section');
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                location.href = '/forward/detail/?forwardid=<?php echo $forward->forwardid; ?>';
            }
        });
        return false;
    });

});
</script>