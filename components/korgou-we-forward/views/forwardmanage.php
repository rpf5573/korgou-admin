<section id="section-1">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php
                do_action('korgou_we_search_form', [[
                        'name' => 'processing', 'type' => 'select', 'label' => 'Processing', 'options' => Korgou_Forward::$PROCESSINGS,
                    ], [
                        'name' => 'userid', 'type' => 'text', 'label' => 'User ID'
                    ], [
                        'name' => 'forwardid', 'type' => 'text', 'label' => 'Forward No.',
                    ], [
                        'name' => 'status', 'type' => 'select', 'label' => 'Status', 'options' => Korgou_Forward::$STATUSES, // 'value' => Korgou_Forward::STATUS_ACCEPTED,
                    ], [
                        'name' => 'forwardcourierid', 'type' => 'select', 'label' => 'Courier', 'options' => $couriers,
                    ], [
                        'name' => 'forwardcouriertrackno', 'type' => 'text', 'label' => 'Tracking number',
                    ], [
                        'name' => ['packagetime_start', 'packagetime_end'], 'type' => 'period', 'label' => 'Application time'
                    ], [
                        'name' => ['updatetime_start', 'updatetime_end'], 'type' => 'period', 'label' => 'Update time'
                    ], [
                        'name' => ['paytime_start', 'paytime_end'], 'type' => 'period', 'label' => 'Payment time'
                    ], [
                        'name' => ['forwardtime_start', 'forwardtime_end'], 'type' => 'period', 'label' => 'Forwarded time'
                    ], 
                ]);
                ?>

                <?php
                do_action('korgou_we_datatable', [
                    '<input type="checkbox" class="check-all">', 'Forward No.', 'User ID', 'Processing', 'Package list',
                    'Status', 'Other special requirements', 'Weight(g)', 'Shipping fee(KRW)', 'VA services fee(KRW)',
                    'Processing fee(KRW)', 'Processing fee discount', 'Total fee(KRW)', 'Application time', 'Payment time',
                    'Update time', 'Forwarded time', 'Opening hours', 'Country', 'Recipient',
                    'Province', 'City', 'Detailed Address', 'Company', 'Zip code',
                    'Contact', 'Courier', 'Tracking number', 'Premium Repacking', 'Remark',
                    'Storage fee', 'Insurance', 'Splittinf fee', 'Premium re-packing'
                ], [8, 9, 10, 11, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24, 25, 30, 31, 32, 33],
                    'forward_load_forward'
                );
                ?>
                
            </div>

        </div>
    </div>
</section>

<!--
<section id="section-2"></section>

<section id="section-3"></section>

<section id="section-4"></section>
-->

<form id="print-form" action="/forward/print/" method="POST" target="print-window">
</form>

<script type="text/javascript">
jQuery(function($) {
    $(document).on('click', 'a.link', function() {
        var href = $(this).attr('href');
        var $section = $(this).closest('section');
        $section.hide();
        // $section.next().data('href', href).load(href);
        $('<section>').data('href', href).load(href).insertAfter($section);
        if ($('.reload-section-btn').length == 0)
            $('.page-title-right').append('<button class="btn btn-secondary reload-section-btn"><i class="fe-refresh-cw"></i></button>');

        return false;
    });
    $(document).on('click', '.cancel-btn', function() {
        var $section = $(this).closest('section');
        $section.prev().show();
        window.scrollTo(0,0);
        $section.remove();
    });
    $(document).on('click', '.reload-section-btn', function() {
        $('section:visible').trigger('reload');
    });
    $(document).on('reload', 'section', function() {
        // alert('reload');
        var href = $(this).data('href');
        $(this).load(href).show();
    });

    $(document).on('click', '.submit-btn', function() {
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                $('#section-2').html('');
                $('#section-1').show();
            }
        });
        return false;
    });

    $(document).on('click', '.ajax-btn', function() {
        var $section = $(this).closest('section');
        $.post('/wp-admin/admin-ajax.php', $(this).data(), function(response) {
            if (response.success) {
                $section.trigger('reload');
            } else {
                alert(response.data);
            }
        });
        return false;
    });

/*
    $(document).on('blur', '.calculate-forward-fee', function() {
        var $input = $('#input-packageweight');
        $input.val($(this).val());
        $input.closest('form').ajaxSubmit(function(response) {
            if (response.success) {
                $("#forwardfee").val(response.data.price);
                var servicefee = response.data.servicefee;
                var servicefeediscount = $("#servicefeediscount").val();
                servicefee = servicefee * servicefeediscount / 100;
                $("#servicefee").val(servicefee);
                calculateTotalFee();
            } else {
                alert(response.data);
            }
        })
    });

    $(document).on('blur', '.calculate-total-fee', function() {
        calculateTotalFee();
    });

    $(document).on('keyup', '.calculate-total-fee', function() {
        calculateTotalFee();
    });

    function calculateTotalFee() {
        $("#totalfee").val(
            Number($("#forwardfee").val())
            + Number($("#priorityfee").val())
            + Number($("#valueaddedfee").val())
            + Number($("#servicefee").val())
            // + Number($("#adjustment").val())
        );
    }
    */

    $('<button id="print-btn" class="btn btn-secondary" tabindex="0" aria-controls="DataTables_Table_0" type="button"><span>Print</span></button>')
        .appendTo('.dt-buttons')
        .click(function() {
            if ($('.datatable .check:checked').length == 0) {
                alert('Please select forwards to print.');
            } else {
                window.open('', 'print-window', 'menubar=no,location=no,resizable=yes,scrollbars=yes,status=no,top=0,left=0,width=1024,height=' + (screen.height - 100));
                var $pform = $('#print-form');
                $pform.html('');
                $('.datatable .check:checked').each(function() {
                    $pform.append('<input type="hidden" name="forwardid[]" value="' + $(this).val() + '">');
                });
                $pform.submit();
            }
            return false;
        });

    $('.check-all').click(function() {
        $('.datatable .check').prop('checked', $(this).is(':checked'));
    });

    $($('#userid-select').html()).insertBefore('#search-form input[name="userid"]');
});
</script>

<script type="text/html" id="userid-select">
    <select class="form-control" name="userid_prefix">
        <option></option>
        <?php foreach (range('A', 'Z') as $ch): ?>
            <option><?php echo $ch; ?>
        <?php endforeach; ?>
    </select>
</script>