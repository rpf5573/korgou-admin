<div class="card-box">
    <h5 class="card-title">Custom declaration</h5>

    <table class="table-bordered table table-hover">
        <thead class="thead-light">
        <tr>
            <th>Item content</th>
            <th>Quantity</th>
            <th>Total Value</th>
            <th>Net weight(g)</th>
            <th>HS Tariff Number</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($customs as $custom): ?>
        <tr>
            <td><?php echo $custom->contents; ?></td>
            <td><?php echo $custom->quantity; ?></td>
            <td><?php echo $custom->value; ?></td>
            <td><?php echo $custom->netweight; ?></td>
            <td><?php echo $custom->hstariffnumber; ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div> <!-- end card-box -->
