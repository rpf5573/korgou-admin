<div class="row mb-4">
    <div class="col-12">
        <?php include 'forward.php'; ?>
        <?php include 'address.php'; ?>
        <?php include 'remark.php'; ?>

        <p class="text-center">
            <button type="button" class="btn btn-secondary" onclick="history.back();">Back</button>
        </p>

    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.page-title').addClass('text-danger text-uppercase');
});
</script>
