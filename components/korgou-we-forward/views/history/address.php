<div class="card-box">
    <h5 class="card-title">Address & Shipment</h5>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Country</th>
            <th>Recipient</th>
            <th>Province</th>
            <th>City</th>
            <th>Detailed Address</th>
            <th>Company</th>
            <th>Zip code</th>
            <th>Contact</th>
            <th>Courier</th>
            <th>Tracking number</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo str_replace('-', '<br>', $forward->get_country_name()); ?></td>
            <td><?php echo $forward->englishname . '<br>' . $forward->nativename; ?></td>
            <td><?php echo $forward->province; ?></td>
            <td><?php echo $forward->city; ?></td>
            <td><?php echo $forward->addressdetail; ?></td>
            <td><?php echo $forward->company; ?></td>
            <td><?php echo $forward->zipcode; ?></td>
            <td><?php echo $forward->phonenum . '<br>' . $forward->mobilenum; ?></td>
            <td><?php echo str_replace('-', '<br>', $forward->forwardcouriername); ?></td>
            <td><?php echo $forward->forwardcouriertrackno; ?></td>
    </tbody>
    </table>
</div>