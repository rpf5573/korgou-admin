<div class="card-box">
    <h5 class="card-title">Transfer order information</h5>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Forward No.</th>
            <th>User ID</th>
            <th>Status</th>
            <th>Application time</th>
            <th>Billing time</th>
            <th>Payment time</th>
            <th>Forwarded time</th>
            <th>Update time</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td id="data-forwardid"><?php echo $forward->forwardid; ?></td>
            <td id="data-userid"><?php echo $forward->userid; ?></td>
            <td><?php echo $forward->get_status_name(); ?></td>
            <td><?php echo $forward->packagetime; ?></td>
            <td><?php echo $forward->billingtime; ?></td>
            <td><?php echo $forward->paytime; ?></td>
            <td><?php echo $forward->forwardtime; ?></td>
            <td><?php echo $forward->updatetime; ?></td>
        </tr>
    </tbody>
    </table>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Weight(g)</th>
            <th>Priority processing</th>
            <th>Premium re-packing</th>
            <th>Insurance</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $forward->packageweight; ?></td>
            <td><?php echo $forward->processing == 'Y' ? 'Premium' : 'Normal'; ?></td>
            <td><?php echo $forward->repacking; ?></td>
            <td><?php echo $forward->insurance; ?></td>
        </tr>
    </tbody>
    </table>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Other special requirements</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $forward->forwardcomment; ?></td>
        </tr>
    </tbody>
    </table>

    <h5 class="card-title">Fee <small>(KRW)</small></h5>

    <table class="table table-bordered">
    <thead class="thead-light">
        <tr>
            <th>Storage fee</th>
            <th>Shipping fee</th>
            <th>Processing discounts</th>
            <th>Processing fee</th>
            <th>Insurance</th>
            <th>Splitting fee</th>
            <th>Premium re-packing fee</th>
            <th>Value-added services fee</th>
            <th>Total fee</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $forward->get_storagefee_total(); ?></td>
            <td><?php echo $forward->forwardfee; ?></td>
            <td><?php if ($forward->servicefeediscount < 100): ?>
                    <b class="text-danger"><?php echo $forward->servicefeediscount; ?>%</b>
                <?php else: ?>
                    <?php echo $forward->servicefeediscount; ?>%
                <?php endif; ?>
            </td>
            <td><?php echo $forward->servicefee; ?></td>
            <td><?php echo $forward->insurancefee; ?></td>
            <td><?php echo $forward->splittingfee; ?></td>
            <td><?php echo $forward->repackingfee; ?></td>
            <td><?php echo $forward->valueaddedfee; ?></td>
            <td id="data-totalfee"><?php echo $forward->totalfee; ?></td>
        </tr>
    </tbody>
    </table>

    <?php if ($forward->status == Korgou_Forward::STATUS_CANCEL_REQUESTED ||
            $forward->status == Korgou_Forward::STATUS_CANNOT_BE_CANCELED ||
            $forward->status == Korgou_Forward::STATUS_CANCEL): ?>
        <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>Cancel reason</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $forward->cancel_reason; ?></td>
        </tbody>
        </table>
    <?php endif; ?>

</div>