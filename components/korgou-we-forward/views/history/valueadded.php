        <div class="card-box">
            <h5 class="card-title">Value-added services</h5>
            <ul>
                <?php foreach ($vas as $va): ?>
                    <li><?php echo "{$va->zhname} {$va->enname} {$va->koname}"; ?></li>
                <?php endforeach; ?>
            </ul>

        </div>
