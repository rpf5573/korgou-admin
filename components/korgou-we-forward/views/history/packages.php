<div class="card-box">
    <h5 class="card-title">Packages</h5>

    <table class="table-bordered table table-hover">
        <thead class="thead-light">
        <tr>
            <th>Package ID</th>
            <th>Arrival time</th>
            <th>Tracking Number</th>
            <th>Weight</th>
            <th>Type</th>
            <th>Storage Fee</th>
            <th>Remarks</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($packages as $package): ?>
        <tr>
            <td><?php echo $package->packageid; ?></td>
            <td><?php echo $package->arrivaltime; ?></td>
            <td><?php echo $package->domestictrackno; ?></td>
            <td><?php echo $package->weight; ?></td>
            <td><?php echo Korgou_Package::$TYPES[$package->type] ?? ''; ?></td>
            <td><?php echo $package->storagefee; ?></td>
            <td><?php echo $package->remark; ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div> <!-- end card-box -->

