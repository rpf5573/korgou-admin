<?php
    do_action('korgou_we_add_form', 'forward_add_courier', [[
        'name' => 'id', 'type' => 'text', 'label' => 'Number',
    ], [
        'name' => 'zhname', 'type' => 'text', 'label' => 'Chinese name',
    ], [
        'name' => 'enname', 'type' => 'text', 'label' => 'English name',
    ], [
        'name' => 'koname', 'type' => 'text', 'label' => 'Korean name',
    ], [
        'name' => 'status', 'type' => 'select', 'label' => 'Status', 'options' => Korgou_Forward_Courier::$STATUSES,
    ]]);
?>

<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form('update_courier'); ?>
                <table class="table table-sm table-bordered">
                    <thead class="thead-light">
                        <th>ID</th>
                        <th>Chinese name</th>
                        <th>English name</th>
                        <th>Korean name</th>
                        <th>Status</th>
                        <th>Delete</th>
                    </tbody>
                    <tbody>
                        <?php foreach ($couriers as $co): ?>
                            <tr>
                                <td class="align-middle">
                                    <?php echo $co->id; ?>
                                    <input type="hidden" name="id[]" class="form-control form-control-sm" value="<?php echo $co->id; ?>">
                                </td>
                                <td class="align-middle">
                                    <input type="text" name="zhname[]" class="form-control form-control-sm" value="<?php echo $co->zhname; ?>">
                                </td>
                                <td>
                                    <input type="text" name="enname[]" class="form-control form-control-sm" value="<?php echo $co->enname; ?>">
                                </td>
                                <td>
                                    <input type="text" name="koname[]" class="form-control form-control-sm" value="<?php echo $co->koname; ?>">
                                </td>
                                <td>
                                    <?php BS_Form::select([
                                        'name' => 'status[]',
                                        'class' => 'form-control-sm',
                                        'value' => $co->status,
                                        'options' => Korgou_Forward_Courier::$STATUSES
                                    ]); ?>
                                </td>
                                <td class="align-middle">
                                    <button type="button" class="btn btn-danger btn-sm delete-btn" data-id="<?php echo $co->id; ?>">Delete</button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

                <p class="text-center">
                    <button type="button" id="submit-btn" class="btn btn-primary">Update</button>
                </p>
            </form>
        </div>

    </div>
</div>

<?php $this->ajax_form('delete_courier'); ?>
    <input type="hidden" name="id" id="delete-id">
</form>

<script type="text/javascript">
jQuery(function($) {
    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        $form.ajaxSubmit((response) => {
            if (response.success) {
                alert(response.data);
                location.reload();
            } else {
                alert("抱歉，系统异常！");
            }
        });
    });
    $('.delete-btn').click(function() {
		if(confirm("Are you sure to delete?")) {
            $('#delete-id').val($(this).data('id')).closest('form').ajaxSubmit((response) => {
                alert(response.data);
                location.reload();
            });
        }
        return false;
    });
});
</script>

