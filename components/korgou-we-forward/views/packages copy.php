        <div class="card-box">
            <h5 class="card-title">Packages selected for forward</h5>

            <table class="table-bordered table table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Package No.</th>
                    <th>Arrival time</th>
                    <th>Package source</th>
                    <th>Domestic courier</th>
                    <th>Tracking number</th>
                    <th>Items in package</th>
                    <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($packages as $package): ?>
                <tr>
                    <td><?php echo $package->packageid; ?></td>
                    <td><?php echo $package->arrivaltime; ?></td>
                    <td><?php echo $package->packagesource; ?></td>
                    <td><?php echo $package->domesticcourier; ?></td>
                    <td><?php echo $package->domestictrackno; ?></td>
                    <td><?php echo $package->packagecontent; ?></td>
                    <td><?php echo $package->remark; ?></td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div> <!-- end card-box -->

