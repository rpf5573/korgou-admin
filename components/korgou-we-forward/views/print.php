<?php
// print_r($forwards);
?>
<style>
body {
    background-color: white;
}
* {
    font-size: 21px;
}
</style>

<div class="container-fluid">
    <table class="table table-bordered">
        <?php foreach ($forwards as $forward): ?>
            <tr>
                <td>
                    <b><?php echo korgou_user_role_id($forward->userid); ?></b>
                    <br>
                    <?php echo $forward->forwardid; ?>
                    <br>
                    <?php foreach ($forward->vas as $va): ?>
                        <b><?php echo $va->koname; ?></b><br>
                    <?php endforeach; ?>
                    <?php if ($forward->repacking == 'Y'): ?>
                        <div class="border-top"><i>PREMIUM REPACKING</i></div>
                    <?php endif; ?>
                </td>
                <td class="p-0">
                    <table class="table table-bordered mb-0">
                        <?php $rows = array_chunk($forward->packages, 3); ?>
                        <?php foreach ($rows as $row): ?>
                            <tr>
                                <?php foreach ($row as $i => $package): ?>
                                    <td style="width: 33.3333%;">
                                        <?php echo $package->domestictrackno; ?>
                                    </td>
                                <?php endforeach; ?>
                                <?php for (; $i < 2; $i++): ?>
                                    <td></td>
                                <?php endfor; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </td>
                <td>
                    <b>
                        <div><?php echo $forward->englishname; ?></div>
                        <div class="border-top"><?php echo $forward->country; ?></div>
                        <div class="border-top"><?php echo str_replace('-', '<br>', $forward->forwardcouriername); ?></div>
                    </b>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="height: 100px;">&nbsp;</td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>

<script type="text/javascript">
jQuery(function($) {
    return;
    window.print();
    setTimeout(window.close, 1000);
});
</script>

