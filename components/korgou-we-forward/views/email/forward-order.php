<div style="margin-bottom: 40px;">
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<tbody>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Forward Order No.', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><a href="<?php echo network_site_url('/my/forwards/?id=' . $forward->forwardid); ?>" target="_blank"><?php echo $forward->forwardid; ?></a></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Package weight', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->packageweight; ?></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Shipping fee', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->forwardfee; ?></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Processing fee', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->servicefee; ?></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Insurance fee', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->insurancefee; ?></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Splitting fee', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->splittingfee; ?></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Premium re-packing fee', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->repackingfee; ?></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Value-added services fee', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->valueaddedfee; ?></th>
			</tr>
			<tr>
				<th class="td" scope="col" style=""><?php _e( 'Total fee', 'korgou' ); ?></th>
				<th class="td" scope="col" style=""><?php echo $forward->totalfee; ?></th>
			</tr>
		</tbody>
	</table>
</div>
