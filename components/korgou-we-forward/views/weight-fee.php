<?php
function display_item($box_sizes, $i, $item = null) {
    ?>
    <tr>
        <td>
            <span class="item-no"><?php echo $i + 1; ?></span>
            <input type="hidden" class="form-control item-id" name="item_id[]" value="<?php if ($item != null) echo $item->id; ?>">
        </td>
        <td>
            <select class="form-control input-boxsize calculate-fee" id="input-boxsize-<?php echo $i+1; ?>" name="item_boxsize[]" data-item="<?php echo $i+1; ?>" style="width: 64px; padding-left: .1rem; padding-right: inherit;">
                <option data-weight=""></option>
                <?php if ($item != null && !empty($item->boxsize) && !in_array($item->boxsize, array_keys($box_sizes))): ?>
                    <option data-weight="" selected><?php echo $item->boxsize; ?></option>
                <?php endif; ?>
                <?php foreach ($box_sizes as $size => $weight): ?>
                    <option data-weight="<?php echo $weight; ?>" <?php if ($item != null && $item->boxsize == $size) echo 'selected'; ?>><?php echo $size; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
        <td>
            <input type="text" class="form-control input-length calculate-fee" id="input-length-<?php echo $i+1; ?>" name="item_length[]" data-item="<?php echo $i+1; ?>" value="<?php if ($item != null) echo $item->length; ?>">
        </td>
        <td>
            <input type="text" class="form-control input-width calculate-fee" id="input-width-<?php echo $i+1; ?>" name="item_width[]" data-item="<?php echo $i+1; ?>" value="<?php if ($item != null) echo $item->width; ?>">
        </td>
        <td>
            <input type="text" class="form-control input-height calculate-fee" id="input-height-<?php echo $i+1; ?>" name="item_height[]" data-item="<?php echo $i+1; ?>" value="<?php if ($item != null) echo $item->height; ?>">
        </td>
        <td>
            <input type="text" class="form-control input-weight calculate-fee" id="input-weight-<?php echo $i+1; ?>" name="item_weight[]" data-item="<?php echo $i+1; ?>" value="<?php if ($item != null) echo $item->weight; ?>" data-box="" data-dim="">
        </td>
        <td>
            <input type="text" class="form-control input-forwardfee" id="input-forwardfee-<?php echo $i+1; ?>" name="item_forwardfee[]" data-item="<?php echo $i+1; ?>" value="<?php if ($item != null) echo $item->forwardfee; ?>" >
        </td>
        <td>
            <button type="button" class="btn btn-link btn-xs p-0 remove-row-btn"><i class="mdi mdi-close-circle-outline text-danger h3"></i></button>
        </td>
    </tr>
    <?php
}
?>

<div class="card-box">

    <?php $this->ajax_form('update_weight_fee'); ?>
        <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">

        <div class="row">
            <div class="col">

                <table class="table table-bordered table-sm">
                <tbody id="fee-table">
                    <tr>
                        <th>Courier</th>
                        <td>
                            <?php echo $forward->forwardcouriername; ?>
                        </td>
                        <th>Country(zone)</th>
                        <td>
                            <select class="form-control form-select-sm" name="shipping_country" style="height: 1.95rem;">
                                <option value="">Select country</option>
                                <?php foreach ($countries as $country): ?>
                                    <option <?php if ($forward->shipping_country == $country) echo 'selected'; ?>><?php echo $country; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
                </table>

                <table class="table table-bordered table-sm">
                <thead class="thead-light">
                    <th>No.</th>
                    <th>Box size</th>
                    <th>Length(cm)</th>
                    <th>Width(cm)</th>
                    <th>Height(cm)</th>
                    <th>Weight(g)</th>
                    <th>Shipping fee(KRW)</th>
                    <th></th>
                </thead>
                <tbody id="forward-items">
                    <?php
                    $box_sizes = Korgou_Forward_Item::get_box_sizes($forward->forwardcourierid);
                    for($i = 0; $i < sizeof($items); $i++) {
                        display_item($box_sizes, $i, $items[$i]);
                    }
                    for(; $i < 10; $i++) {
                        display_item($box_sizes, $i, $items[$i]);
                    }
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">Total</td>
                        <td>
                            <input type="text" class="form-control-plaintext calculate-forward-fee" id="packageweight" name="packageweight"
                                value="<?php echo $forward->packageweight; ?>" required readonly>
                        </td>
                        <td>
                            <input type="text" readonly class="form-control-plaintext forwardfee" value="<?php echo $forward->forwardfee; ?>">
                        </td>
                    </tr>
                </tfoot>
                </table>
                <div class="">
                    <button type="button" class="btn btn-info" id="add-row-btn">Add row</button>
                </div>
            </div>

            <div class="col">
                <table class="table table-bordered">
                <tbody id="fee-table">
                    <tr>
                        <th>User ID</th>
                        <td>
                            <?php echo korgou_user_role_id($forward->userid); ?>
                        </td>
                        <th>Country</th>
                        <td>
                            <?php echo $forward->get_country_name('en'); ?>
                        </td>
                    </tr>
                </tbody>
                </table>

                <table class="table table-bordered table-sm">
                <thead class="thead-light">
                    <th>Item</th>
                    <th>Fee</th>
                </thead>
                <tbody>
                    <tr>
                        <th>Item storage fee(KRW)
                        </th>
                        <td>
                            <input type="text" class="form-control storagefee input-storagefee calculate-total-fee" id="storagefee" name="storagefee"
                                value="<?php echo $forward->storagefee; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Forwarding storage fee(KRW)
                        </th>
                        <td>
                            <input type="text" class="form-control storagefee2 input-storagefee calculate-total-fee" id="storagefee2" name="storagefee2"
                                value="<?php echo $forward->storagefee2; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Storage fee total(KRW)
                        </th>
                        <td class="pl-3">
                            <input type="text" class="form-control-plaintext storagefee-total" id="storagefee-total" name="storagefee-total"
                                value="<?php echo $forward->get_storagefee_total(); ?>" readonly >
                        </td>
                    </tr>
                    <tr>
                        <th>Shipping fee(KRW)
                        </th>
                        <td>
                            <input type="text" class="form-control forwardfee calculate-total-fee" name="forwardfee" value="<?php echo $forward->forwardfee; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Processing option</th>
                        <td class="pl-3">
                            <?php echo ($forward->processing == 'P') ? 'Priority' : 'Normal'; ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Processing fee discount
                        </th>
                        <td>
                            <div class="input-group">
                                <input type="text" class="form-control " id="servicefeediscount" name="servicefeediscount" value="<?php echo $forward->servicefeediscount; ?>">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">%</span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>Processing fee(<?php echo $processing_unit_fee; ?> KRW/kg)
                        </th>
                        <td>
                            <input type="text" class="form-control calculate-total-fee" id="servicefee" name="servicefee" value="<?php echo $forward->servicefee; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Insurance(KRW)</th>
                        <td>
                            <input type="text" class="form-control calculate-total-fee" id="insurancefee" name="insurancefee" value="<?php echo $forward->insurancefee; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Splitting fee(KRW)</th>
                        <td>
                            <input type="text" class="form-control calculate-total-fee" id="splittingfee" name="splittingfee" value="<?php echo $forward->splittingfee; ?>" required>
                        </td>
                    </tr>
                    <tr>
                        <th>Premium re-packing fee(KRW) (= Weight X 1000 + 4000)</th>
                        <td>
                            <input type="text" class="form-control calculate-total-fee " id="repackingfee" name="repackingfee" value="<?php echo $forward->repackingfee; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Value-added services fee(KRW)
                        </th>
                        <td>
                            <input type="text" class="form-control calculate-total-fee" id="valueaddedfee" name="valueaddedfee" value="<?php echo $forward->valueaddedfee; ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>Total fee(KRW)
                        </th>
                        <td class="pl-3">
                            <input type="text" class="form-control-plaintext" id="totalfee" name="totalfee" value="<?php echo $forward->totalfee; ?>" readonly>
                        </td>
                    </tr>
                </table>
                <p class="text-right">
                    <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                    <button type="button" class="btn btn-primary" id="weight-fee-btn" <?php if ($forward->status == Korgou_Forward::STATUS_FORWARDED) echo ' disabled'; ?>>Save</button>
                </p>
            </div>
        </div>
    </form>
</div>

<?php $this->ajax_form('calculate_forward_fee'); ?>
    <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">
    <input type="hidden" name="packageweight" id="input-packageweight">
</form>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jsrender/1.0.11/jsrender.min.js" integrity="sha512-bKlNlbTH3duwZ28zoqEhXui/yuaPuQVci6OAVu0zh2WfYbEKD39HszVR8byP4/L4YyBo3b5CGIY+4ldVN93uCg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script id="forward-item-tmpl" type="text/x-jsrender">
    <tr>
        <td>
            <span class="item-no"></span>
            <input type="hidden" class="form-control" name="item_id[]" value="">
        </td>
        <td>
            <select class="form-control input-boxsize calculate-fee" name="item_boxsize[]" style="width: 64px; padding-left: .1rem;">
                <option data-weight=""></option>
                <?php foreach ($box_sizes as $size => $weight): ?>
                    <option data-weight="<?php echo $weight; ?>"><?php echo $size; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
        <td>
            <input type="text" class="form-control input-lehgth calculate-fee" name="item_length[]" value="">
        </td>
        <td>
            <input type="text" class="form-control input-width calculate-fee" name="item_width[]" value="">
        </td>
        <td>
            <input type="text" class="form-control input-height calculate-fee" name="item_height[]" value="">
        </td>
        <td>
            <input type="text" class="form-control input-weight calculate-fee" name="item_weight[]" value="" data-box="" data-dim="">
        </td>
        <td>
            <input type="text" class="form-control input-forwardfee" name="item_forwardfee[]" value="" >
        </td>
        <td>
            <button type="button" class="btn btn-link btn-xs p-0 remove-row-btn"><i class="mdi mdi-close-circle-outline text-danger h3"></i></button>
        </td>
    </tr>
</script>

<script type="text/javascript" src="/wp-content/themes/astra-ubold/assets/js/vendor.min.js"></script>
<script type="text/javascript">
jQuery(function($) {
    /*
    1	Korea Post EMS
    2	DHL Express
    3	TNT Express
    4	SF Express (China, HongKong, Taiwan, Singapore, Malaysia, Japan, US and Russia ONLY)
    5	Duty free channel (to China ONLY)
    6	Korea Post Sea Parcel
    7	Korea Post Air Parcel
    8	Domestic shipping delivery(CJ  logistics)
    9	FEDEX Express
    10  Korea Post Small Packet
    11  UPS
    9999	User Defined (additional charges apply)
    */
    var courierid = '<?php echo $forward->forwardcourierid; ?>';

    const DIM_WEIGHT_COURIERS = [
        '2', '3', '9', '11',  // 5000
        '1', '7', '10'  // 6000
    ];

    var processingUnitFee = <?php echo $processing_unit_fee; ?>;

    var insurance = <?php echo $forward->insurance == 'Y' ? 'true' : 'false'; ?>;

    var repacking = <?php echo $forward->repacking == 'Y' ? 'true' : 'false'; ?>;

    var totalWeight, totalWeightKg, totalWeightProcessingKg, boxCount;
    function calculateTotalWeight() {
        totalWeight = 0;
        totalWeightKg = 0;
        boxCount = 0;

        $('.input-weight').each(function() {
            const val = parseInt($(this).val());
            if (!isNaN(val)) {
                totalWeight += val;
                boxCount++;
            }
        });
        totalWeightKg = Math.ceil(totalWeight / 1000.0);
        totalWeightProcessingKg = Math.max(1.0, Math.ceil(totalWeight / 1000.0 * 2) * 0.5);

        $('#packageweight').val(totalWeight);
    }
    calculateTotalWeight();

    function calculateSplittingfee() {
        var splittingfee = (boxCount > 1) ? boxCount * 1000 : 0;
        $('#splittingfee').val(splittingfee);
    }

    function calculateRepackingfee() {
        if (repacking) {
            if ($('#repackingfee').length > 0) {
                $('#repackingfee').val(totalWeightKg * 1000 + 4000);
            }
        }
    }

    function calculateInsurancefee() {
        if (insurance) {
            if (courierid == '1' || courierid == '6' || courierid == '7' || courierid == '3' || courierid == '9') {
                $('#insurancefee').val(boxCount * 10000);
            } else if (courierid == '2') {
                $('#insurancefee').val(boxCount * 15000);
            }
        }
    }

    function calculateTotal() {
        calculateRepackingfee();
        calculateInsurancefee();

        var totalFee = 0;
        $('.calculate-total-fee').each(function() {
            const val = parseInt($(this).val());
            if (!isNaN(val)) {
                totalFee += val;
            }
        });

/*
        $('#priorityfee').each(function() {
            $(this).val(totalWeight/1000*4000);
        });
        */

        $('#totalfee').val(totalFee);
    }

    function selectPrice(weight) {
        var country = fares[$('select[name="shipping_country"]').val()];
        //console.log($('select[name="shipping_country"]').val());
        // console.log(country);
        var price = 0;
        for (const cw in country) {
            var w = parseInt(cw);
            if (weight <= w) {
                price = country[cw];
                break;
            }
        }
        // console.log(price);
        
        return price;
    }

    function getValue(field, $tr) {
        var val = parseInt($tr.find('.input-'+field).val());
        // console.log(field, ': ', val);
        return !isNaN(val) && isFinite(val) ? val : 0;
    }

    function calculateWeight($tr) {
        var length = getValue('length', $tr),
            width = getValue('width', $tr),
            height = getValue('height', $tr);
        return ['2', '3', '9', '11'].includes(courierid) ?
            Math.ceil(length * width * height / 5000) * 1000 :
            Math.ceil(length * width * height / 6000) * 1000 ;
    }

    function calculateFee(self) {
        var $tr = $(self).closest('tr'),
            weight = getValue('weight', $tr),
            boxWeight = $tr.find('.input-boxsize option:selected').data('weight'),
            dimWeight = calculateWeight($tr),

        boxWeight = (boxWeight != '') ? parseInt(boxWeight) : 0;

        var feeWeight = DIM_WEIGHT_COURIERS.includes(courierid) ? Math.max(boxWeight, dimWeight, weight) : weight;

        // console.log('box:', boxWeight, 'dim:', dimWeight, 'weight:', weight, 'fee weight:', feeWeight);

        $tr.find('.input-forwardfee').val((feeWeight > 0) ? selectPrice(feeWeight) : '');
        calculateServicefee();
        calculateTotal();
    }

    function calculateForwardfee() {
        var forwardfee = 0;
        $('.input-forwardfee').each(function() {
            forwardfee += parseInt($(this).val()) || 0;
        });
        $('.forwardfee').val(forwardfee);
    }

    function calculateStoragefee() {
        var total = 0;
        $('.input-storagefee').each(function() {
            total += parseInt($(this).val()) || 0;
        });
        $('.storagefee-total').val(total);
    }

    function calculateServicefee() {
        var servicefee = totalWeightProcessingKg * processingUnitFee * parseInt($('#servicefeediscount').val()) / 100.0;
        $('#servicefee').val(servicefee);
    }

    $('.storagefee').keyup(function() {
        calculateStoragefee();
        calculateTotal();
    });
    $('.storagefee2').keyup(function() {
        calculateStoragefee();
        calculateTotal();
    });
    $('.calculate-total-fee').blur(function() {
        calculateTotal();
    });
    $('.calculate-total-fee').keyup(function() {
        calculateTotal();
    });
    $('.calculate-total-fee').change(function() {
        calculateTotal();
    });
    $('#servicefeediscount').keyup(function() {
        calculateServicefee();
        calculateTotal();
    });
    $('#servicefeediscount').change(function() {
        calculateServicefee();
        calculateTotal();
    });
    // $('.input-forwardfee').change(function() {
    $(document).on('keyup', '.input-forwardfee', function() {
        // console.log('k');
        calculateForwardfee();
        calculateServicefee();
        calculateSplittingfee();
        calculateTotal();
    });
    $(document).on('keyup', '.calculate-fee', function() {
        calculateFee(this);
        calculateForwardfee();
        calculateTotalWeight();
        calculateServicefee();
        calculateSplittingfee();
        calculateTotal();
    });
    $(document).on('change', '.calculate-fee', function() {
        calculateFee(this);
        calculateForwardfee();
        calculateTotalWeight();
        calculateServicefee();
        calculateSplittingfee();
        calculateTotal();
    });
    $('select[name="shipping_country"]').change(function() {
        $('.input-weight').each(function() {
            calculateFee(this);
            calculateForwardfee();
            calculateTotalWeight();
            calculateServicefee();
            calculateSplittingfee();
            calculateTotal();
        });
    });
    // calculateTotal();

    var fares = <?php echo json_encode($fares); ?>;

    $('#weight-fee-btn').click(function() {
		var packageweight = $('input[name="packageweight"]').val();
		if (packageweight == '') {
			alert("Please enter weight.");
			return false;
		}
		var forwardfee = $('input[name="forwardfee"]').val();
		if (forwardfee == '') {
			alert("Please enter the transfer fee.");
			return false;
		}
		var servicefee = $('input[name="servicefee"]').val();
		if (servicefee == '') {
			alert("Please enter the transfer service charge.");
			return false;
		}
		var valueaddedfee = $('input[name="valueaddedfee"]').val();
		if (valueaddedfee == '') {
			alert("Please enter the value added service charge.");
			return false;
		}
		var totalfee = $('input[name="valueaddedfee"]').val();
		if (totalfee == '') {
			alert("Please enter the total charge.");
			return false;
		}
        var $section = $(this).closest('section');
        var $form = $(this).closest('form');
        $(this).prop('disabled', true).text('Saving...').append('<div class="spinner-border spinner-border-sm text-light ml-2" role="status"><span class="sr-only">Loading...</span></div>');
        $('body').css({ 'cursor': 'wait' });
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                location.href = '/forward/detail/?forwardid=<?php echo $forward->forwardid; ?>';
            }
        });
        return false;
    });

    $('#add-row-btn').click(function() {
        var html = $('#forward-item-tmpl').html();
        $('#forward-items').append(html);
        numberItems();
    });

    $(document).on('click', '.remove-row-btn', function() {
        var $tr = $(this).closest('tr');
        $tr.hide();
        $tr.find('.input-forwardfee').val('');
        calculateForwardfee();
        calculateTotalWeight();
        calculateServicefee();
        calculateSplittingfee();
        calculateTotal();
        numberItems();
    });

    function numberItems() {
        var no = 0;
        $('.item-no').each(function(i) {
            if ($(this).closest('tr').is(':visible'))
                $(this).text(++no);
        });
    }
});
</script>
