<div class="card-box">
    <h5 class="card-title">History</h5>

    <table class="table-bordered table table-hover">
        <thead class="thead-light">
        <tr>
            <th>Date</th>
            <th>User</th>
            <th>状态</th>
            <th>转运说明</th>
            <th>重量(G/克)</th>
            <th>Priority Fee(KRW)</th>
            <th>转运费(KRW)</th>
            <th>增值服务费(KRW)</th>
            <th>服务费(KRW)</th>
            <th>服务费折扣</th>
            <th>人工调价(KRW)</th>
            <th>总收费(KRW)</th>
            <th>申请时间</th>
            <th>付款时间</th>
            <th>收货时间</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($history as $item): $forward = new Korgou_Forward(); $forward->from_json($item->data); ?>
        <tr>
            <td><?php echo $item->create_date; ?></td>
            <td><?php echo $item->userid; ?></td>
            <td><?php echo $forward->get_status_name(); ?></td>
            <td><?php echo $forward->forwardcomment; ?></td>
            <td><?php echo $forward->packageweight; ?></td>
            <td><?php echo $forward->priorityfee; ?></td>
            <td><?php echo $forward->forwardfee; ?></td>
            <td><?php echo $forward->valueaddedfee; ?></td>
            <td><?php echo $forward->servicefee; ?></td>
            <td><?php if ($forward->servicefeediscount < 100): ?>
                    <b class="text-danger"><?php echo $forward->servicefeediscount; ?>%</b>
                <?php else: ?>
                    <?php echo $forward->servicefeediscount; ?>%
                <?php endif; ?>
            </td>
            <td><?php echo $forward->adjustment; ?></td>
            <td id="data-totalfee"><?php echo $forward->totalfee; ?></td>
            <td><?php echo $forward->packagetime; ?></td>
            <td><?php echo $forward->paytime; ?></td>
            <td><?php echo $forward->receipttime; ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div> <!-- end card-box -->

