<div class="card-box">

    <?php $this->ajax_form('change_status'); ?>

        <input type="hidden" name="forwardid" value="<?php echo $forward->forwardid; ?>">

        <table class="table table-bordered w-50">
        <tbody>
            <tr>
                <th>Current state</th>
                <td>
                    <input type="text" class="form-control-plaintext" value="<?php echo $forward->get_status_name(); ?>" readonly>
                </td>
            </tr>
            <tr>
                <th>Status</th>
                <td>
                    <?php BS_Form::select([
                        'options' => Korgou_Forward::$STATUSES,
                        'name' => 'status',
                        'value' => $forward->status,
                    ]); ?>
                </td>
            </tr>
        </tbody>
        </table>

        <p>
            <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
            <button type="button" class="btn btn-primary change-status-btn">Save</button>
        </p>

    </form>
</div>

<script type="text/javascript">
jQuery(function($) {
    $(document).on('click', '.change-status-btn', function() {
        var $section = $(this).closest('section');
        var $form = $(this).closest('form');
        $form.ajaxSubmit(function(response) {
            alert(response.data);
            if (response.success) {
                location.href = '/forward/detail/?forwardid=<?php echo $forward->forwardid; ?>';
            }
        });
        return false;
    });
});
</script>