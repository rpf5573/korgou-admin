    <?php foreach ($controls as $control): $fg_id = 'form-group-' . uniqid(); ?>
        <div class="form-group mr-2 mb-2" id="<?php echo $fg_id; ?>">
            <label class="mr-1"><?php echo $control['label']; ?></label>
            <?php if ($control['type'] == 'text'): ?>
                <input type="text" class="form-control mr-2" name="<?php echo $control['name']; ?>">
            <?php elseif ($control['type'] == 'country'): ?>
                <?php do_action('korgou_select_country', '', ['class' => 'mr-2']); ?>
            <?php elseif ($control['type'] == 'select'): ?>
                <?php
                    $control['class'] = 'mr-2';
                    $control['blank_option'] = '- Please choose -';
                    BS_Form::select($control); ?>
            <?php elseif ($control['type'] == 'period'): ?>
                <input type="text" class="datepicker form-control mr-1 input-start-date" name="<?php echo $control['name'][0]; ?>" style="width: 100px;">~
                <input type="text" class="datepicker form-control ml-1 mr-2 input-end-date" name="<?php echo $control['name'][1]; ?>" style="width: 100px;">
                <script type="text/javascript">
                    jQuery(function($) {
                        $('#<?php echo $fg_id; ?> .datepicker').datepicker({
                            autoclose: true,
                            format: 'yyyy-mm-dd'
                        });
                    });
                </script>
            <?php elseif ($control['type'] == 'period_buttons'): ?>
                <input type="text" class="datepicker form-control mr-1 input-start-date" name="<?php echo $control['name'][0]; ?>" style="width: 100px;">~
                <input type="text" class="datepicker form-control ml-1 mr-2 input-end-date" name="<?php echo $control['name'][1]; ?>" style="width: 100px;">
                <button type="button" class="btn btn-light mr-2 date-btn" data-start="0" data-end="0">Today</button>
                <button type="button" class="btn btn-light mr-2 date-btn" data-start="1" data-end="1">Yesterday</button>
                <button type="button" class="btn btn-light mr-2 date-btn" data-start="7" data-end="0">Week</button>
                <button type="button" class="btn btn-light mr-2 date-btn" data-start="30" data-end="0">Month</button>
                <script type="text/javascript">
                    jQuery(function($) {
                        $('#<?php echo $fg_id; ?> .datepicker').datepicker({
                            autoclose: true,
                            format: 'yyyy-mm-dd'
                        });
                        function setDate(start, end) {
                        }
                        $('#<?php echo $fg_id; ?> .date-btn').click(function() {
                            var today = new Date(), date = new Date();
                            date.setDate(today.getDate() - parseInt($(this).data('start')));
                            $('#<?php echo $fg_id; ?> .input-start-date').datepicker('setDate', date);
                            date.setDate(today.getDate() - parseInt($(this).data('end')));
                            $('#<?php echo $fg_id; ?> .input-end-date').datepicker('setDate', date);
                            return false;
                        });

                    });
                </script>
            <?php elseif ($control['type'] == 'search'): ?>
                <button type="submit" class="btn btn-primary" id="search-btn">Search</button>
            <?php elseif ($control['type'] == 'submit'): ?>
                <button type="submit" class="btn btn-primary" id="submit-btn"><?php echo $control['button_label'] ?? 'Submit'; ?></button>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
