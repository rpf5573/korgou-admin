<div class="row">
    <div class="col-12">
        <div class="card-box">
            <?php $this->ajax_form($action, ['class' => 'form-inline']); ?>
                <?php include 'form-controls.php'; ?>

                <button type="button" class="btn btn-info mb-2 add-btn">Add</button>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
jQuery(function($) {
    $('.add-btn').click(function() {
        if(confirm("你确定添加吗？")) {
            $(this).closest('form').ajaxSubmit(function(response) {
                alert(response.data);
                location.reload();
            });
        }
    });
});
</script>