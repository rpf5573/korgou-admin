                <?php foreach ($controls as $control): ?>
                    <?php if ($control['type'] == 'spacer') { echo '<div class="mb-1" style="flex-basis: 100%; height: 0;"></div>'; continue; } ?>
                    <label><?php echo $control['label']; ?>： </label>
                    <?php if ($control['type'] == 'text'): ?>
                        <input type="text" class="form-control mr-2" name="<?php echo $control['name']; ?>">
                    <?php elseif ($control['type'] == 'country'): ?>
                        <?php do_action('korgou_select_country', '', ['class' => 'mr-2']); ?>
                    <?php elseif ($control['type'] == 'select'): ?>
                        <?php
                            $control['class'] = 'mr-2';
                            $control['blank_option'] = '- Please choose -';
                            BS_Form::select($control); ?>
                    <?php endif; ?>
                <?php endforeach; ?>
