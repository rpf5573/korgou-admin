<form id="search-form" class="form-inline">
    <?php $controls[] = ['type' => $type]; ?>
    <?php include 'form-controls.php'; ?>
    <?php echo $extra; ?>
</form>

<hr class="border-secondary1 mb-4" style="border-color: lightgray;">
