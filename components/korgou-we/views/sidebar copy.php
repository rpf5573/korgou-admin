<div class="left-side-menu" style="top: 0; display: inherit;">

    <div class="slimScrollDiv" style="position: relative; width: auto;"><div class="slimscroll-menu" style="width: auto;">

        <!--- Sidemenu -->
        <div id="sidebar-menu" class="active">

            <ul class="metismenu in" id="side-menu">

                <?php foreach ($rights as $right): ?>
                <li>
                    <a href="<?php echo $right->url; ?>" target="main">
                        <span> <?php echo $right->rightname; ?> </span>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div><div class="slimScrollBar" style="background: rgb(158, 165, 171); width: 8px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 170.56px;"></div><div class="slimScrollRail" style="width: 8px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
    <!-- Sidebar -left -->

</div>

<script type="text/javascript">
jQuery(function($) {
    $('#sidebar-menu a').click(function() {
        $('#sidebar-menu a').removeClass('active');
        $(this).addClass('active');
    }).get(0).click();
});
</script>
