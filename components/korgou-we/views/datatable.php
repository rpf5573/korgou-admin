    <div class="text-right float-right">
        <div class="btn-group dropdown-hover dt-column-selector" style="width: 180px; display: none;" id="dt-column-selector">
            <button type="button" class="btn btn-outline-secondary dropdown-toggle dt-toggle-column-btn" data-toggle="dropdown" aria-expanded="false"
                style="border-color: #ced4da;">Edit the display items
                <i class="mdi mdi-chevron-down float-right"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-start" style="position: absolute; width: 180px;">
                <!-- <a class="dropdown-item" href="#"><input type="checkbox" class="align-middle" />&nbsp;Option 1</a> -->
            </div>
        </div>
    </div>

<table class="table table-sm table-bordered datatable" style="width: 100%;">
    <thead class="thead-light">
        <?php foreach ($columns as $column): ?>
            <th><?php echo $column; ?></th>
        <?php endforeach; ?>
    </thead>
    <tbody>
    </tbody>
</table>

<script type="text/javascript">
var $datatable;

jQuery(function($) {
    var hiddenColumns = [<?php echo $hidden_columns; ?>];

    $('.datatable th').each(function(index) {
        // console.log($(this).html());
        $('.dt-column-selector .dropdown-menu').append(
            '<a class="dropdown-item" href="#" data-column="' + index + '" data-checked="' + !hiddenColumns.includes(index) + '">' +
            '<input type="checkbox" class="align-middle" ' +
            (hiddenColumns.includes(index) ? '' : 'checked') + 
            '/> ' + $(this).text() + '</a>');
    });

    $( '.dt-column-selector' ).on('click', 'a', function() {
        var $target = $(this), $inp = $target.find( 'input' );

        // console.log($target.data('checked'));
        if ($target.data('checked')) {
            $target.data('checked', false);
            setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
        } else {
            $target.data('checked', true);
            setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
        }

        var column = $datatable.column( $(this).attr('data-column') );
        column.visible( ! column.visible() );

        $( event.target ).blur();

        return false;
    });

    $datatable = $('.datatable').DataTable( {
        dom: 'Brtlip',
        buttons: [
            // 'copy', 'csv', 'excel', 'pdf', 'print'
            'excel'
        ],
        processing: true,
        serverSide: true,
        searching: false,
        ordering: false,
        autoWidth: false,
        pagingType: 'full_numbers',
        // 'buttons': [
        //     'copy', 'csv', 'excel', 'print'
        // ],
        language: {
            processing: '<i class="fa fa-spinner fa-spin fa-5x fa-fw"></i><span class="sr-only">Loading...</span>'
        },
        ajax: {
            'url': '<?php echo admin_url( 'admin-ajax.php' ); ?>',
            'type': 'POST',
            'data': function(d) {
                d.action = '<?php echo $action; ?>';
                d._wpnonce = '<?php echo $nonce; ?>';

                $('#search-form').serializeArray().forEach(function(param) {
                    if (param.value != '')
                        d[param.name] = param.value;
                });

                // console.log(d);

            }
        },
        // 'columns': [<?php echo $columns; ?>],
        columnDefs: [{targets: hiddenColumns, visible: false}]
    });

    $(document).on( 'init.dt', function ( e, settings ) {
        var $columnSelector = $('#dt-column-selector');
        // $( $datatable.table().container() ).eq(0).children().eq(0).children().eq(1).append($columnSelector);
        $columnSelector.show().parent().addClass('text-right');
    } );
    $(document).on( 'draw.dt', function ( e, settings ) {
        $('[data-toggle="tooltip"]').tooltip();
    } );
    $datatable.on('xhr.dt', function(e, settings, json, xhr) {
    });

    $('#search-btn').click(function() {
        $datatable.draw();
        return false;
    }).closest('form').submit(function() {
        $datatable.draw();
        return false;
    });

    var $btn = $('#submit-btn'), $form = $btn.closest('form');
    $form.submit(function() {
        return false;
    });
    $btn.click(function() {
        $form.ajaxSubmit(function(response) {
            if (response.success) {
                alert(response.data);
                location.reload();
            } else {
                alert("抱歉，系统异常！");
            }
        });
    });
});
</script>
