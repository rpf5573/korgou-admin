<?php
defined( 'ABSPATH' ) or exit;

return new class(__FILE__) extends Shoplic_Controller
{
    protected $id = 'korgou_we';

    function load()
    {
        $this->add_shortcode('main');
        $this->add_shortcode('body');
        $this->add_shortcode('header');
        $this->add_shortcode('sidebar');
        $this->add_shortcode('service_management');

        $this->add_action('datatable_toggle_column');
        $this->add_action('datatable', 10, 3);
        $this->add_action('search_form', 10, 3);
        $this->add_action('add_form', 10, 2);
        $this->add_action('form_controls');

        $this->add_ajax('list_all_currencies');
    }

    function get_menu_items($parent = 0)
    {
        $menu_items = [];
        $menu_name = 'primary';

        if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
            $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
            $menu_items = array_filter(wp_get_nav_menu_items($menu->term_id), function($item) use ($parent) {
                return $item->menu_item_parent == $parent;
            });
        }

        return $menu_items;
    }

    function main()
    {
        $this->view('frame', [
            'type' => 'rows',
            'ratio' => '70, *',
            'frames' => [
                'header' => '/main/header/',
                'body' => '', // '/main/body/?id=2',
            ]
        ]);
    }

    function body()
    {
        $this->view('frame', [
            'type' => 'cols',
            'ratio' => '180, *',
            'frames' => [
                'sidebar' => '/main/sidebar/?id=' . $_GET['id'],
                'main' => '',
            ]
        ]);
    }

    function header()
    {
        /*
        $rights = apply_filters('boss_get_right_list', [], ['parentid' => '0']);
        $this->view('header', [
            'rights' => $rights,
        ]);
        */
        $this->view('header', [
            'menu_items' => $this->get_menu_items()
        ]);
    }

    function sidebar()
    {
        $id = $_GET['id'];

        $rights = apply_filters('boss_get_right_list', [], ['parentid' => $id, 'treeview' => 1]);

        $this->view('sidebar', [
            'menu_items' => $this->get_menu_items($id)
        ]);
    }

    function datatable_toggle_column()
    {
        $this->view('datatable_toggle_column');
    }

    function datatable($columns, $hidden_columns, $action_name)
    {
        $this->view('datatable', [
            'columns' => $columns,
            'hidden_columns' => implode(',', $hidden_columns),
            'action' => $this->get_tag($action_name),
            'nonce' => $this->create_nonce($action_name),
        ]);
    }

    function search_form($controls, $extra = '', $type = 'search')
    {
        $this->view('search-form', ['controls' => $controls, 'extra' => $extra, 'type' => $type]);
    }

    function add_form($action, $controls)
    {
        $this->view('add-form', ['action' => $action, 'controls' => $controls]);
    }

    function form_controls($controls)
    {
        $this->view('form-controls', ['controls' => $controls]);
    }

    function list_all_currencies()
    {
        $data = apply_filters('korgou_currency_get_exchange_list', []);
        wp_send_json_success($data);
    }
};
