<?php
/**
 * Plugin Name: Korgou Admin
 * Plugin URI:  https://shoplic.kr
 * Description: Korgou Admin Plugin
 * Version:     0.1
 * Author:      Shoplic Inc.
 * Author URI:  https://shoplic.kr
 * License:     Shoplic License
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function kg_send_json_success($data = 'Successful operation') {
	wp_send_json_success($data);
}

class KG_Admin
{
	public static function is_admin()
	{
		$user = wp_get_current_user();
		return ( in_array( 'administrator', (array) $user->roles ) );
	}

}
if ( class_exists( 'Shoplic_Plugin' ) ) {
	( new Shoplic_Plugin(
		[
			'id'            => 'korgou_admin',
			'version'       => '0.1',
			'file'          => __FILE__,
		]
	) )->load();
}
